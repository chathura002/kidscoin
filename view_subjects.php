<?php include 'header.php';?>
<!--nav bar-->
<?php $page_title='VIEW / EDIT AND DELETE SUBJECTS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="user-info">
    <div class="col-md-12">
        <table id="sub-tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>sub_id</th>
                    <th>Subject Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        loadSubjectInfo();

        redirectToEditable("sub-tab","edit_subject.php",'sub_id');
        
        deleteSubjects();
    });
</script>

