<?php
session_start();
if (!empty($_SESSION['student_id'])) {
    $myId = $_SESSION['student_id'];
} else {
    header('Location:' . URL . '/index.php');
}
include 'header.php';
$page_title='MY ACCOUNTS';
?>
<!--nav bar-->
<?php include './student_navigation.php'; ?>
<!--end nav bar-->
<!--<div class="col-lg-12 dash">
    <div class="text-center" id="">MY ACCOUNTS</div>
</div>-->
<div class="container" id="my-acc">
    <div class="col-md-12">
        <div class="col-md-8 centered">
            <div class="col-xs-4 circleBase type2 text-center" id="curr-display"></div>
            <div class="col-xs-4 col-xs-offset-3 circleBase type2 text-center" id="tot-display"></div>
            <div class="col-xs-4 col-xs-offset-3 circleBase type2 text-center" id="sav-display"></div>

            <div class="col-md-12">
                <table id="trans-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Transaction</th>
                            <th>Current Account</th>
                            <th>Savings Account</th>
                            <th>Bonus Points</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-1">
            <form  name="my-trans" id="my-trans" method="post" action="<?php echo URL; ?>/models/transfer_points.php">
                <div class="form-group">
                    <label for="curr-acc" class="control-label">From Current Account</label>
                    <input class="form-control input-sm" type="number" name="curr-acc" id="curr-acc" data-validate="required,number" readonly="true" min="0">
                </div>

                <div class="form-group">
                    <label for="sav-acc" class="control-label">To Savings Account</label>
                    <input class="form-control input-sm" type="number" name="sav-acc" id="sav-acc" data-validate="required" readonly="true" min="1" da>
                </div>

                <div class="form-group">
                    <label for="sav-acc" class="control-label">Amount</label>
                    <input class="form-control input-sm" type="number" name="amt" id="amt" data-validate="required" min="1" da>
                </div>

                <div class="form-group">
                    <label for="ref" class="control-label">Reference</label>
                    <input class="form-control input-sm" type="text" name="ref" id="ref" data-validate="required">
                </div>
                <input type="hidden" name="my-id" id="my-id" value="<?php echo $myId; ?>">
                <div class="form-group">
                    <button type="submit" id="trns-btn" class="btn btn-primary pull-right">Transfer</button>
                </div>
            </form>
            <div id="msg"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        loadAccountAmounts();
        loadTransactions();

        var stat = decodeURIComponent($.urlParam('status'));
        if (stat === "t") {
            $("<div>Transaction Done</div>").appendTo("#msg").addClass("alert alert-success text-center");
        }
        if (stat === "f") {
            $("<div>Something going wrong</div>").appendTo("#msg").addClass("alert alert-danger text-center");
        }
    });
</script>




