<?php
session_start();
if(!empty($_SESSION['school_id'])) {
    $scl_id = $_SESSION['school_id'];
}  else {
    $scl_id="";
}
include 'header.php';
$user_type=$_GET['user'];

if($user_type==='4'){
    $page_title='CREATE STUDENT STEP 1';?>
<!--nav bar-->
<?php include './teacher_navigation.php';?>
<!--end nav bar-->
<?php } elseif ($user_type==='3') {
    $page_title='CREATE TEACHER STEP 1';?>
<!--nav bar-->
<?php include './manager_navigation.php';?>
<!--end nav bar-->
<?php } else{ ?>
<!--nav bar-->
<?php $page_title='CREATE MANAGER STEP 1';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<?php }?>
<!--<div class="col-lg-12 dash">
    <div class="text-center" id="user-title"></div>
</div>-->
<div class="container" id="add-mngr">
    <div class="col-md-10">
        <form class="form-horizontal" name="mngr-frm" id="mngr-frm" method="post" action="models/add_user_model.php">
            <div class="form-group">
                <label for="schools" class="col-sm-3 control-label">Select School</label>
                <div class="col-sm-9" id="myScl">
                    <select class="form-control" name="school" id="school" data-validate="schoolChecker"></select>
                </div>          
            </div>

            <div class="form-group">
                <label for="user_name" class="col-sm-3 control-label">User Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="test" name="user_name" id="user_name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="pwd" class="col-sm-3 control-label">Create Password</label>
                <div class="col-sm-9">
                    <input class="form-control" type="password" name="pwd" id="pwd"  data-validate="required" size="32"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="re-pwd" class="col-sm-3 control-label">Confirm Password</label>
                <div class="col-sm-9">
                    <input class="form-control" type="password" name="re-pwd" id="re-pwd"  data-validate="required,pwdChecker" size="32"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="user-email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input class="form-control" type="email" name="user-email" id="user-email"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="status" id="status">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>          
            </div>

            <input type="hidden" name="user-type" id="user-type" value="">
            <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id;?>">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" id="btn-user" class="btn btn-primary pull-right">Next Step</button>
                </div>
            </div>

        </form> 
    </div>
</div>
<script>
    $(document).ready(function () {
        /*
         * URL data manipulation
         */
        var user = decodeURIComponent($.urlParam('user'));
        console.log(user);

        if (user === '2') {
            $("#user-title").text("ADD MANAGER");
            $("#user-type").val(user);
        }
        if (user === '2') {
            getSchoolList();
            hasManager();
        }

        if (user === '3') {
            setInputToCreateUser();
            getSchoolById();
            $("#user-title").text("ADD TEACHER");
            $("#user-type").val(user);
            
        }
        if(user === '4'){
            setInputToCreateUser();
            getSchoolById();
            $("#user-title").text("ADD STUDENT");
            $("#user-type").val(user);
            
        }
        userNameAvailability();
    });
</script>



