<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">CREATE SCHOOL</div>
</div>-->
<!--nav bar-->
<?php $page_title='CREATE SCHOOL';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="create-school">
    <div class="col-md-10">
        <form class="form-horizontal" name="school-frm" id="school-frm" method="post" action="models/create_school_model.php" enctype="multipart/form-data">
            <div class="form-group">
                <label for="school-name" class="col-sm-3 control-label">School Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="school-name" id="school-name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone Number</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="phone" id="phone"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="fax" class="col-sm-3 control-label">Fax Number</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="fax" id="fax"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Address</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="address" id="address"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="status" id="status">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>          
            </div>

            <div class="form-group">
                <label for="logo" class="col-sm-3 control-label">School Logo</label>
                <div class="col-sm-9">
                    <input class="form-control file" type="file" name="logo" id="logo">
                    <label>Accept Formats : jpg, jpeg and png only</label><br>
                    <label>Accept size   : 120*130px only</label>
                </div>
            </div>

            <div class="form-group">
                <label for="des" class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="des" id="des" rows="4" cols="50"></textarea>
                </div>          
            </div>
            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Save School</button>
                </div>
            </div>

        </form> 
    </div>
</div>
<script>
    $(document).ready(function () {
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results === null) {
                return null;
            }
            else {
                return results[1] || 0;
            }
        };

        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {
            $("<div>Successfully add school</div>").insertAfter("#school-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something going wrong</div>").insertAfter("#school-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }

    });
</script>

