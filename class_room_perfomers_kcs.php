<?php
//session_start();
if (!empty($_SESSION)) {
    $cls_id = $_SESSION['class_id'];
    $scl_id = $_SESSION['school_id'];
}
//require './models/common_model.php';

$rt_std_arr = loadLessKidCoiners($cls_id, $scl_id, 'DESC','=');
$std_name_arr = $rt_std_arr[0];
$std_img_arr = $rt_std_arr[1];
$std_pts_arr = $rt_std_arr[2];
?>
<!--<pre><?php print_r($std_img_arr) ?>;</pre>-->
<table class="table table-hover">
    <?php
    if (!empty($std_name_arr)) {
        for ($i = 0; $i < count($std_name_arr); $i++) {
            ?>
            <tbody><tr>
                    <td class="col-xs-2"><img class="circleBase" src="<?php
                        if ($std_img_arr[$i] === 'uploads/') {
                            echo 'uploads/default.png';
                        } else {
                            echo $std_img_arr[$i];
                        };
                        ?>"></td>
                    <td class="col-xs-6 txt"><?php echo ($i + 1) . '. ' . $std_name_arr[$i]; ?></td>
                    <td class="col-xs-4 circleBase type-points text-center pull-right"><?php echo $std_pts_arr[$i]; ?></td>
                </tr>
            </tbody>
        <?php
        }
    } else {
        ?>
        <div class="col-xs-12">
            <div class="col-xs-12 txt alert alert-info"><?php echo '<strong>Sorry...</strong> No Students To Disply'; ?></div>
        </div>
<?php } ?>
</table>
