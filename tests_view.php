<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center" id=""></div>
</div>-->
<!--nav bar-->
<?php $page_title = 'VIEW TESTS';
include './super_admin_navigation.php';
?>
<!--end nav bar-->
<div class="container" id="txt-info">
    <div class="col-md-12">
        <table id="my-tst" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>t_id</th>
                    <th>Test Name</th>
                    <th>Test Description</th>
                    <th>Test Points</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        loadTestToView();
        getSelectedTestId();

    });
</script>