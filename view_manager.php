<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center" id=""></div>
</div>-->
<!--nav bar-->
<?php $page_title='VIEW / EDIT AND DELETE MANAGERS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="user-info">
    <div class="col-md-12">
        <table id="mgr-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First_Name</th>
                    <th>Last_Name</th>
                    <th>User_Name</th>
                    <th>School</th>
                    <th>is Active</th>
                    <th>Edit/Delete</th>
                </tr>
            </thead>
        </table>
        <input id="test" type="text">
    </div>
</div>
<script>
    $(document).ready(function () {
        var url = 'models/get_school_info_model.php?call=3';
        $.ajax({
            type: "GET",
            url: url,
            success: function (results) {
                console.log(results);
                $('#mgr-table').DataTable({
                    "ajax": "models/manager_fill.txt",
                    "columns": [
                        {"data": "manager_id"},
                        {"data": "first_name"},
                        {"data": "last_name"},
                        {"data": "username"},
                        {"data": "school_name"},
                        {"data": "active"},
                        {
                            data: null,
                            className: "center",
                            defaultContent: '<a class="editor_edit">Edit</a> / <a class="editor_remove">Deactivate</a>'
                        }
                    ]
                });
            }
        }); 
        getDataTableRowData('mgr-table');
        deactivateUsers('mgr-table','2');
//        $('#mgr-table').on('click', 'a.editor_edit', function () {
//            var tab = $('#mgr-table').DataTable();
//            var data = tab.row( $(this).parents('tr') ).data();
//            var m_id = data.manager_id;
//            //alert(s_id);
//            sessionStorage.setItem("m_id",m_id);
//            window.location.href = "edit_managers.php";
//        });
    });
</script>





