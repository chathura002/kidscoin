<?php

$sa_rtn_arr = loadMostKCSchools();
$scl_name_arr = $sa_rtn_arr[0];
$pts_arr = $sa_rtn_arr[1];
$scl_img_arr = $sa_rtn_arr[2];
?>
<!--<pre><?php print_r($std_img_arr) ?>;</pre>-->
<table class="table table-hover">
    <?php
    if (!empty($scl_name_arr)) {
        for ($i = 0; $i < count($scl_name_arr); $i++) {
            ?>
            <tbody><tr>
                    <td class="col-xs-2"><img class="circleBase" src="<?php
                        if ($scl_img_arr[$i] === 'logod/' || $scl_img_arr[$i] === '' || $scl_img_arr[$i] === null) {
                            echo 'logos/scl-logo-default.png';
                        } else {
                            echo $scl_img_arr[$i];
                        };
                        ?>"></td>
                    <td class="col-xs-6 txt"><?php echo ($i + 1) . '. ' . $scl_name_arr[$i]; ?></td>
                    <td class="col-xs-4 circleBase type-points text-center pull-right"><?php echo $pts_arr[$i]; ?></td>
                </tr>
            </tbody>
        <?php
        }
    } else {
        ?>
        <div class="col-xs-12">
            <div class="col-xs-12 txt alert alert-info"><?php echo '<strong>Sorry...</strong> No Students To Disply'; ?></div>
        </div>
<?php } ?>
</table>
