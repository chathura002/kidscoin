<?php
session_start();
if (!empty($_SESSION['school_id'])) {
    $scl_id = $_SESSION['school_id'];
} else {
    $scl_id = "";
}
include 'header.php';
$page_title='ASSIGN CLASS ROOM TO TEACHERS';
?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">ASSIGN CLASS ROOM TO TEACHERS</div>
</div>-->
<!--nav bar-->
<?php include './manager_navigation.php';?>
<!--end nav bar-->
<div class="container" id="as-class">
    <div class="col-md-10">
        <form class="form-horizontal" name="cls-tcr-frm" id="test-mgr-frm" method="post" action="<?php echo URL; ?>/models/assign_class_teacher.php">
            <div class="form-group">
                <label for="school-name" class="col-sm-3 control-label">School</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="school-name" id="school-name"  disabled="true">
                </div>          
            </div>

            <div class="form-group">
                <label for="class" class="col-sm-3 control-label">Select Class Room</label>
                <div class="col-sm-9">
                    <select class="form-control" name="class" id="class" data-validate="selectChecker"></select>
                </div>          
            </div>

            <div class="form-group">
                <label for="tcr" class="col-sm-3 control-label">Select Teacher</label>
                <div class="col-sm-9">
                    <select class="form-control" name="tcr" id="tcr" data-validate="selectChecker"></select>
                </div>          
            </div>

            <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id; ?>">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Assign Class</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>

    $(document).ready(function () {
        
        getSchoolById();
        getClassRooms();
        getTeachers();

        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {

            $("<div>Successfully assign Class</div>").insertAfter("#test-mgr-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something is going wrong</div>").insertAfter("#test-mgr-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
        if (status === 'c') {
            $("<div>This class already assign to this teacher, Please try to assign another class</div>").insertAfter("#test-mgr-frm").addClass("alert alert-warning text-center col-sm-9 col-sm-offset-3");
        }
    });

</script>


