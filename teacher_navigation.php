<?php include './nav_bar_top.php'; ?>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li><a href="<?php echo URL; ?>/dashbord_teacher.php">Dashboard</a></li>
            <li><a href="<?php echo URL; ?>/create_user.php?user=4">Add Student</a></li>
            <li><a href="<?php echo URL; ?>/assign_class_student.php">Assign Class Student</a></li>
            <li><a href="<?php echo URL; ?>/assign_test_students.php">Assign Test Student</a></li>
            <li><a href="<?php echo URL; ?>/assign_bonus_points.php">Assign Bonus Point to Student</a></li>
            <li><a href="<?php echo URL; ?>/view_students.php">View/Edit/Delete Students</a></li>
            <li><a href="<?php echo URL; ?>/view_my_profile.php"><?php echo $_SESSION['user_name'].' ';?> Profile</a></li>
            <li><a href="<?php echo URL; ?>/models/logout_action.php">Log out</a></li>
        </ul>
    </div>
</nav>

