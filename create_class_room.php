<?php
session_start();
$scl_id = $_SESSION['school_id'];
include 'header.php';
$page_title='CREATE CLASS ROOMS';
?>
<!--<div class="col-lg-12 mgr_dash">
    <div class="text-center">CREATE CLASS ROOMS</div>
</div>-->
<!--nav bar-->
<?php include './manager_navigation.php';?>
<!--end nav bar-->
<div class="container" id="mgr-class">
    <div class="col-md-10">
        <form class="form-horizontal" name="class-frm" id="lesson-frm" method="post" action="<?php echo URL; ?>/models/add_class_room_model.php">
            <div class="form-group">
                <label for="school-name" class="col-sm-3 control-label">School Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="school-name" id="school-name"  disabled="true" data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="class-name" class="col-sm-3 control-label">Class Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="class-name" id="class-name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="class-des" class="col-sm-3 control-label">Class Special Notes</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="class-des" id="class-des" rows="4" cols="50"></textarea>
                </div>          
            </div>  

            <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id; ?>">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Save Class Room</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>

    $(document).ready(function () {
        getSchoolById();

        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {

            $("<div>Successfully Create Class Room"+$("#class-name").val()+"</div>").insertAfter("#lesson-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something going wrong</div>").insertAfter("#lesson-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }

    });

</script>
