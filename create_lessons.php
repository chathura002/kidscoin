<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">CREATE LESSONS</div>
</div>-->
<!--nav bar-->
<?php $page_title='CREATE LESSONS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="scl-lesson">
    <div class="col-md-10">
        <form class="form-horizontal" name="lesson-frm" id="lesson-frm" method="post" action="models/add_lesson_model.php">
            <div class="form-group">
                <label for="subject-name" class="col-sm-3 control-label">Select Subject</label>
                <div class="col-sm-9">
                    <select class="form-control" name="subject-name" id="subject-name" data-validate="selectChecker"></select>
                </div>          
            </div>
            
            <div class="form-group">
                <label for="lesson-name" class="col-sm-3 control-label">Lesson Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="lesson-name" id="lesson-name"  data-validate="required"> 
                </div>          
            </div>
            
            <div class="form-group">
                <label for="lesson-des" class="col-sm-3 control-label">Lesson Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="lesson-des" id="lesson-des" rows="4" cols="50"></textarea>
                </div>          
            </div>     

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Save Subject</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>
    
    $(document).ready(function () {
        /*
         * Get subject names
         */
        var url = 'models/get_school_info_model.php?call=4';
        $.ajax({
            type: "GET",
            url: url,
            success: function (results) {
                $('#subject-name').html('<option value="0">--Please Select--</option>' + results);
            }
        });
        /*
         * Verify add rule for passord check
         */
        
        $.verify.addRules({
            selectChecker: function(r) {
                setTimeout(function() {
                  var result;
                if(r.val()==="0"){
                   result="Please Select Subject For Lesson";
                    } 
                    else{
                       result=true; 
                    }
                    r.callback(result);
                },200);
            }
        });


  
    });
var status = decodeURIComponent($.urlParam('status'));
console.log(status);

if(status==='t'){
    
    $("<div>Successfully add Lesson</div>").insertAfter("#lesson-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
}
if(status==='f'){
    $("<div>Something going wrong</div>").insertAfter("#lesson-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
}
</script>
