<?php require_once './models/config.php'; ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Kids Coin</title>
        <!--[if lt IE 9]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Add JQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <!-- Bootstrap and jquery datatable-->
        <link href="<?php echo URL; ?>/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">
        <script src="<?php echo URL; ?>/bootstrap/js/bootstrap.js"></script>
        <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js"></script>
        <!-- Local style -->
        <link rel="stylesheet"  href="<?php echo URL; ?>/css/styles.css">
        <script src="<?php echo URL; ?>/js/Verify.min.js"></script>
        <script src="<?php echo URL; ?>/js/common.js"></script>
        <!--fonts-->
        <link href='https://fonts.googleapis.com/css?family=Covered+By+Your+Grace' rel='stylesheet' type='text/css'>
    </head>