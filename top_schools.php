<?php
$school_arr = getSchoolNames();
$scl_ids = $school_arr[1];
$scl_name = $school_arr[0];
$rt_attempts = array();
$scl_logo_arr = array();
for ($i = 0; $i < count($scl_ids); $i++) {
    $rt_attempts[] = loadClassPerformence("'%'", $scl_ids[$i], ' LIKE ');
    $scl_logo_arr[] = getSchoolLogos($scl_ids[$i]);
}
for ($j = 0; $j < count($scl_ids); $j++) {
    array_push($rt_attempts[$j], $scl_logo_arr[$j][0]);
    array_push($rt_attempts[$j], $scl_name[$j]);
    if ($rt_attempts[$j][0] == '' || $rt_attempts[$j][0] == null) {
        unset($rt_attempts[$j]);
    }
}
$success_rate = array(); //(intval($rt_attempts[1]) / intval($rt_attempts[0])) * 100;
$z = 0;
foreach ($rt_attempts as $x => $x_value) {
    $success_rate[$z][] = (intval($x_value[1]) / intval($x_value[0]) * 100);
    $success_rate[$z][] = $x_value[3];
    $success_rate[$z][] = $x_value[2];
    $z++;
}
arsort($success_rate);
?>
<table class="table table-hover">
    <?php
    if (!empty($success_rate)) {
        $y = 0;
        $p = 0;
        foreach ($success_rate as $key => $value) {
            ?>
            <tbody><tr>
                    <td class="col-xs-2"><img class="circleBase" src="<?php
                        if ($value[$y + 2] === 'logos/' || $value[$y + 2] === '' || $value[$y + 2] === null) {
                            echo 'logos/scl-logo-default.png';
                        } else {
                            echo $value[$y + 2];
                        };
                        ?>"></td>
                    <td class="col-xs-6 txt"><?php echo ($p + 1) . '. ' . $value[$y + 1]; ?></td>
                    <td class="col-xs-4 circleBase type-points text-center pull-right"><?php echo round($value[$y], 1); ?></td>
                </tr>
            </tbody>
            <?php
            $p++;
        }
    } else {
        ?>
        <div class="col-xs-12">
            <div class="col-xs-12 txt alert alert-info"><?php echo '<strong>Sorry...</strong> No Students To Disply'; ?></div>
        </div>
    <?php } ?>
</table>


