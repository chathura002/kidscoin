<?php include 'header.php';?>
<!--nav bar-->
<?php $page_title='VIEW / EDIT AND DELETE SUBJECTS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="view-less">
    <div class="col-md-12">
        <form class="form-horizontal" name="ed-sub-frm" id="ed-sub-frm">
            <div class="form-group">
                <label for="subject-name" class="col-sm-3 control-label">Select Subject</label>
                <div class="col-sm-6">
                    <select class="form-control" name="subject-name" id="subject-name">
                    </select>
                </div>
            </div>
        </form>
        <table id="les-tab" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>les_id</th>
                    <th>Lesson Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready(function(){
        loadSubjectsName();
        $('#subject-name').change(function(){
            loadLessonToDatatTable($('#subject-name').val());
        });
        redirectToEditable("les-tab","edit_lessons.php",'lesson_id');
        deleteLessons();
    });
</script>

