<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">EDIT TESTS</div>
</div>-->
<!--nav bar-->
<?php
$page_title = 'QUESTIONS AND ANSWERS';
include './super_admin_navigation.php';
?>
<!--end nav bar-->
<div class="container" id="ed-scl-test">
    <div class="col-md-10">
        <form class="form-horizontal" name="test-edit-frm" id="test-edit-frm" method="post" action="<?php echo URL; ?>/models/add_test_session.php">
            <div class="form-group">
                <label for="subject-name" class="col-sm-3 control-label">Select Subject</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" id="subject-name" name="subject-name">
<!--                    <select class="form-control" name="subject-name" id="subject-name" data-validate="selectChecker"></select>-->
                </div>          
            </div>

            <div class="form-group">
                <label for="lesson-name" class="col-sm-3 control-label">Select Lesson</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" id="lesson-name" name="lesson-name">
<!--                    <select class="form-control" name="lesson-name" id="lesson-name" data-validate="selectChecker"></select>-->
                </div>          
            </div>

            <div class="form-group">
                <label for="test-name" class="col-sm-3 control-label">Test Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="test-name" id="test-name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="points" class="col-sm-3 control-label">Coins Assign</label>
                <div class="col-sm-9">
                    <input class="form-control" type="number" name="points" id="points"  min="0" data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="test-des" class="col-sm-3 control-label">Test Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="test-des" id="test-des" rows="4" cols="50"></textarea>
                </div>          
            </div>
            <div class="form-group"></div>
        </form>
    </div>
    <div class="col-md-12 center-table">
        <table id="ed-qa-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Question</th>
                    <th>Answer A</th>
                    <th>Answer B</th>
                    <th>Answer C</th>
                    <th>Answer D</th>
                    <th>Correct Answer</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>

    $(document).ready(function () {
        loadLessonsNames();
        var my_tid = sessionStorage.getItem("tid");
        loadTestInfo(my_tid);
        loadQA(my_tid);
        reDerectQA(my_tid);
    });

</script>



