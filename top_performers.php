<?php
$top_per_arr=loadTopPerformerce();
$rtn_stud_nme_arr=$top_per_arr[0];
$rtn_stud_img_arr=$top_per_arr[1];
$rtn_stud_pts_arr=$top_per_arr[2];
$rtn_stud_scl_arr=$top_per_arr[3];
?>
<table class="table table-hover">
    <?php
    if (!empty($rtn_stud_nme_arr)) {
        for ($i = 0; $i < count($rtn_stud_nme_arr); $i++) {
            ?>
            <tbody><tr>
                    <td class="col-xs-2"><img class="circleBase" src="<?php
                        if ($rtn_stud_img_arr[$i] === 'uploads/') {
                            echo 'uploads/default.png';
                        } else {
                            echo $rtn_stud_img_arr[$i];
                        };
                        ?>"></td>
                    <td class="col-xs-6 txt" id="sa-top"><?php echo ($i + 1) . '. ' . $rtn_stud_nme_arr[$i].'</br><small>'.$rtn_stud_scl_arr[$i].'</small>'; ?></td>
                    <td class="col-xs-4 circleBase type-points text-center pull-right"><?php echo $rtn_stud_pts_arr[$i]; ?></td>
                </tr>
            </tbody>
        <?php
        }
    } else {
        ?>
        <div class="col-xs-12">
            <div class="col-xs-12 txt alert alert-info"><?php echo '<strong>Sorry...</strong> No Students To Disply'; ?></div>
        </div>
<?php } ?>
</table>
