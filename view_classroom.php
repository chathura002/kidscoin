<?php  
session_start();
if (!empty($_SESSION['school_id'])) {
    $scl_id = $_SESSION['school_id'];
} else {
    $scl_id = "";
}
include 'header.php';
$page_title='CLASS ROOMS';
?>
<!--<div class="col-lg-12 dash">
    <div class="text-center" id=""></div>
</div>-->
<!--nav bar-->
<?php include './manager_navigation.php';?>
<!--end nav bar-->
<div class="container" id="user-info">
    <div class="col-md-12">
        <table id="cls-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>class_id</th>
                    <th>Class Name</th>
                    <th>Description</th>
                    <th>Edit/Delete</th>
                </tr>
            </thead>
        </table>
        <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id; ?>">
    </div>
</div>
<script>
    $(document).ready(function(){
        getClassView();
        getClassRoomId();
        var res=deleteClass();
        if(res==='t'){
            $("<div>Successfully delete classroom and teachers</div>").insertAfter("#cls-table").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        } if(res==='f'){
            $("<div>Something is going wrong</div>").insertAfter("#cls-table").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
    });
</script>