<?php include './nav_bar_top.php'; ?>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li><a href="<?php echo URL; ?>/dashbord_super.php">Dashboard</a></li>
            <li><a href="<?php echo URL; ?>/create_school.php">Create School</a></li>
            <li><a href="<?php echo URL; ?>/create_user.php?user=2">Add Manager</a></li>
            <li><a href="<?php echo URL; ?>/create_subject.php">Create Subject</a></li>
            <li><a href="<?php echo URL; ?>/create_lessons.php">Create Lessons</a></li>
            <li><a href="<?php echo URL; ?>/create_test.php">Create Test</a></li>
            <li><a href="<?php echo URL; ?>/assign_test_managers.php">Assign Test Managers</a></li>
            <li><a href="<?php echo URL; ?>/tests_view.php">View Tests</a></li>
            <li><a href="<?php echo URL; ?>/view_schools.php">View/ Edit/ Delete Schools</a></li>
            <li><a href="<?php echo URL; ?>/view_manager.php">View/ Edit/ Delete Managers</a></li>
            <li><a href="<?php echo URL; ?>/view_subjects.php">View/ Edit/ Delete Subjects</a></li>
            <li><a href="<?php echo URL; ?>/view_lessons.php">View/ Edit/ Delete Lessons</a></li>
            <li><a href="<?php echo URL; ?>/models/logout_action.php">Log out</a></li>
        </ul>
    </div>
</nav>

