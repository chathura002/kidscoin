<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">ASSIGN TEST TO MANAGERS</div>
</div>-->
<!--nav bar-->
<?php $page_title='CREATE TESTS MANAGERS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="as-test">
    <div class="col-md-10">
        <form class="form-horizontal" name="test-mgr-frm" id="test-mgr-frm" method="post" action="<?php echo URL;?>/models/assign_test_manager.php">
            <div class="form-group">
                <label for="mgr-name" class="col-sm-3 control-label">Select Manager</label>
                <div class="col-sm-9">
                    <select class="form-control" name="mgr-name" id="mgr-name" data-validate="selectChecker"></select>
                </div>          
            </div>
            
            <div class="form-group">
                <label for="school-name" class="col-sm-3 control-label">School Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="school-name" id="school-name"  disabled="true"> 
                </div>          
            </div>
            
            <div class="form-group">
                <label for="txt" class="col-sm-3 control-label">Test</label>
                <div class="col-sm-9">
                    <select class="form-control" name="txt[]" id="txt" data-validate="selectChecker" multiple="true"></select>
                </div>          
            </div>

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Assign Tests</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>
    
    $(document).ready(function () {
        /*
         * Get subject names
         */
        var url = 'models/get_set_ajax.php?find=1';
        $.ajax({
            type: "GET",
            url: url,
            success: function (results) {
                $('#mgr-name').html('<option value="0">--Please Select--</option>' + results);
            }
        });
        
        $("#mgr-name").change(function(){
            var mgr_id=$("#mgr-name").val();
            var url = 'models/get_set_ajax.php?find=2';
            $.ajax({
                type: "POST",
                url: url,
                data: "mgr="+mgr_id,
                success: function (result){
                    $("#school-name").val(result);
                }
            });
            
        });
        
        var url = 'models/get_set_ajax.php?find=3';
        $.ajax({
            type: "GET",
            url: url,
            success: function (results) {
                $('#txt').html('<option value="0">--Please Select--</option>' + results);
            }
        });
        /*
         * Verify add rule for passord check
         */
        
        $.verify.addRules({
            selectChecker: function(r) {
                setTimeout(function() {
                  var result;
                if(r.val()==="0"){
                   result="Your selection is not a correct option";
                    } 
                    else{
                       result=true; 
                    }
                    r.callback(result);
                },200);
            }
        });


  
    });
var status = decodeURIComponent($.urlParam('status'));
console.log(status);

if(status==='t'){
    
    $("<div>Successfully assign test</div>").insertAfter("#test-mgr-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
}
if(status==='f'){
    $("<div>Some tests are already assign to this manager</div>").insertAfter("#test-mgr-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
}
if(status==='c'){
    $("<div>This test already assign to this manager, Please try to assign another test</div>").insertAfter("#test-mgr-frm").addClass("alert alert-warning text-center col-sm-9 col-sm-offset-3");
}
</script>
