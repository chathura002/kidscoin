<?php
session_start();
if (!empty($_SESSION['school_id'])) {
    $scl_id = $_SESSION['school_id'];
    $cls_id = $_SESSION['class_id'];
} else {
    $scl_id = "";
}
include 'header.php';
$page_title='VIEW AND MODIFY STUDENTS';
?>
<!--nav bar-->
<?php include './teacher_navigation.php';?>
<!--end nav bar-->
<!--<div class="col-lg-12 dash">
    <div class="text-center" id=""></div>
</div>-->
<div class="container" id="student-info">
    <div class="col-md-12">
        <table id="std-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First_Name</th>
                    <th>Last_Name</th>
                    <th>User_Name</th>
                    <th>is Active</th>
                    <th>Stats</th>
                    <th>Edit/Delete</th>
                </tr>
            </thead>
        </table>
        <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id; ?>">
        <input type="hidden" name="cid" id="cid" value="<?php echo $cls_id; ?>">
    </div>
</div>
<script>
    $(document).ready(function () {
        getStudentsView();
        getDataTableRowData('std-table');
        deactivateUsers('std-table','4');

        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {
            $("<div>Successfully edit user</div>").insertAfter("#tcr-table").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something going wrong</div>").insertAfter("#tcr-table").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        } 
    });
</script>


