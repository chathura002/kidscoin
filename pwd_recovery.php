<?php
include 'header.php';
require_once './models/config.php';
?>
<body class="login-bcolor">
<div class="container container-table">
    <div class="row vertical-center-row">
        <div class="col-md-4 col-md-offset-4">
            <div class="text-center login-title">PASSWORD RECOVERY</div>
            <div class="login-form">
                <form name="pwd-rec-form" id="pwd-rec-form" action="<?php echo URL; ?>/models/pwd_rocover_model.php" method="post">
                    <div class="form-group">
                        <input class="form-control" type="email" name="user-email" id="user-email" placeholder="Enter Your Email Address" data-validate="required" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="form-control btn btn-default ">Send</button>
                    </div>
                    <div class="text-right"><a href="<?php echo URL;?>/index.php">Login</a></div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {
        var err = decodeURIComponent($.urlParam('status'));
        console.log(err);

        if (err === 't') {
            $("<div>Please check your email, password sent</div>").insertAfter("#pwd-rec-form").addClass("text-center sux");
        }
        else if (err === 'f') {
            $("<div>Something is going wrong</div>").insertAfter("#pwd-rec-form").addClass("text-center err");
        }
        else if (err === 'v') {
            $("<div>Your email is not in our system. Please register</div>").insertAfter("#pwd-rec-form").addClass("text-center err");
        }
    });
</script>
