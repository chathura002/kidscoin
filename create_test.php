<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">CREATE TESTS</div>
</div>-->
<!--nav bar-->
<?php $page_title='CREATE TESTS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="scl-test">
    <div class="col-md-10">
        <form class="form-horizontal" name="test-frm" id="test-frm" method="post" action="<?php echo URL; ?>/models/add_test_session.php">
            <div class="form-group">
                <label for="subject-name" class="col-sm-3 control-label">Select Subject</label>
                <div class="col-sm-9">
                    <select class="form-control" name="subject-name" id="subject-name" data-validate="selectChecker"></select>
                </div>          
            </div>

            <div class="form-group">
                <label for="lesson-name" class="col-sm-3 control-label">Select Lesson</label>
                <div class="col-sm-9">
                    <select class="form-control" name="lesson-name" id="lesson-name" data-validate="selectChecker"></select>
                </div>          
            </div>

            <div class="form-group">
                <label for="test-name" class="col-sm-3 control-label">Test Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="test-name" id="test-name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="points" class="col-sm-3 control-label">Coins Assign</label>
                <div class="col-sm-9">
                    <input class="form-control" type="number" name="points" id="points"  min="0" data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="test-des" class="col-sm-3 control-label">Test Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="test-des" id="test-des" rows="4" cols="50"></textarea>
                </div>          
            </div> 

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Create Test Next Step</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>

    $(document).ready(function () {
        loadSubjectsName();
        loadLessonsNames();
        
        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {

            $("<div>Successfully add test</div>").insertAfter("#test-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something going wrong</div>").insertAfter("#test-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
    });

</script>


