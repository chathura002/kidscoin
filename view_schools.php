<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center" id=""></div>
</div>-->
<!--nav bar-->
<?php $page_title='VIEW / EDIT AND DELETE SCHOOL';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="user-info">
    <div class="col-md-12">
        <table id="scl-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>school_id</th>
                    <th>school_Name</th>
                    <th>status</th>
                    <th>Edit/Delete</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<script>
    $(document).ready(function () {
        var url = 'models/get_school_info_model.php?call=2';
        $.ajax({
            type: "GET",
            url: url,
            success: function (results) {
                console.log(results);
                $('#scl-table').DataTable({
                    "ajax": "models/school_fill.txt",
                    "columns": [
                        {"data": "school_id"},
                        {"data": "school_name"},
                        {"data": "active_state"},
                        {
                            data: null,
                            className: "center",
                            defaultContent: '<a href="#" class="editor_edit">Edit</a> / <a href="#" class="editor_remove">Delete</a>'
                        }
                    ]
                });
            }
        });
        
        redirectToEditable("scl-table","edit_schools.php",'school_id');
        
    });
    
</script>



