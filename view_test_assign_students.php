<?php
session_start();
$std = $_SESSION['student_id'];
include 'header.php';
require './models/common_model.php';
$page_title='MY TESTS';
$rtn_arr = getTestAssignedToStudents($std);
if ($rtn_arr) {
    $sub_id_arr = array_values(array_unique($rtn_arr[2]));
    $sub_info = getSubjectsById($sub_id_arr);
    $sub_name = $sub_info[0];
    $sub_id = $sub_info[1];
} else {
    ?>
    <!--UP TEMP-->
    <!--nav bar-->
        <?php include './student_navigation.php'; ?>
        <!--end nav bar-->
<!--    <div class="col-lg-12 dash">
        <div class="text-center" id="">SUBJECTS AND LESSONS</div>
    </div>-->
    <div class="container" id="std-class">
        <div class="col-md-9 col-md-offset-1">
            <div class="alert alert-info text-center" role="alert"><strong>Ops..</strong>You don't have assigned test</div>
        </div>
    </div>
<?php } ?>
<?php if (!empty($rtn_arr)) { ?>
<!--    <div class="col-lg-12 dash">
        <div class="text-center" id="">SUBJECTS AND LESSONS</div>
    </div>-->
    <!--nav bar-->
        <?php include './student_navigation.php'; ?>
        <!--end nav bar-->
    <div class="container" id="std-assign-txt">
        <div class="col-md-12" id="view-test">
            <?php for ($i = 0; $i < count($sub_name); $i ++) { ?>
                <div class="col-md-4" col-xs-12>
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><span class="money-converter"><img src="/KidsCoin/images/money-convert.png"></span>
                                    <a data-toggle="collapse" href="#collapse<?php echo $i + 1; ?>">
                                        <?php echo ' ' . $sub_name[$i] ?>
                                        <?php
                                        $rtn_arr = getLessonsBySubjectId($sub_id[$i], $std);
                                        $lsn_rtn_arr = $rtn_arr[0];
                                        $tst_id_arr = $rtn_arr[1];
                                        $tst_nme_arr = $rtn_arr[2];
                                        $tst_pts_arr = $rtn_arr[3];
                                        ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?php echo $i + 1; ?>" class="panel-collapse collapse">
                                <ul class="list-group">
                                    <?php for ($k = 0; $k < count($lsn_rtn_arr); $k++) { ?>
                                        <li class="list-group-item"><?php echo $lsn_rtn_arr[$k]; ?><div class="pull-right"><span class="label label-info"><?php echo 'Points - ' . $tst_pts_arr[$k]; ?></span><span><?php echo '  |  '; ?></span><a href="the_test.php?tid=<?php echo $tst_id_arr[$k] . "&tanme=" . $tst_nme_arr[$k]; ?>"><?php echo $tst_nme_arr[$k]; ?></a></div></li>
                                                <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
<script>
    $(document).ready(function () {
        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {

            $("<div></div>").insertAfter("#view-test").prepend($('<img>',{class: 'img-responsive',src:'/KidsCoin/images/kids-success-msg.png'})).addClass("col-sm-8 col-sm-offset-2");
        }
        if (status === 'f') {
            $("<div></div>").insertAfter("#view-test").prepend($('<img>',{class: 'img-responsive',src:'/KidsCoin/images/kids-unsuccess-msg.png'})).addClass("col-sm-8 col-sm-offset-2");
        }
    });
</script>
