<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">EDIT SCHOOL</div>
</div>-->
<!--nav bar-->
<?php
$page_title = 'ADD MANAGER INFO';
include './super_admin_navigation.php';
?>
<!--end nav bar-->
<div class="container" id="create-school">
    <div class="col-md-10">
        <form class="form-horizontal" name="school-frm" id="school-frm" method="post" action="<?php echo URL; ?>/models/edit_school_model.php" enctype="multipart/form-data">
            <div class="form-group">
                <label for="school-id" class="col-sm-9 col-sm-offset-3 text-center control-label"></label>
                <br>
                <hr class="col-sm-9 col-sm-offset-3">
                <div class="form-group">
                    <img id="logo-img" class="img-responsive col-sm-2 col-sm-offset-10"/>
                </div>

                <label for="school-name" class="col-sm-3 control-label">School Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="school-name" id="school-name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Phone Number</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="phone" id="phone"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="fax" class="col-sm-3 control-label">Fax Number</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="fax" id="fax"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Address</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="address" id="address"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="status" class="col-sm-3 control-label">Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="status" id="status">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>          
            </div>

            <div class="form-group">
                <label for="logo" class="col-sm-3 control-label">Edit School Logo</label>
                <div class="col-sm-9">
                    <input class="form-control file" type="file" name="logo" id="logo">
                    <label>Accept Formats : jpg, jpeg and png only</label><br>
                    <label>Accept size   : 120*130px only</label>
                </div>
            </div>

            <div class="form-group">
                <label for="des" class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="des" id="des" rows="4" cols="50"></textarea>
                </div>          
            </div>

            <input type="hidden" name="sid" id="sid" value="">
            <input type="hidden" name="img-url" id="img-url" value="">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Edit School</button>
                    <button type="button" class="btn btn-primary pull-right btn-space" onclick="window.location.href = '<?php echo URL; ?>/view_schools.php';">View Schools</button>
                </div>
            </div>

        </form> 
    </div>
</div>

<script>
    $(document).ready(function () {
        var my_id = sessionStorage.getItem("id");
        $.ajax({
            type: 'GET',
            url: 'models/get_school_info_model.php?call=6',
            data: "s_id=" + my_id,
            success: function (results) {
                var obj = $.parseJSON(results);
                if (my_id === null || my_id === "") {
                    $("label[for='school-id']").html("School ID:");
                } else {
                    $("label[for='school-id']").html("School ID: " + my_id);
                }
                $("#school-name").val(obj[0][0]);
                $("#des").val(obj[0][1]);
                $("#phone").val(obj[0][2]);
                $("#fax").val(obj[0][3]);
                $("#address").val(obj[0][4]);
                $("#status").val(obj[0][5]);
                $("#sid").val(obj[0][6]);
                if (obj[0][7] === "logos/" || obj[0][7] === "") {
                    $("#logo-img").remove();
                }
                else {
                    $("#logo-img").attr("src", obj[0][7]);
                }
                $("#img-url").val(obj[0][7]);
            }
        });

        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {
            $("<div>Successfully edit school</div>").insertAfter("#school-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something going wrong</div>").insertAfter("#school-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }

        sessionStorage.clear();
    });

</script>

