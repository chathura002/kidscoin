<?php
session_start();
require './models/common_model.php';
include 'header.php';
$std_id = "";
if (isset($_GET['sid'])) {
    $std_id = $_GET['sid'];
}
$rtn_stat_arr = studentOverallStat($std_id);
$page_title = 'VIEW STUDENTS STATS';
?>
<!--<pre><?php print_r($rtn_stat_arr) ?></pre>-->
<!--nav bar-->
<?php include './teacher_navigation.php'; ?>
<!--end nav bar-->
<div class="row text-center dash-title">
    <h4>STUDENT NAME : <?php echo $rtn_stat_arr[0][0]; ?></h4>
    <div class="centered">
        <div class="circleBase type3 col-xs-8">
            <?php echo $rtn_stat_arr[0][2]; ?>
        </div>
        <div class="circleBase type3 col-xs-8 col-xs-offset-1">
            <?php echo $rtn_stat_arr[0][1]; ?>
        </div>
        <div class="circleBase type3 col-xs-8 col-xs-offset-1">
            <?php echo round($rtn_stat_arr[0][3], 2) . '%'; ?>
        </div>
    </div>

    <div class="centered-inline text-center">
        <div class="col-xs-8">
            <?php echo 'ATTEMPED TESTS'; ?>
        </div>
        <div class="col-xs-8">
            <?php echo 'SUCCESSFUL TESTS'; ?>
        </div>
        <div class="col-xs-8">
            <?php echo 'SUCCESS RATE'; ?>
        </div>
    </div>
</div>
<div class="container" id="student-stat">
    <div class="col-md-12">
        <table id="stat-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Test Name</th>
                    <th>Assign Date</th>
                    <th>Attempts</th>
                    <th>Pass/Fail</th>
                    <th>Success Rate (%)</th>
                </tr>
            </thead>
        </table>
         <input type="hidden" name="sid" id="sid" value="<?php echo $std_id; ?>">
    </div>
</div>
<script>
$(document).ready(function(){
    var id=$('#sid').val();
    loadSudentStat(id);
});
</script>


