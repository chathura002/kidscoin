<?php include 'header.php';?>
<!--nav bar-->
<?php $page_title='VIEW / EDIT AND DELETE LESSONS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="ed-lesson">
    <div class="col-md-10">
        <form class="form-horizontal" name="ed-lesson-frm" id="ed-lesson-frm" method="post" action="<?php echo URL?>/models/edit_lesson_model.php">
            <div class="form-group">
                <label for="lesson-name" class="col-sm-3 control-label">Lesson Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="lesson-name" id="lesson-name"  data-validate="required"> 
                </div>          
            </div>
            
            <div class="form-group">
                <label for="lesson-des" class="col-sm-3 control-label">Lesson Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="lesson-des" id="lesson-des" rows="4" cols="50"></textarea>
                </div>          
            </div>  
            
            <input type="hidden" name="less-id" id="less-id" value="">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Edit Lesson</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>
$(document).ready(function(){
    var lid = sessionStorage.getItem("id");
    loadLessonInfo(lid);
    sessionStorage.clear();
    
    var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {
            $("<div>Successfully edit lesson</div>").insertAfter("#ed-lesson-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something is going wrong,<strong>Try again</strong></div>").insertAfter("#ed-lesson-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
});</script>
