<?php
//session_start();
if (!empty($_SESSION)) {
    $scl_id = $_SESSION['school_id'];
}
$rt_scl_arr = loadSchoolTestSitCounts($scl_id);
$display_arr=bestClassCalculation($rt_scl_arr);
?>
<!--<pre><?php print_r($display_arr) ?>;</pre>-->

<table class="table table-hover">
    <?php
    if (!empty($display_arr)) {
        $i=0;
        foreach($display_arr as $x => $x_value) {
            ?>
            <tbody><tr>
                  <td class="col-xs-6 txt"><?php echo ($i + 1) . '. ' . $x_value['name']; ?></td>
                    <td class="col-xs-4 circleBase type-points text-center pull-right"><?php echo round($x_value['s_rate'],1); ?></td>  
                </tr>
            </tbody>
        <?php
        $i++;
        }
    } else {
        ?>
        <div class="col-xs-12">
            <div class="col-xs-12 txt alert alert-info"><?php echo '<strong>Sorry...</strong> No Students To Disply'; ?></div>
        </div>
<?php } ?>
</table>

