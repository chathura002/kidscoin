<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">CREATE TESTS ADD QUESTION AND ANSWERS</div>
</div>-->
<!--nav bar-->
<?php
$page_title = 'ADD QUESTION AND ANSWERS';
include './super_admin_navigation.php';
?>
<!--end nav bar-->
<div class="container" id="qa-test">
    <div class="col-md-12">

        <form class="form-horizontal" name="qa-frm" id="qa-frm" method="post" action="">
            <div class="form-group col-md-10">
                <label for="txt-que" class="col-sm-3 control-label">Question</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="txt-que" id="txt-que">
                </div>
            </div>

            <div class="form-group col-md-10">
                <label for="ans-1" class="col-sm-3 control-label">Answer 1</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="ans-1" id="ans-1">
                </div>
            </div>

            <div class="form-group col-md-10">
                <label for="ans-2" class="col-sm-3 control-label">Answer 2</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="ans-2" id="ans-2">
                </div>
            </div>

            <div class="form-group col-md-10">
                <label for="ans-3" class="col-sm-3 control-label">Answer 3</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="ans-3" id="ans-3">
                </div>
            </div>

            <div class="form-group col-md-10">
                <label for="ans-4" class="col-sm-3 control-label">Answer 4</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="ans-4" id="ans-4">
                </div>
            </div>

            <div class="form-group col-md-10">
                <label for="ans-correct" class="col-sm-3 control-label" data-validate="selectChecker">Correct Answer</label>
                <div class="col-sm-9">
                    <select class="form-control" name="ans-correct" id="ans-correct" >
                        <option value="0">--Please Select Correct Answer--</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                    </select>
                </div>
            </div>

            <div class="form-group" style="clear: both;">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="button" id="qa-set" class="btn btn-primary pull-right">Set Question and Answer</button>
                </div>
            </div>
            <div class="col-md-12 center-table">

                <table id="qa-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Question</th>
                            <th>Answer A</th>
                            <th>Answer B</th>
                            <th>Answer C</th>
                            <th>Answer D</th>
                            <th>Correct Answer</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" id="qa-save" class="btn btn-primary pull-right">Save Test</button>
                    <button type="button" id="qa-del" class="btn btn-primary pull-right btn-space">Delete Selected</button>
                    <button type="button" id="qa-ed" class="btn btn-primary pull-right btn-space">Edit Selected</button>
                </div>
            </div>

        </form> 
    </div>

</div>
<script>
    $(document).ready(function () {
        setTestToTable('qa-table');
        sendTestDataPhp('qa-table');
        selectMe('qa-table');
        deleteMe('qa-table');
        editMe('qa-table');
    });
</script>




