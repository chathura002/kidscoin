<?php
session_start();
include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center" id="user-edit">EDIT USER INFO</div>
</div>-->
<?php $my_role=$_SESSION['role_code'];
if ($my_role === '1') {
    ?>
    <!--nav bar-->
    <?php $page_title = 'EDIT MANAGER INFO';
    include './super_admin_navigation.php';
    ?>
    <!--end nav bar-->
<?php } elseif ($my_role === '2') {?>
    <!--nav bar-->
    <?php $page_title = 'EDIT TEACHER INFO';
    include './manager_navigation.php';
    ?>
    <!--end nav bar-->
<?php } else { ?>
    <!--nav bar-->
    <?php $page_title = 'EDIT STUDENT INFO';
    include './teacher_navigation.php';
    ?>
    <!--end nav bar-->
<?php }?>
<div class="container" id="edit-user">
    <div class="col-md-10">
        <form class="form-horizontal" name="user-edit-frm" id="mngr-frm" method="post" action="<?php echo URL; ?>/models/edit_user_info_model.php" enctype="multipart/form-data">
            <label for="id" class="col-sm-9 col-sm-offset-3 text-center control-label"></label>
            <br>
            <hr class="col-sm-9 col-sm-offset-3">
            <div class="form-group">
                <img id="prof-img" class="img-responsive img-circle col-sm-2 col-sm-offset-10"/>
            </div>

            <div class="form-group">
                <label for="first_name" class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="test" name="first_name" id="first_name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="test" name="last_name" id="last_name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="dob" class="col-sm-3 control-label">Birthday</label>
                <div class="col-sm-9">
                    <input class="form-control" type="date" name="dob" id="dob"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="gender" class="col-sm-3 control-label">Gender  </label>
                <label class="radio-inline">
                    <input type="radio" name="gender" id="male" value="male"> Male
                </label>
                <label class="radio-inline">
                    <input type="radio" name="gender" id="female" value="female"> Female
                </label>         
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Land Phone</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="phone" id="phone"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="mobile" class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="mobile" id="mobile"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Address</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="address" id="address"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="image" class="col-sm-3 control-label">Change Profile Image</label>
                <div class="col-sm-9">
                    <input class="form-control file" type="file" name="image" id="image"> 
                    <!--<input type="submit" value="Upload Image" name="submit">-->
                </div>          
            </div>

            <div class="form-group">
                <label for="des" class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="des" id="des" rows="4" cols="50"></textarea> 
                </div>          
            </div>

            <input type="hidden" name="user-type" id="user-type" value="">
            <input type="hidden" name="uid" id="uid" value="">
            <input type="hidden" name="img-url" id="img-url" value="">


            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" id="btn-user" class="btn btn-primary pull-right">Edit User</button>
                </div>
            </div>

        </form> 
    </div>
</div>
<script>
    $(document).ready(function () {
        getUserInfoToEdit('edit');
    });

</script>