
$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results === null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
};

var schoolChecker = function () {
    $("#btn-user").click(function () {
        var select_val = $("#school").val();
        console.log(select_val);
        if (select_val === "0") {
            //$('#wrr').remove();
            $("<div>Please Select A School</div>").insertAfter("#mngr-frm").attr('id', 'wrr').addClass("alert alert-danger text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
    });

};

/*
 * User name check
 */
var userNameAvailability = function () {
    $("#pwd").focus(function () {
        var user_nm = $("#user_name").val();
        $.ajax({
            type: "GET",
            url: 'models/get_available_user_name.php',
            data: 'user_nm=' + user_nm,
            success: function (results) {
                //alert(results);
                $('#wrr').remove();
                $("<div>" + results + "</div>").insertAfter("#mngr-frm").attr('id', 'wrr').addClass("alert alert-warning text-center col-sm-9 col-sm-9 col-sm-offset-3");
            }
        });
    });
};
/*
 * Get schools detals
 */
var getSchoolList = function () {
    var url = 'models/get_school_info_model.php?call=1';
    $.ajax({
        type: "GET",
        url: url,
        success: function (results) {
            $('#school').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};
/*
 * Check school already has a manager
 */
var hasManager = function () {
    $('#school').change(function () {
        $('#wrr').remove();
        $('button').prop('disabled', false);
        var url = 'models/get_set_ajax.php?find=20';
        var scl = $('#school').val();
        $.ajax({
            type: "POST",
            url: url,
            data: "sc_id=" + scl,
            success: function (results) {
                var data = jQuery.parseJSON(results);
                if (parseInt(data) > 0) {
                    $('#wrr').remove();
                    $("<div>" + 'This school already has a manger' + "</div>").insertAfter("#mngr-frm").attr('id', 'wrr').addClass("alert alert-danger text-center col-sm-9 col-sm-9 col-sm-offset-3");
//                    $('#user_name').attr("disabled", true);
//                    $('#pwd').attr("disabled", true);
//                    $('#re-pwd').attr("disabled", true);
//                    $('#user-email').attr("disabled", true);
//                    $('#user-email').attr("disabled", true);
//                    $('#status').attr("disabled", true);
                    $('button').prop('disabled', true);
                }
            }
        });
    });
};
/*
 * Get school name by school id
 * @param school_id
 * @returns html
 */
var getSchoolById = function () {
    var url = 'models/get_set_ajax.php?find=5';
    var scl = $("#sid").val();
    $.ajax({
        type: "POST",
        url: url,
        data: "scl=" + scl,
        success: function (results) {
            $('#school-name').val(results);
        }
    });
};

/*
 * School Checker
 */
$.verify.addRules({
    schoolChecker: function (r) {
        setTimeout(function () {
            var result;
            if (r.val() === "0") {
                result = "Please Select A School For Manager";
            }
            else {
                result = true;
            }
            r.callback(result);
        }, 200);
    }
});

/*
 * Verify add rule for passord check
 */
$.verify.addRules({
    pwdChecker: function (r) {
        setTimeout(function () {
            var parent_val = $("#pwd").val();
            var result;
            if (parent_val === r.val()) {
                result = true;
            }
            else {
                result = "Password Not Match...!!!";
            }
            r.callback(result);
        }, 500);
    }
});

/*
 * Verify add rule for passord check
 */

$.verify.addRules({
    selectChecker: function (r) {
        setTimeout(function () {
            var result;
            if (r.val() === "0") {
                result = "Your selection is not a valid";
            }
            else {
                result = true;
            }
            r.callback(result);
        }, 200);
    }
});

$.verify.addRules({
    selectMultiChecker: function (r) {
        setTimeout(function () {
            var result;
            console.log(r);
            if (r.val() === "" || r.val() === null) {
                result = "Your selection is not a valid";
            }
            else {
                result = true;
            }
            r.callback(result);
        }, 200);
    }
});

/*
 * Get class rooms in school
 * @param school_id
 * @return html(class rooms)
 */
var getClassRooms = function () {
    var url = 'models/get_set_ajax.php?find=6';
    var scl = $("#sid").val();
    $.ajax({
        type: "POST",
        url: url,
        data: "scl=" + scl,
        success: function (results) {
            $('#class').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};

/*
 * Get teachers in school
 * @param school_id
 * @return html(teachers)
 */
var getTeachers = function () {
    var url = 'models/get_set_ajax.php?find=7';
    var scl = $("#sid").val();
    $.ajax({
        type: "POST",
        url: url,
        data: "scl=" + scl,
        success: function (results) {
            $('#tcr').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};
/*
 * Get test to assign
 * @return html(test)
 */
var getTest = function () {
    var url = 'models/get_set_ajax.php?find=3';
    $.ajax({
        type: "GET",
        url: url,
        success: function (results) {
            $('#txt').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};
/*
 * Get all class rooms to view/edit
 * @return html(test)
 */
var getClassView = function () {
    var url = 'models/get_set_ajax.php?find=8';
    var scl_id = $('#sid').val();
    $.ajax({
        type: "POST",
        url: url,
        data: "scl=" + scl_id,
        success: function (results) {
            $('#cls-table').DataTable({
                "ajax": "models/class_fill.txt",
                "columns": [
                    {"data": "class_id", "visible": false},
                    {"data": "class_name"},
                    {"data": "des"},
                    {
                        data: null,
                        className: "center",
                        defaultContent: '<a href="#" class="editor_edit">Edit</a> / <a href="#" class="editor_remove">Delete</a>'
                    }
                ]
            });
        }
    });
};
/*
 * Get selected class room id to edit
 */
var getClassRoomId = function () {
    $('#cls-table').on('click', 'a.editor_edit', function () {
        var tab = $('#cls-table').DataTable();
        var data = tab.row($(this).parents('tr')).data();
        localStorage.setItem('cls_data', JSON.stringify(data));
        window.location.href = "edit_classroom.php";
    });
};
/*
 * Get selected class room id to delete
 */
var deleteClass = function () {
    $('#cls-table').on('click', 'a.editor_remove', function () {
        var r = confirm("Do you really want delete this classroom...!!! It will remove all teachers and students in this classroom");
        if (r === true) {
            var tab = $('#cls-table').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            var class_id = data.class_id;
            $.ajax({
                type: 'POST',
                url: 'models/delete_class_model.php',
                data: 'cls=' + class_id,
                success: function (results) {
                    return results;
                }
            });
        }
    });
};
/*
 * Get teachers details and set to table
 * @param school id
 * return html
 */
var getTeachersView = function () {
    var url = 'models/get_set_ajax.php?find=9';
    var scl_id = $('#sid').val();
    $.ajax({
        type: "POST",
        url: url,
        data: "scl=" + scl_id,
        success: function (results) {
            console.log(results);
            $('#tcr-table').DataTable({
                "ajax": "models/teachers_fill.txt",
                "columns": [
                    {"data": "teacher_id", "visible": false},
                    {"data": "first_name"},
                    {"data": "last_name"},
                    {"data": "username"},
                    {"data": "school_name"},
                    {"data": "active"},
                    {
                        data: null,
                        className: "center",
                        defaultContent: '<a class="editor_edit">Edit</a> / <a class="editor_remove">Deactivate</a>'
                    }
                ]
            });
        }
    });
};
/*
 * Common function to get datatable row data
 * and set id for HTML session storage
 * @param table id
 *
 */
var getDataTableRowData = function (tab_id) {
    $('#' + tab_id).on('click', 'a.editor_edit', function () {
        var tab = $('#' + tab_id).DataTable();
        var data = tab.row($(this).parents('tr')).data();
        var id = '';
        var u_type = '';
        if (data.manager_id) {
            id = data.manager_id;
            u_type = '2';
        }
        else if (data.teacher_id) {
            id = data.teacher_id;
            u_type = '3';
        } else if (data.student_id) {
            id = data.student_id;
            u_type = '4';
        }
        sessionStorage.clear();
        sessionStorage.setItem("id", id);
        sessionStorage.setItem("u_type", u_type);
        window.location.href = "edit_user_info.php";
    });
};
/*
 * Common function to get and set data to edit_user_info.php
 * @param table id
 *
 */
var getUserInfoToEdit = function ($form_type) {
    var my_id;
    var user_type;
    var lab_name;
    if ($form_type === 'edit') {
        my_id = sessionStorage.getItem("id");
        user_type = sessionStorage.getItem("u_type");
        sessionStorage.clear();
        lab_name = '';
        console.log(my_id);
    } else if ($form_type === 'view') {
        my_id = $('#uid').val();
        user_type = $('#user-type').val();
    }

    $.ajax({
        type: 'POST',
        url: 'models/get_set_ajax.php?find=4',
        data: "mgr=" + my_id + "&user=" + user_type,
        success: function (results) {
            var obj = $.parseJSON(results);
            if ($form_type === 'edit') {
                if (user_type === '2') {
                    lab_name = 'Manager ID: ';
                }
                else if (user_type === '3') {
                    lab_name = 'Teacher ID: ';
                }
                else if (user_type === '4') {
                    lab_name = 'Student ID: ';
                }
                if (my_id === null || my_id === "") {
                    $("label[for='id']").html(lab_name);
                } else {
                    $("label[for='id']").html(lab_name + my_id);
                }
            }
            $("#first_name").val(obj[0][0]);
            $("#last_name").val(obj[0][1]);
            $("#dob").val(obj[0][2]);
            if (obj[0][3] === "male") {
                $("#male").prop('checked', true);
            } else {
                $("#female").prop('checked', true);
            }
            $("#phone").val(obj[0][4]);
            $("#mobile").val(obj[0][5]);
            $("#address").val(obj[0][6]);
            if (obj[0][7] === "uploads/") {
                $("#prof-img").remove();
            }
            else {
                $("#prof-img").attr("src", obj[0][7]);
            }
            $("#img-url").val(obj[0][7]);
            $("#des").val(obj[0][8]);
            $("#uid").val(obj[0][9]);
            $("#user-type").val(user_type);
        }
    });
};
/*
 * Common function: teacher and student
 * set input for school
 */
var setInputToCreateUser = function () {
    $("#school").remove();
    $('#myScl').append('<input type="test" name="school-name" id="school-name">');
    $('#school-name').addClass("form-control");
    $('#school-name').attr("disabled", true);
};

/*
 * Get students in school
 * @param school_id
 * @return html(teachers)
 */
var getStudents = function () {
    var url = 'models/get_set_ajax.php?find=10';
    var scl = $("#sid").val();
    $.ajax({
        type: "POST",
        url: url,
        data: "scl=" + scl,
        success: function (results) {
            if ($.trim(results) === "false") {
                $('#std').html('<option>No Students To Display</option>');
                $('button').prop('disabled', true);
            } else {
                $('#std').html(results);
            }
        }
    });
};

/*
 * Get students in school
 * @param user_name
 * @return html(tests)
 */
//var getTeacherClass = function () {
//    var url = 'models/get_set_ajax.php?find=11';
//    var user_name = $('#user').val();
//    $.ajax({
//        type: "POST",
//        url: url,
//        data: "user=" + user_name,
//        success: function (results) {
//            console.log($.parseJSON(results));
//            $myObj=$.parseJSON(results);
//            $('#class-name').val($myObj[0]);
//        }
//    });
//};

/*
 * Get students in class
 * @param user_name
 * @return html(tests)
 */
var getTeachersTests = function () {
    var url = 'models/get_set_ajax.php?find=12';
    var user_name = $('#user').val();
    $.ajax({
        type: "POST",
        url: url,
        data: "user=" + user_name,
        success: function (results) {
            $('#test').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};
/*
 * Get students in class
 * @param class Name
 * @return html(students)
 */
var getStudentsTests = function () {
    var url = 'models/get_set_ajax.php?find=13';
    var cls = $('#cid').val();
    var frm_id = $(this).closest("form").attr('id');
    $.ajax({
        type: "POST",
        url: url,
        data: "cls=" + cls,
        success: function (results) {
            if (frm_id === 'std-cls-frm') {
                $('#std').html(results);
            } else {
                $('#std').html('<option value="0">--Please Select--</option>' + results);
            }

        }
    });
};

/*
 * Get and set students to table
 * @param school id,class id
 * return html
 */
var getStudentsView = function () {
    var url = 'models/get_set_ajax.php?find=14';
    var scl_id = $('#sid').val();
    var cls_id = $('#cid').val();
    $.ajax({
        type: "POST",
        url: url,
        data: "scl=" + scl_id + "&cls=" + cls_id,
        success: function (results) {
            console.log(results);
            $('#std-table').DataTable({
                "ajax": "models/student_fill.txt",
                "columns": [
                    {"data": "student_id", "visible": false},
                    {"data": "first_name"},
                    {"data": "last_name"},
                    {"data": "username"},
                    {"data": "active"},
                    {
                        data: "student_id",
                        className: "center",
                        "mRender": function (data) {
                            //var enc_p1 = encodeURIComponent('sid=' + data );
                            return '<a href=view_students_stats.php?sid=' + data + '>View Stat</a>';
                        }
                    },
                    {
                        data: null,
                        className: "center",
                        defaultContent: '<a class="editor_edit">Edit</a> / <a class="editor_remove">Deactivate</a>'
                    }
                ]
            });
        }
    });
};

/*
 * Get and set test to view table
 * return html
 */
var loadTestToView = function () {
    var url = 'models/get_set_ajax.php?find=15';
    $.ajax({
        type: "GET",
        url: url,
        success: function () {
            $('#my-tst').empty();
            $('#my-tst').DataTable({
                "ajax": "models/mytest_fill.txt",
                "columns": [
                    {"data": "test_id",
                        //"visible": false,
                        "title": "t_id", "width": "5%"},
                    {"data": "test_name",
                        "title": "Test Name", "width": "5%"},
                    {"data": "test_des",
                        "title": "Test Description", "width": "45%"},
                    {"data": "points",
                        "title": "Points", "width": "10%"},
                    {
                        data: null,
                        className: "center",
                        "title": "Edit/Delete",
                        defaultContent: '<a class="editor_edit">View Test</a>',
                        "width": "15%"
                    }
                ]
            });
        }
    });
};
/*
 * Get selected test id to edit
 */
var getSelectedTestId = function () {
    $('#my-tst').on('click', 'a.editor_edit', function () {
        var tab = $('#my-tst').DataTable();
        var tdata = tab.row($(this).parents('tr')).data();
        var tid = tdata.test_id;
        sessionStorage.clear();
        sessionStorage.setItem("tid", tid);
        window.location.href = "edit_test.php";
    });
};

/*
 * Get subject names
 */
var loadSubjectsName = function () {
    var url = 'models/get_school_info_model.php?call=4';
    $.ajax({
        type: "GET",
        url: url,
        success: function (results) {
            $('#subject-name').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};
/*
 * Get lesson names
 */
var loadLessonsNames = function () {
    $('#subject-name').change(function () {
        var sub_name = $('#subject-name').val();
        getLessonName(sub_name);
    });
};

var getLessonName = function (sub) {
    var url = 'models/get_school_info_model.php?call=5';
    $.ajax({
        type: "GET",
        url: url,
        data: "sub=" + sub,
        success: function (results) {
            $('#lesson-name').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};
/*
 * Get all test info by test id
 */
var loadTestInfo = function (tid) {
    loadSubjectsName();
    $.ajax({
        type: "POST",
        url: 'models/get_set_ajax.php?find=16',
        data: "tid=" + tid,
        success: function (results) {
            var obj = $.parseJSON(results);
            $('#subject-name').val(obj[0][0]);
            $('#lesson-name').val(obj[0][1]);
            $('#test-name').val(obj[0][2]);
            $('#points').val(obj[0][3]);
            $('#test-des').val(obj[0][4]);
            $('#lesson-name').prop('selectedIndex', obj[0][1]);

        }
    });
};

var checkTest = function () {
    $('#the-test-frm').submit(function (e) {
        var num = $('.list-group').length;
        var status = [];
        for (var i = 1; i < (num + 1); i++) {
            $("input:radio[name='Q" + i + "']:checked").each(function () {
                status.push($(this).val());
            });
        }
        console.log(status);
        if (status.length !== num) {
            alert("Please answer all the questions...!!!");
            $('#btn-tst').attr({"data-toggle": "popover", "data-placement": "left", "data-content": "Please complte all questions"});
        } else {
            var jsonString = JSON.stringify(status);
            $.ajax({
                type: "POST",
                url: 'models/test_check_model.php',
                data: {data: jsonString},
                success: function (result) {
                    var data = jQuery.parseJSON(result);
                    var getUrl = window.location.origin;
                    if (data === 'true') {
                        location.replace(getUrl + '/KidsCoin/view_test_assign_students.php?status=t');
                    }
                    if (data === 'false') {
                        location.replace(getUrl + '/KidsCoin/view_test_assign_students.php?status=f');
                    }

                }
            });
        }
        e.preventDefault();
    });
};
/*
 * Get and set account amounts(Current and Savings both)
 * @param student id
 * return html
 */
var loadAccountAmounts = function () {
    var myId = $('#my-id').val();
    $.ajax({
        type: "POST",
        url: 'models/get_set_ajax.php?find=17',
        data: "myId=" + myId,
        success: function (results) {
            var amts = jQuery.parseJSON(results);
            $('#curr-acc').val(amts[0]);
            $('#sav-acc').val(amts[1]);
            $('#curr-display').html("Everyday" + '</br>' + amts[0]);
            $('#sav-display').html("Savings" + '</br>' + amts[1]);
            $('#tot-display').html("Total" + '</br>' + amts[2]);
        }
    });
};

/*
 * Get and set students to table
 * @param school id,class id
 * return html
 */
var loadTransactions = function () {
    var url = 'models/get_set_ajax.php?find=18';
    var myId = $('#my-id').val();
    $.ajax({
        type: "POST",
        url: url,
        data: "myId=" + myId,
        success: function (results) {
            console.log(results);
            $('#trans-table').DataTable({
                "ajax": "models/transaction_fill.txt",
                "columns": [
                    {"data": "trans_date", "width": "15%"},
                    {"data": "trans_des", "width": "40%"},
                    {"data": "income_account_tr", "width": "15%"},
                    {"data": "savings_account_tr", "width": "15%"},
                    {"data": "bonus_points_tr", "width": "15%"}
                ]
            });
        }
    });
};


var setTestToTable = function (tbl) {
    var tab = $('#' + tbl).DataTable();
    var counter = 0;
    $('#qa-set').on('click', function () {
        var que = $('#txt-que').val();
        var ans1 = $('#ans-1').val();
        var ans2 = $('#ans-2').val();
        var ans3 = $('#ans-3').val();
        var ans4 = $('#ans-4').val();
        var corr_ans = $('#ans-correct :selected').val();
        if (que.length !== 0 && ans1.length !== 0 && ans2.length !== 0 && ans3.length !== 0 && ans4.length !== 0 && corr_ans !== "0") {
            $('#err').remove();
            tab.row.add([
                counter,
                que,
                ans1,
                ans2,
                ans3,
                ans4,
                corr_ans
            ]).draw(false);
            counter++;
            clearTest();
        }
        else {
            $('#err').remove();
            $("<div>You can't add empty values to table</div>").attr('id', 'err').insertAfter("#qa-set").addClass("alert alert-danger text-center col-md-12").css('margin-top', '10px');
        }
    });
};

/*
 * Clear all test boxes
 */
var clearTest = function () {
    $('#txt-que').val('');
    $('#ans-1').val('');
    $('#ans-2').val('');
    $('#ans-3').val('');
    $('#ans-4').val('');
    $('#ans-correct').val(0);
};

var createJSON = function (mtab) {
    var tab = $('#' + mtab).DataTable();
    var rowData = tab.data();
    jsonObj = [];
    for (var i = 0; i < tab.data().length; i++) {
        item = {};
        item["q_id"] = rowData[i][0];
        item["que"] = rowData[i][1];
        item["a1"] = rowData[i][2];
        item["a2"] = rowData[i][3];
        item["a3"] = rowData[i][4];
        item["a4"] = rowData[i][5];
        item["crr"] = rowData[i][6];
        jsonObj.push(item);
    }

    console.log(jsonObj);
    return jsonObj;
};

var sendTestDataPhp = function (mtab) {
    $('#qa-frm').one('submit', function (e) {
        var myJson = createJSON(mtab);
        var json_str = JSON.stringify(myJson);
        console.log(json_str);
        $.ajax({
            type: 'POST',
            url: 'models/add_test.php',
            data: {data: json_str},
            success: function (results) {
                var data = jQuery.parseJSON(results);
                var getUrl = window.location.origin;
                if (data === 'true') {
                    location.replace(getUrl + '/create_test.php?status=t');
                }
                else if (data === 'false') {
                    location.replace(getUrl + '/create_test.php?status=f');
                }
            }
        });
        e.preventDefault();
    });

};
/*
 * Get students in class
 * @param user_name
 * @return html(tests)
 */
var loadTestAssignMan = function () {
    var url = 'models/get_set_ajax.php?find=19';
    var mid = $('#mid').val();
    $.ajax({
        type: "POST",
        url: url,
        data: "man_id=" + mid,
        success: function (results) {
            $('#txt').html('<option value="0">--Please Select--</option>' + results);
        }
    });
};
/*
 * Deactivate users all levels
 */
var deactivateUsers = function (stab, utype) {
    $('#' + stab).on('click', 'a.editor_remove', function () {
        var r = confirm("Do you really want deactivate this user");
        if (r === true) {
            var tab = $('#' + stab).DataTable();
            var data = tab.row($(this).parents('tr')).data();
            console.log(data);
            var u_id = '';
            if (utype === '2') {
                u_id = data.manager_id;
            } else if (utype === '3') {
                u_id = data.teacher_id;
            } else if (utype === '4') {
                u_id = data.student_id;
            }
            $.ajax({
                type: 'POST',
                url: 'models/get_set_ajax.php?find=21',
                data: 'uid=' + u_id + '&u_type=' + utype,
                success: function (results) {
                    location.reload(true);
                }
            });
        }
    });
};

/*
 * Select row of datatable
 */
var selectMe = function (tab) {
    $('#' + tab + ' tbody').on('click', 'tr', function () {
        var tbl;
        if ($.fn.dataTable.isDataTable('#' + tab)) {
            tbl = $('#' + tab).DataTable();
        }
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            tbl.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
};
/*
 * delete row of datatable
 */
var deleteMe = function (tab) {
    $('#qa-del').click(function () {
        var table = $('#' + tab).DataTable();
        table.row('.selected').remove().draw(false);
    });
};
/*
 * edit row of datatable
 */
var editMe = function (tab) {

    $('#qa-ed').click(function () {
        var tbl;
        if ($.fn.dataTable.isDataTable('#' + tab)) {
            tbl = $('#' + tab).DataTable();
            //var tbl = $('#' + tab).DataTable();
            var tdata = tbl.row('.selected').data();
            $('#txt-que').val(tdata[1]);
            $('#ans-1').val(tdata[2]);
            $('#ans-2').val(tdata[3]);
            $('#ans-3').val(tdata[4]);
            $('#ans-4').val(tdata[5]);
            $('#ans-correct').val(tdata[6]);
            tbl.row('.selected').remove().draw(false);
        }
    });
};

var reDerectQA = function (t_id) {
    $('#ed-tst').click(function () {
//        var frm = $('#test-edit-frm');
//        console.log(frm.serializeArray());
//        var data = JSON.stringify(frm.serializeArray());
//        console.log(data);
        sessionStorage.clear();
        sessionStorage.setItem("t_id", t_id);
        window.location.href = "edit_test_QA.php";
    });
};

/*
 * Get and set QA
 * @param test_id
 * return html
 */
var loadQA = function (tid) {
    var url = 'models/get_set_ajax.php?find=22';
    $.ajax({
        type: "POST",
        url: url,
        data: "tid=" + tid,
        success: function (results) {
            console.log(results);
            $('#ed-qa-table').DataTable({
                "sAjaxDataProp": "",
                "destroy": true,
                "ajax": "models/qa_fill.txt",
                "columns": [
                    {"data": "question_no", "width": "15%"},
                    {"data": "question", "width": "40%"},
                    {"data": "ans1", "width": "15%"},
                    {"data": "ans2", "width": "15%"},
                    {"data": "ans3", "width": "15%"},
                    {"data": "ans4", "width": "15%"},
                    {"data": "correct_ans", "width": "15%"}
                ]
            });
        }
    });
};
/*
 * Remove component from child profile
 */
var removeCompChild = function () {
    var u_type = $('#user-type').val();
    if (u_type === '4') {
        $("label[for='phone']").closest(".form-group").remove();
        $("label[for='mobile']").closest(".form-group").remove();
        $("label[for='address']").closest(".form-group").remove();
        $("label[for='des']").html('My Personal Motto');
    }
};
/*
 * load subjects info to datatable
 */
var loadSubjectInfo = function () {
    $.ajax({
        type: "POST",
        url: 'models/get_set_ajax.php?find=23',
        success: function (results) {
            $('#sub-tab').DataTable({
                "aaData": $.parseJSON(results),
                "aoColumns": [
                    {"mData": "sub_id", "visible": false},
                    {"mData": "sub_name"},
                    {"mData": "", "mRender": function () {
                            return '<a class="editor_edit" href=#>Edit</a>';
                        }},
                    {"mData": "", "mRender": function () {
                            return '<a class="editor_remove" href=#>Delete</a>';
                        }}
                ]
            });
        }
    });
};
/*
 * Get table row id and rederect edit page
 */
var redirectToEditable = function (mtab, page, col) {
    $('#' + mtab).on('click', 'a.editor_edit', function () {
        var tab = $('#' + mtab).DataTable();
        var data = tab.row($(this).parents('tr')).data();
        var s_id = data[col];
        //alert(s_id);
        sessionStorage.setItem("id", s_id);
        window.location.href = page;
    });
};

var loadSubjectEdit = function (sid) {
    $.ajax({
        type: "POST",
        url: 'models/get_set_ajax.php?find=24',
        data: "sid=" + sid,
        success: function (results) {
            var data = JSON.parse(results);
            $('#subject-name').val(data[0][1]);
            $('#subject-des').val(data[0][2]);
            $('#sub-type').val(data[0][3]);
            $('#sub-id').val(data[0][0]);
        }
    });
};

/*
 * load subjects info to datatable
 */
var loadLessonToDatatTable = function (sub_id) {
    $.ajax({
        type: "POST",
        url: 'models/get_set_ajax.php?find=25',
        data: "sub_id=" + sub_id,
        success: function (results) {
            $('#les-tab').DataTable({
                "aaData": $.parseJSON(results),
                "destroy": true,
                "aoColumns": [
                    {"mData": "lesson_id", "visible": false},
                    {"mData": "lesson_name"},
                    {"mData": "", "mRender": function () {
                            return '<a class="editor_edit" href=#>Edit</a>';
                        }},
                    {"mData": "", "mRender": function () {
                            return '<a class="editor_remove" href=#>Delete</a>';
                        }}
                ]
            });
        }
    });
};

var loadLessonInfo = function (lid) {
    $.ajax({
        type: "POST",
        url: 'models/get_set_ajax.php?find=26',
        data: "lid=" + lid,
        success: function (results) {
            console.log(results);
            var data = JSON.parse(results);
            $('#lesson-name').val(data[0][1]);
            $('#lesson-des').val(data[0][2]);
            $('#less-id').val(data[0][0]);
        }
    });
};
/*
 * Delete lessons
 * @param lesson_id
 * @return ajax
 */
var deleteLessons = function () {
    $('#les-tab').on('click', 'a.editor_remove', function () {
        var r = confirm("Do you really want dlelet this lesson...!");
        if (r === true) {
            var tab = $('#les-tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            var id = data.lesson_id;
            $.ajax({
                type: 'POST',
                url: 'models/get_set_ajax.php?find=27',
                data: 'id=' + id,
                success: function (results) {
                    var rt_msg = JSON.parse(results);
                    console.log(rt_msg);
                    if (rt_msg === 'true') {
                        $("<div>Lesson deleted successfully</div>").insertAfter("#les-tab").addClass("alert alert-success text-center col-sm-8 col-sm-offset-2");
                    } else {
                        $("<div><strong>SORRY...</strong>Can't delete this lesson</div>").insertAfter("#les-tab").addClass("alert alert-danger text-center col-sm-8 col-sm-offset-2");
                    }
                    window.setTimeout(function () {
                        location.reload(true);
                    }, 3000);
                }
            });
        }
    });
};
/*
 * Delete subjects
 * @param subject_id
 * @return ajax
 */
var deleteSubjects = function () {
    $('#sub-tab').on('click', 'a.editor_remove', function () {
        var r = confirm("Do you really want dlelet this subject...!");
        if (r === true) {
            var tab = $('#sub-tab').DataTable();
            var data = tab.row($(this).parents('tr')).data();
            var id = data.lesson_id;
            $.ajax({
                type: 'POST',
                url: 'models/get_set_ajax.php?find=28',
                data: 'id=' + id,
                success: function (results) {
                    var rt_msg = JSON.parse(results);
                    console.log(rt_msg);
                    if (rt_msg === 'true') {
                        $("<div>Subject deleted successfully</div>").insertAfter("#sub-tab").addClass("alert alert-success text-center col-sm-8 col-sm-offset-2");
                    } else {
                        $("<div><strong>SORRY...</strong>Can't delete this subject</div>").insertAfter("#sub-tab").addClass("alert alert-danger text-center col-sm-8 col-sm-offset-2");
                    }
                    window.setTimeout(function () {
                        location.reload(true);
                    }, 3000);
                }
            });
        }
    });
};
/*
 * Pass selected student id to sudent stat page
 * @return ajax
 */
var loadSudentStat = function (id) {
        $.ajax({
            type: 'POST',
            url: 'models/get_set_ajax.php?find=29',
            data: 'id=' + id,
            success: function (results) {
                console.log(results);
                $('#stat-table').DataTable({
                    "aaData": $.parseJSON(results),
                    "aoColumns": [
                       {"mData": "test_name"},
                       {"mData": "date_assign","sClass": "right"},
                       {"mData": "sit_count",className: "center"},
                       {"mData": "stat",className: "center"},
                       {"mData": "s_rate",className: "center"}
                    ]
                });
            }
        });
};
