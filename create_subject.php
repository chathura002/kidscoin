<?php include 'header.php'; ?>
<!--<div class="col-lg-12 dash">
    <div class="text-center">CREATE SUBJECT</div>
</div>-->
<!--nav bar-->
<?php $page_title='CREATE SUBJECT';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="scl-subject">
    <div class="col-md-10">
        <form class="form-horizontal" name="school-frm" id="subject-frm" method="post" action="models/add_subject_model.php">
            <div class="form-group">
                <label for="subject-name" class="col-sm-3 control-label">Subject Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="subject-name" id="subject-name"  data-validate="required"> 
                </div>          
            </div>
            
            <div class="form-group">
                <label for="subject-des" class="col-sm-3 control-label">Subject Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="subject-des" id="subject-des" rows="4" cols="50"></textarea>
                </div>          
            </div>     
            
            <div class="form-group">
                <label for="sub-type" class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" name="sub-type" id="sub-type">
                        <option value="1">Compulsory</option>
                        <option value="2">Not Compulsory</option>
                        <option value="3">Normal</option>
                    </select>
                </div>          
            </div>
            
            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Save Subject</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>
var status = decodeURIComponent($.urlParam('status'));
console.log(status);

if(status==='t'){
    $("<div>Successfully add Subject</div>").insertAfter("#subject-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
}
if(status==='f'){
    $("<div>Something going wrong</div>").insertAfter("#subject-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
}
</script>
