<?php include './nav_bar_top.php'; ?>
    <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
            <li><a href="<?php echo URL; ?>/dashbord_manager.php">Dashboard</a></li>
            <li><a href="<?php echo URL; ?>/create_class_room.php">Create Class Rooms</a></li>
            <li><a href="<?php echo URL; ?>/create_user.php?user=3">Add Teacher</a></li>
            <li><a href="<?php echo URL; ?>/assign_class_teacher.php">Assign Class-Teacher</a></li>
            <li><a href="<?php echo URL; ?>/assign_test_teachers.php">Assign Test-Teacher</a></li>
            <li><a href="<?php echo URL; ?>/view_classroom.php">View/Edit/Delete Class Room</a></li>
            <li><a href="<?php echo URL; ?>/view_teachers.php">View/Edit/Delete Teachers</a></li>
            <li><a href="<?php echo URL; ?>/view_my_profile.php"><?php echo $_SESSION['user_name'].' ';?> Profile</a></li>
            <li><a href="<?php echo URL; ?>/models/logout_action.php">Log out</a></li>
        </ul>
    </div>
</nav>

