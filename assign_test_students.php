<?php
session_start();
if (!empty($_SESSION['school_id']) && !empty($_SESSION['user_name'])) {
    $scl_id = $_SESSION['school_id'];
    $user = $_SESSION['user_name'];
    $cls = $_SESSION['class_id'];
    $cls_name = $_SESSION['class_name'];
}
include 'header.php';
$page_title='ASSIGN TEST TO STUDENTS';
?>
<!--nav bar-->
<?php include './teacher_navigation.php';?>
<!--end nav bar-->
<!--<div class="col-lg-12 dash">
    <div class="text-center">ASSIGN TEST TO STUDENTS</div>
</div>-->
<div class="container" id="std-class">
    <div class="col-md-10">
        <form class="form-horizontal" name="std-cls-frm" id="std-cls-frm" method="post" action="<?php echo URL; ?>/models/assign_test_students.php">
            <div class="form-group">
                <label for="school-name" class="col-sm-3 control-label">School</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="school-name" id="school-name"  disabled="true">
                </div>          
            </div>
            
            <div class="form-group">
                <label for="class-name" class="col-sm-3 control-label">Class</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="class-name" id="class-name"  disabled="true" value="<?php echo $cls_name?>">
                </div>          
            </div>

            <div class="form-group">
                <label for="test" class="col-sm-3 control-label">Select Test</label>
                <div class="col-sm-9">
                    <select class="form-control" name="test" id="test" data-validate="selectChecker"></select>
                </div>          
            </div>

            <div class="form-group">
                <label for="std" class="col-sm-3 control-label">Select Student</label>
                <div class="col-sm-9">
                    <select class="form-control" name="std[]" id="std" multiple="true" data-validate="selectMultiChecker"></select>
                </div>          
            </div>

            <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id; ?>">
            <input type="hidden" name="cid" id="cid" value="<?php echo $cls; ?>">
            <input type="hidden" name="user" id="user" value="<?php echo $user; ?>">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" id="std-btn" class="btn btn-primary pull-right">Assign Test</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>

    $(document).ready(function () {
        
        getSchoolById();
        getTeachersTests();
        getStudentsTests();
        
        //$('[data-toggle="popover"]').popover({container: 'body'});

        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {

            $("<div>Successfully assign Test</div>").insertAfter("#std-cls-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'c') {
            $("<div>Some of student you selected already assign this test</div>").insertAfter("#std-cls-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something is going wrong, probably these student already assign this test</div>").insertAfter("#std-cls-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
    });

</script>



