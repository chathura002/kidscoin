<?php
$kc_rtn_arr = loadKidsCoinPerformence();
if(!empty($kc_rtn_arr[0]) && !empty($kc_rtn_arr[1])){
$success_rate = (intval($kc_rtn_arr[1]) / intval($kc_rtn_arr[0])) * 100;
}
 else {
     $kc_rtn_arr[0]='0';
     $kc_rtn_arr[1]='0';
  $success_rate='0';
}
?>
<div class="centered">
    <div class="circleBase type3 col-xs-8">
        <?php echo $kc_rtn_arr[0]; ?>
    </div>
    <div class="circleBase type3 col-xs-8 col-xs-offset-1">
        <?php echo $kc_rtn_arr[1]; ?>
    </div>
    <div class="circleBase type3 col-xs-8 col-xs-offset-1">
        <?php echo round($success_rate, 2) . '%'; ?>
    </div>
</div>

<div class="centered-inline text-center">
    <div class="col-xs-8">
        <?php echo 'ATTEMPED TESTS'; ?>
    </div>
    <div class="col-xs-8">
        <?php echo 'SUCCESSFUL TESTS'; ?>
    </div>
    <div class="col-xs-8">
        <?php echo 'SUCCESS RATE'; ?>
    </div>
</div>

