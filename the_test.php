<?php
session_start();
if (isset($_GET['tid']) && isset($_GET['tanme'])) {
    $tst_id = filter_input(INPUT_GET, 'tid');
    $tst_name = filter_input(INPUT_GET, 'tanme');
    $_SESSION['test_id'] = $tst_id;
    $_SESSION['test_name'] = $tst_name;
}
include 'header.php';
require './models/common_model.php';
$rs_qa_arr = loadQA($tst_id);
$rs_sit_cnt = loadStudentTestSitCount($_SESSION['student_id'], $tst_id);
$sit_cnt = $rs_sit_cnt[0][0];
$pts = $rs_sit_cnt[0][1];
$page_title=$tst_name;
?>
<!--<pre><?php print_r($rs_sit_cnt) ?>;</pre>-->
<!--<div class="col-lg-12 dash">
    <div class="text-center" id=""><?php echo $tst_name ?></div>
</div>-->
<!--nav bar-->
<?php include './student_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="user-info">
    <div class="col-md-8 col-md-offset-2">
        <div class="col-md-3 circleBase type1 text-center" id="curr-display"><?php echo 'Attempt'.'</br>'.$sit_cnt;?></div>
        <div class="col-md-3 circleBase type2 text-center col-md-offset-1 pull-right" id="sav-display"><?php echo 'Credits'.'</br>'.$pts;?></div>
    </div>
    <div class="col-md-8 col-md-offset-2">
        <form class="form-horizontal" name="the-test-frm" id="the-test-frm" method="post" action="">
            <?php for ($i = 0; $i < count($rs_qa_arr); $i++) { ?>
                <div class="form-group">
                    <ul class="list-group" id="<?php echo 'lg' . strval($i + 1) ?>">
                        <h4 class="list-group-item active"><?php echo strval($i + 1) . '. ' . $rs_qa_arr[$i]['question'] ?></h4>
                        <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . strval($i + 1) ?>" value="A"><?php echo $rs_qa_arr[$i]['ans1'] ?></li>
                        <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . strval($i + 1) ?>" value="B"><?php echo $rs_qa_arr[$i]['ans2'] ?></li>
                        <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . strval($i + 1) ?>" value="C"><?php echo $rs_qa_arr[$i]['ans3'] ?></li>
                        <li class="list-group-item"><input type="radio" name="<?php echo 'Q' . strval($i + 1) ?>" value="D"><?php echo $rs_qa_arr[$i]['ans4'] ?></li>
                    </ul>
                </div>
            <?php } ?>
            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" id="btn-tst" class="btn btn-primary pull-right">Finish</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(function () {
            $('[data-toggle="popover"]').popover();
        });
        checkTest();
    });
</script>

