<?php 
session_start();
if (!empty($_SESSION['school_id'])) {
    $scl_id = $_SESSION['school_id'];
} else {
    $scl_id = "";
}
include 'header.php';
$page_title='TEACHERS';
?>
<!--<div class="col-lg-12 dash">
    <div class="text-center" id=""></div>
</div>-->
<!--nav bar-->
<?php include './manager_navigation.php';?>
<!--end nav bar-->
<div class="container" id="user-info">
    <div class="col-md-12">
        <table id="tcr-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>#</th>
                    <th>First_Name</th>
                    <th>Last_Name</th>
                    <th>User_Name</th>
                    <th>School</th>
                    <th>is Active</th>
                    <th>Edit/Delete</th>
                </tr>
            </thead>
        </table>
        <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id; ?>">
    </div>
</div>
<script>
    $(document).ready(function () {
        getTeachersView();
        getDataTableRowData('tcr-table');
        deactivateUsers('tcr-table','3');
        
        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {
            $("<div>Successfully edit user</div>").insertAfter("#tcr-table").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something going wrong</div>").insertAfter("#tcr-table").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
        if (status === '2') {
            $("<div>Image already exsit, try with differnt names</div>").insertAfter("#tcr-table").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
        if (status === '3') {
            $("<div><strong>oh.. Too big image </strong>image width and height should be 120px * 120px</div>").insertAfter("#tcr-table").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
        if (status === '4') {
            $("<div><strong>oh.. Check image type </strong>accept image types are jpeg,jpg and png only</div>").insertAfter("#tcr-table").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }

    });
</script>
    

