<?php
session_start();
$_SESSION['op']='=';
include 'header.php';
require './models/common_model.php';
$page_title='DASHBOARD';
?>
<!--nav bar-->
<?php include './teacher_navigation.php';?>
<!--end nav bar-->
<div class="row text-center dash-title">
    <h4>MY CLASS PERFORMENCE : <?php echo $_SESSION['class_name'] ?></h4>
    <?php include './class_performence.php'; ?>
</div>


<!--Three widgets row-->
<div class="row three-widgets">
    <div class="col-md-4 col-sm-4 wid-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                MOST KIDSCOINES
            </div>
            <?php include './class_room_most_kcs.php'; ?>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 wid-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                STUDENT NEED HELP (Based on success rate%)
            </div>
            <?php include './class_room_less_kcs.php'; ?>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 wid-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                TOP 10 PERFORMERS (Based on success rate%)
            </div>
            <?php include './class_room_perfomers_kcs.php'; ?>
        </div>
    </div>
</div>
<!--End of three widgets row-->
<!--<pre><?php //print_r($_SESSION)        ?>;</pre>-->
