<?php
session_start();
$scl_id = $_SESSION['school_id'];
include 'header.php';
?>
<div class="col-lg-12 mgr_dash">
    <div class="text-center">EDIT CLASS ROOMS</div>
</div>
<div class="container" id="ed-class">
    <div class="col-md-10">
        <form class="form-horizontal" name="edit-class-frm" id="lesson-frm" method="post" action="<?php echo URL; ?>/models/edit_class_model.php">
            <div class="form-group">
                <label for="class-id" class="col-sm-9 col-sm-offset-3 text-center control-label"></label>
                <br>
                <hr class="col-sm-9 col-sm-offset-3">
                <label for="school-name" class="col-sm-3 control-label">School Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="school-name" id="school-name"  disabled="true" data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="class-name" class="col-sm-3 control-label">Class Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="class-name" id="class-name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="class-des" class="col-sm-3 control-label">Class Special Notes</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="class-des" id="class-des" rows="4" cols="50"></textarea>
                </div>          
            </div>  

            <input type="hidden" name="sid" id="sid" value="<?php echo $scl_id; ?>">
            <input type="hidden" name="cls-id" id="cls-id" value="">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Edit Classroom</button>
                </div>
            </div>
        </form> 
    </div>
</div>
<script>

    $(document).ready(function () {
        getSchoolById();
        var myObj = localStorage.getItem('cls_data');
        var cls = JSON.parse(myObj);
        $("label[for='class-id']").html("Class ID: "+cls.class_id);
        $('#cls-id').val(cls.class_id);
        $('#class-name').val(cls.class_name);
        $('#class-des').val(cls.des);

        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);
        if (status === 't') {

            $("<div>Successfully Edit Class Room"+$("#class-name").val()+"</div>").insertAfter("#lesson-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something is going wrong</div>").insertAfter("#lesson-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
        localStorage.clear();
        
    });

</script>

