<?php include 'header.php';?>
<!--nav bar-->
<?php $page_title='VIEW / EDIT AND DELETE SUBJECTS';
include './super_admin_navigation.php'; ?>
<!--end nav bar-->
<div class="container" id="scl-subject">
    <div class="col-md-10">
        <form class="form-horizontal" name="ed-sub-frm" id="ed-sub-frm" method="post" action="<?php echo URL?>/models/edit_subject_model.php">
            <div class="form-group">
                <label for="subject-name" class="col-sm-3 control-label">Subject Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="subject-name" id="subject-name"  data-validate="required">
                </div>
            </div>

            <div class="form-group">
                <label for="subject-des" class="col-sm-3 control-label">Subject Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="subject-des" id="subject-des" rows="4" cols="50"></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="sub-type" class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" name="sub-type" id="sub-type">
                        <option value="1">Compulsory</option>
                        <option value="2">Not Compulsory</option>
                        <option value="3">Normal</option>
                    </select>
                </div>
            </div>
            
            <input type="hidden" name="sub-id" id="sub-id" value="">

            <div class="form-group">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <button type="submit" class="btn btn-primary pull-right">Edit Subject</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        var s_id = sessionStorage.getItem("id");
        loadSubjectEdit(s_id);
        sessionStorage.clear();
        
        var status = decodeURIComponent($.urlParam('status'));
        console.log(status);

        if (status === 't') {
            $("<div>Successfully edit school</div>").insertAfter("#ed-sub-frm").addClass("alert alert-success text-center col-sm-9 col-sm-9 col-sm-offset-3");
        }
        if (status === 'f') {
            $("<div>Something going wrong</div>").insertAfter("#ed-sub-frm").addClass("alert alert-danger text-center col-sm-9 col-sm-offset-3");
        }
    });
</script>

