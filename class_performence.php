<?php
//session_start();
if (!empty($_SESSION)) {
    $cls_id = $_SESSION['class_id'];
    $scl_id = $_SESSION['school_id'];
}
//require './models/common_model.php';

$rt_attempts = loadClassPerformence($cls_id, $scl_id,'=');
if(!empty($rt_attempts[0]) && !empty($rt_attempts[1])){
$success_rate = (intval($rt_attempts[1]) / intval($rt_attempts[0])) * 100;
}
 else {
     $rt_attempts[0]='0';
     $rt_attempts[1]='0';
  $success_rate='0';
}
?>
<div class="centered">
    <div class="circleBase type3 col-xs-8">
        <?php echo $rt_attempts[0]; ?>
    </div>
    <div class="circleBase type3 col-xs-8 col-xs-offset-1">
        <?php echo $rt_attempts[1]; ?>
    </div>
    <div class="circleBase type3 col-xs-8 col-xs-offset-1">
        <?php echo round($success_rate, 2) . '%'; ?>
    </div>
</div>

<div class="centered-inline">
    <div class="col-xs-12">
        <?php echo 'ATTEMPED TESTS'; ?>
    </div>
    <div class="col-xs-12">
        <?php echo 'SUCCESSFUL TESTS'; ?>
    </div>
    <div class="col-xs-12">
        <?php echo 'SUCCESS RATE'; ?>
    </div>
</div>

