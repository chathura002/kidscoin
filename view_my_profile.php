<?php  
 session_start();
 if (!empty($_SESSION['role_code'])) {
     $role_code=$_SESSION['role_code'];
     $uid;
     switch ($role_code){
         case '2':
             $uid=$_SESSION['manager_id'];
             break;
         case '3':
             $uid=$_SESSION['teacher_id'];
             break;
         case '4':
             $uid=$_SESSION['student_id'];
             break;
         default :
     }
}
 else {
    header('Location:' . URL . '/index.php?login=f');
}
include 'header.php';
$page_title='MY PROFILE';
if($_SESSION['role_code']==='3'){
?>
<!--nav bar-->
<?php include './teacher_navigation.php';?>
<!--end nav bar-->
<?php } elseif ($_SESSION['role_code']==='2') {
    ?>
<!--nav bar-->
<?php include './manager_navigation.php';?>
<!--end nav bar-->
<?php }else{?>
<!--nav bar-->
<?php include './student_navigation.php';?>
<!--end nav bar-->
<?php }?>

<!--<div class="col-lg-12 dash">
    <div class="text-center" id="user-edit">My Profile</div>
</div>-->
<div class="container" id="view-user">
    <div class="col-md-10">
        <form class="form-horizontal" name="view-user-frm" id="view-user-frm">
            <label for="id" class="col-sm-9 col-sm-offset-3 text-center control-label"><?php echo $_SESSION['user_name']?></label>
            <br>
            <hr class="col-sm-9 col-sm-offset-3">
            <div class="form-group">
                <img id="prof-img" class="img-responsive img-circle col-sm-2 col-sm-offset-10"/>
            </div>

            <div class="form-group">
                <label for="first_name" class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="test" name="first_name" id="first_name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="last_name" class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input class="form-control" type="test" name="last_name" id="last_name"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="dob" class="col-sm-3 control-label">Birthday</label>
                <div class="col-sm-9">
                    <input class="form-control" type="date" name="dob" id="dob"  data-validate="required"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="gender" class="col-sm-3 control-label">Gender  </label>
                <label class="radio-inline">
                    <input type="radio" name="gender" id="male" value="male"> Male
                </label>
                <label class="radio-inline">
                    <input type="radio" name="gender" id="female" value="female"> Female
                </label>         
            </div>
            
            <div class="form-group">
                <label for="my-email" class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input class="form-control" type="email" name="my-email" id="my-email" value="<?php echo $_SESSION['email'];?>"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="phone" class="col-sm-3 control-label">Land Phone</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="phone" id="phone"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="mobile" class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input class="form-control" type="tel" name="mobile" id="mobile"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="address" class="col-sm-3 control-label">Address</label>
                <div class="col-sm-9">
                    <input class="form-control" type="text" name="address" id="address"> 
                </div>          
            </div>

            <div class="form-group">
                <label for="des" class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="des" id="des" rows="4" cols="50"></textarea> 
                </div>          
            </div>
            
            <input type="hidden" name="user-type" id="user-type" value="<?php echo $role_code;?>">
            <input type="hidden" name="uid" id="uid" value="<?php echo $uid;?>">
        </form> 
    </div>
</div>
<script>
    $(document).ready(function () {
        getUserInfoToEdit('view');
        removeCompChild();
    });

</script>

