<?php
session_start();
include 'header.php';
require './models/common_model.php';
$scl_arr = getAllSchoolData($_SESSION['school_id']);
$page_title = 'DASHBOARD'
?>
<!--nav bar-->
<?php include './manager_navigation.php'; ?>
<!--end nav bar-->
<div class="row text-center dash-title">
    <h4>MY SCHOOL PERFORMENCE : <?php echo $scl_arr[0][0]; ?></h4>
    <?php include './school_performence.php'; ?>
</div>
<!--Three widgets row-->
<div class="row three-widgets">
    <div class="col-md-4 col-sm-4 wid-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                MOST KIDSCOINES
            </div>
            <?php include './school_most_kcs.php'; ?>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 wid-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                TOP 10 CLASS ROOMS (Based on success rate%)
            </div>
            <?php include './school_best_classes.php'; ?>
        </div>
    </div>

    <div class="col-md-4 col-sm-4 wid-1">
        <div class="panel panel-default">
            <div class="panel-heading">
                SCHOOL TOP PERFORMERS (Based on success rate%)
            </div>
            <?php include './school_top_performers.php'; ?>
        </div>
    </div>
</div>
