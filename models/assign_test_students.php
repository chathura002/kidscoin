<?php
session_start();
require_once 'config.php';
require 'common_model.php';
$ts_id = intval(filter_input(INPUT_POST, 'test'));
$std_ass_arr = array();

function assignTestStudents() {
    try {
        global $ts_id;
        $assign_by = $_SESSION['user_name'];
        $assign_role = $_SESSION['role_code'];
        $localCon = dbConnect();
        $sql = '';

        foreach ($_POST['std'] as $selected_student) {
            if (checkAlreadyAssignTest($selected_student)) {
                $sql.="INSERT INTO test_student(stud_id, text_id, sit_count, test_stat, assign_by, role_assign, date_assign, update_by, role_update, date_modify) "
                        . "VALUES(" . $selected_student . ", " . $ts_id . ", 0, 'N', '" . $assign_by . "', " . $assign_role . ", NOW(), '" . $assign_by . "', " . $assign_role . ", NOW());";
            }
        }
        if (!empty($sql)) {
            if (!mysqli_multi_query($localCon, $sql)) {
                //die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            return TRUE;
        } else {
            return FALSE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function checkAlreadyAssignTest($std_id) {
    try {
        global $ts_id;
        $localCon = dbConnect();
        $sql = "SELECT text_id FROM test_student WHERE stud_id=" . $std_id . "";
        $result = mysqli_query($localCon, $sql);
        $tests = array();
        global $std_ass_arr;
        while ($row = mysqli_fetch_assoc($result)) {
            $tests[] = $row['text_id'];
        }
        if (in_array($ts_id, $tests)) {
            array_push($std_ass_arr, $std_id);
            return FALSE;
        } else {
            return TRUE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

if (count($std_ass_arr) === 0 && assignTestStudents() === TRUE) {
    header('Location:' . URL . '/assign_test_students.php?status=t');
} elseif (count($std_ass_arr) > 0 && assignTestStudents() === TRUE) {
    header('Location:' . URL . '/assign_test_students.php?status=c');
} elseif (count($std_ass_arr) > 0 && assignTestStudents() === FALSE) {
    header('Location:' . URL . '/assign_test_students.php?status=f');
}