<?php

session_start();
require_once 'config.php';
require 'common_model.php';

function addTest() {
    try {
        global $counter;
        $data = json_decode(filter_input(INPUT_POST, 'data'), true);
        $curr_test_id = getMaxTestId();
        $test_id = increaseId($curr_test_id);
        $localCon = dbConnect();

        if ((isset($data) || $data !== NULL) && (!empty($_SESSION['tst']))) {
            $test_name = mysqli_real_escape_string($localCon, $_SESSION['tst'][2]);
            $test_des = mysqli_real_escape_string($localCon, $_SESSION['tst'][4]);
            $sql_str1 = "INSERT INTO tests(test_id, lesson_id, test_name, test_des, points)"
                    . "VALUES (" . $test_id . "," . $_SESSION['tst'][1] . ",'" . $test_name . "','" . $test_des . "'," . $_SESSION['tst'][3] . ");";
            for ($i = 0; $i < count($data); $i++) {
                $ques = mysqli_real_escape_string($localCon, $data[$i]["que"]);
                $a1 = mysqli_real_escape_string($localCon, $data[$i]["a1"]);
                $a2 = mysqli_real_escape_string($localCon, $data[$i]["a2"]);
                $a3 = mysqli_real_escape_string($localCon, $data[$i]["a3"]);
                $a4 = mysqli_real_escape_string($localCon, $data[$i]["a4"]);
                $sql_str1 .= "INSERT INTO qa_table(test_id, question_no, question, ans1, ans2, ans3, ans4, correct_ans)"
                        . "VALUES (" . $test_id . "," . $data[$i]["q_id"] . ",'" . $ques . "','" . $a1 . "','" . $a2 . "','" . $a3 . "','" . $a4 . "','" . $data[$i]["crr"] . "');";
            }
            if (!mysqli_multi_query($localCon, $sql_str1)) {
                die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            $counter=$counter+1;
            return TRUE;
        } else {
            return FALSE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

$counter = 0;
if ($counter === 0) {
    if (addTest() === TRUE) {
        echo json_encode('true');
    } else {
        echo json_encode('false');
    }
}  else {
    exit();
}
