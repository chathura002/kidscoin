<?php require_once 'config.php';
require 'common_model.php';
?>

<?php
$call=$_GET['call'];
if($call==="1"){
    getActiveSchool();
}
else if($call==="2"){
    getViewSchool();
}
else if($call==="4"){
    getSubjectNames();
}
else if($call==="5"){
    $sub=$_GET['sub'];
    getLessNames($sub);
}
else if($call==="6"){
    $id=$_GET['s_id'];
    getViewAllSchoolData($id);
}
else{
    getViewManagers();
}

function getActiveSchool() {
    $rs_arr = getSchoolNames();
    $arr1 = $rs_arr[0];
    $arr2 = $rs_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr2[$i]) . "\">" . $arr1[$i] . "</option>";
    }
}

function getViewSchool() {
    $rs_arr = getSchoolInfo();
    $json_str=json_encode($rs_arr);
    $file = 'school_fill.txt';
    file_put_contents($file, $json_str);
}

function getViewManagers() {
    $rs_arr = getManagersInfo();
    $json_str=json_encode($rs_arr);
    $file = 'manager_fill.txt';
    file_put_contents($file, $json_str);
}

function getSubjectNames() {
    $rs_arr = getSubjectName();
    $arr1 = $rs_arr[0];
    $arr2 = $rs_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function getLessNames($sub) {
    $rs_arr = getLessonName($sub);
    $arr1 = $rs_arr[0];
    $arr2 = $rs_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function getViewAllSchoolData($id){
    $rs_arr = getAllSchoolData($id);
    echo json_encode($rs_arr);
}

