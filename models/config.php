<?php
define('URL', 'http://localhost/KidsCoin');
define('DB_TYPE', 'mysql');
define('DB_HOST', 'localhost');
define('DB_NAME', 'Kidscoin_db');
define('DB_USER', 'root');
define('DB_PASS', '');

date_default_timezone_set('Pacific/Auckland');
define('KB', 1024);
define('PATH', $_SERVER['DOCUMENT_ROOT'].'/KidsCoin');

//database connection 
function dbConnect(){
        static $connection;
        if(!isset($connection)) {
	$connection=mysqli_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
        }
 //Check connection
	if($connection === false) {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  }
return $connection;
	}