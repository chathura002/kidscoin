<?php

session_start();
require_once 'config.php';
require './common_model.php';

$u_email = filter_input(INPUT_POST, 'user-email');

function isValidEmail($u_email) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT count(userId) AS cnt FROM user WHERE email='" . $u_email . "'";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        if ($row[0] === "1") {
            return TRUE;
        } else {
            return FALSE;
        }
        //mysqli_close($localCon);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function randomPassword() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function sendPwd($u_email){
    try {
        $localCon = dbConnect();
        $rand_pwd=randomPassword();
        $sql = "UPDATE user SET pwd='".$rand_pwd."' WHERE email='".$u_email."'";
        if (!mysqli_query($localCon, $sql)) {
            die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        mysqli_close($localCon);
        $msg = "Password reset \n\nYour new password is : ".$rand_pwd."\n\nKidsCoin Team";
        $msg = wordwrap($msg,70);
        mail("chathura002@gmail.com","KidsCoin Password Recovery",$msg);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}

if(isValidEmail($u_email)===TRUE){
    if(sendPwd($u_email)===TRUE){
        header('Location:' . URL . '/pwd_recovery.php?status=t');
    }  else {
        header('Location:' . URL . '/pwd_recovery.php?status=f');
    }
}  else {
    header('Location:' . URL . '/pwd_recovery.php?status=v');
}