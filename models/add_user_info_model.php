<?php
session_start();
require_once 'config.php';
require 'common_model.php';
?>
<?php

function addUserInfo() {
    try {
        $role_id = filter_input(INPUT_POST, 'user-type');
        $first_name = filter_input(INPUT_POST, 'first_name');
        $last_name = filter_input(INPUT_POST, 'last_name');
        $dob = filter_input(INPUT_POST, 'dob');
        $gender = filter_input(INPUT_POST, 'gender');
        $phone = filter_input(INPUT_POST, 'phone');
        $mobile = filter_input(INPUT_POST, 'mobile');
        $address = filter_input(INPUT_POST, 'address');
        //$image = filter_input(INPUT_POST, 'image');
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $des = filter_input(INPUT_POST, 'des');
        $create_by = $_SESSION['user_name'];
        $role = $_SESSION['role'];
        $role_code = $_SESSION['role_code'];
        $my_user = $_SESSION['vals'][1];
        $my_email = $_SESSION['vals'][3];
        $user_id = getManagerUserId($my_user, $my_email);
        $max_user_id = getUserMaxId();
        $curr_user_id = increaseId($max_user_id);

        $localCon = dbConnect();

        $sql_str = "INSERT INTO user(userId, roleId, username, email, pwd, create_time, create_by, active, school_id) "
                . "VALUES (" . $curr_user_id . "," . $role_id . ",'" . $_SESSION['vals'][1] . "','" . $_SESSION['vals'][3] . "','" . $_SESSION['vals'][2] . "',NOW(), '" . $role_code . "', " . $_SESSION['vals'][4] . ", " . $_SESSION['vals'][0] . ");";
        if ($role_id === '2') {
            $mgr_id = getManagerId();
            $curr_mgr_id = increaseId($mgr_id);
            $sql_str .= "INSERT INTO manager(manager_id, user_id, first_name, last_name, dob, gender, phone, mobile, address, image_link, description, create_by, role, create_date, update_by, date_modify) "
                    . "VALUES (" . $curr_mgr_id . "," . $curr_user_id . ",'" . $first_name . "','" . $last_name . "','" . $dob . "','" . $gender . "', '" . $phone . "','" . $mobile . "','" . $address . "','" . $target_file . "','" . $des . "','" . $create_by . "', " . $role_code . ", NOW(), '" . $create_by . "', NOW());";
        } elseif ($role_id === '3') {
            $tcr_id = getTeacherId();
            $curr_tcr_id = increaseId($tcr_id);
            $sql_str .="INSERT INTO teacher(teacher_id, user_tab_id, first_name, last_name, dob, gender, phone, mobile, address, image_link, description, create_by, create_role, create_date, update_by, role_update, date_update) "
                    . "VALUES (" . $curr_tcr_id . "," . $curr_user_id . ",'" . $first_name . "','" . $last_name . "','" . $dob . "','" . $gender . "','" . $phone . "','" . $mobile . "','" . $address . "','" . $target_file . "','" . $des . "','" . $create_by . "'," . $role_code . ",NOW(),'" . $create_by . "'," . $role_code . ",NOW());";
        } elseif ($role_id === '4') {
            $std_id = getStudentId();
            $curr_std_id = increaseId($std_id);
            $sql_str .="INSERT INTO student(student_id, user_tid, first_name, last_name, dob, gender, phone, mobile, address, image_link, description, create_by, create_role, create_date, update_by, role_update, date_update) "
                    . "VALUES (" . $curr_std_id . "," . $curr_user_id . ",'" . $first_name . "','" . $last_name . "','" . $dob . "','" . $gender . "','" . $phone . "','" . $mobile . "','" . $address . "','" . $target_file . "','" . $des . "','" . $create_by . "'," . $role_code . ",NOW(),'" . $create_by . "'," . $role_code . ",NOW());";
            $sql_str .= "INSERT INTO kids_coin_account(student_id, income_account_points, savings_account_points, bonus_points, fine_points, create_date, create_by, create_role, update_by, date_modify, role_update) "
                    . "VALUES (" . $curr_std_id . ", 0, 0, 0, 0, NOW(),'" . $create_by . "'," . $role_code . ",'" . $create_by . "',NOW()," . $role_code . ")";
        }

        if (!mysqli_multi_query($localCon, $sql_str)) {
            die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }

        if (!empty($_FILES["image"]["name"])) {
            uploadImage(PATH.'/uploads/',"image");
        }
        mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    } 
}

if (addUserInfo() === TRUE) {
    //unset ($_SESSION['vals']);
    header('Location:' . URL . '/add_user_info.php?status=t');
} else {
    header('Location:' . URL . '/add_user_info.php?status=f');
}
