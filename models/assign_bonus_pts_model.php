<?php
session_start();
require_once 'config.php';
require './common_model.php';

function assignBonusPts() {
    $pts_des = filter_input(INPUT_POST, 'points-name');
    $pts = filter_input(INPUT_POST, 'points');
    $std_id = filter_input(INPUT_POST, 'std');
    $std_role = $_SESSION['role_code'];
    $std_name = $_SESSION['user_name'];
    $t_id = $_SESSION['teacher_id'];
    $acc_id=  getStudentAccountId($std_id);
    try {
        $localCon = dbConnect();
        $sql_str = "UPDATE kids_coin_account SET "
                . "income_account_points=income_account_points+" . $pts . ","
                . "bonus_points=bonus_points+" . $pts . ","
                . "update_by='" . $std_name . "',"
                . "date_modify=NOW(),"
                . "role_update=" . $std_role . " "
                . "WHERE student_id=" . $std_id . ";";
        $sql_str .= "INSERT INTO kidscoin_transaction(acnt_id, trans_des, income_account_tr, savings_account_tr, bonus_points_tr, fine_points_tr, create_date, create_by, create_role) "
                . "VALUES (" . $acc_id . ",'Bonus-Points for " . $pts_des . "',0,0,".$pts.",0,NOW(),'" . $std_name . "'," . $std_role . ");";
        $sql_str .="INSERT INTO bonus_points(student, teacher, des, points, assign_by, assign_date, role_assign, update_by, modify_date, role_modify) "
                . "VALUES(" . $std_id . ", " . $t_id . ", '".$pts_des."', " . $pts . ", '" . $std_name . "', NOW(), " . $std_role . ", '" . $std_name . "', NOW(), " . $std_role . ");";
        if (!mysqli_multi_query($localCon, $sql_str)) {
            die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}
if(assignBonusPts()===TRUE){
    header('Location:' . URL . '/assign_bonus_points.php?status=t');
} else {
    header('Location:' . URL . '/assign_bonus_points.php?status=f');
}