<?php
session_start();
require_once 'config.php';
require 'common_model.php';


function addClassRoom(){
    try {
        $school_id= filter_input(INPUT_POST, 'sid');
        $class_name= filter_input(INPUT_POST, 'class-name');
        $class_des= filter_input(INPUT_POST, 'class-des');
        $curr_class_id=  getMaxClassId();
        $class_id=  increaseId($curr_class_id);
        $create_by=$_SESSION['user_name'];
        $role_create=$_SESSION['role_code'];
        
        $con=dbConnect();
        $sql_str= "INSERT INTO class_room(class_room_id, school_id, class_room_name, des, create_date, create_by, create_role, update_date, update_by, role_update) "
                . "VALUES (".$class_id.",".$school_id.",'".$class_name."','".$class_des."',NOW(),'".$create_by."',".$role_create.",NOW(),'".$create_by."',".$role_create.")";
        if (!mysqli_query($con, $sql_str)) {
            die('Error: ' . mysqli_error($con));
            return FALSE;
        }
        mysqli_close($con);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    } 
}
if(addClassRoom()===TRUE){
    header('Location:' . URL . '/create_class_room.php?status=t');
} else {
    header('Location:' . URL . '/create_class_room.php?status=f');
}