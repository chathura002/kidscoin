<?php

require_once 'config.php';
require 'common_model.php';


$mgr_ass_arr = array();
function assignTestManager() {
    try {
        $mgr_id = intval(filter_input(INPUT_POST, 'mgr-name'));
        $sql = '';
        global $mgr_ass_arr;
        $localCon = dbConnect();
        $rt_txt_arr=checkAlreadyAssign($mgr_id);
        foreach ($_POST['txt'] as $sel_test) {
            if (!in_array($sel_test, $rt_txt_arr)) {
                $sql .= "INSERT INTO test_manager(mgr_id, txt_id) VALUES (" . $mgr_id . "," . $sel_test . ");";
            } else {
                array_push($mgr_ass_arr, $sel_test);
            }
        }
        if (!empty($sql)) {
            if (!mysqli_multi_query($localCon, $sql)) {
                //die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            return TRUE;
        } else {
            return FALSE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function checkAlreadyAssign($mgr_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT txt_id FROM test_manager WHERE mgr_id =" . $mgr_id . "";
        $result = mysqli_query($localCon, $sql);
        $txt = array();
        global $mgr_ass_arr;
        while ($row = mysqli_fetch_assoc($result)) {
            $txt[] = $row['txt_id'];
        }
        return $txt;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

if (count($mgr_ass_arr) === 0 && assignTestManager() === TRUE) {
    header('Location:' . URL . '/assign_test_managers.php?status=t');
} elseif (count($mgr_ass_arr) > 0 && assignTestManager() === TRUE) {
    header('Location:' . URL . '/assign_test_managers.php?status=c');
} elseif (count($mgr_ass_arr) > 0 && assignTestManager() === FALSE) {
    header('Location:' . URL . '/assign_test_managers.php?status=f');
}
