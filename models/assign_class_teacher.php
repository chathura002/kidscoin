<?php
session_start();
require_once 'config.php';
require 'common_model.php';
$cls_id = intval(filter_input(INPUT_POST, 'class'));
$t_id = intval(filter_input(INPUT_POST, 'tcr'));
function assignClass(){
    try {
        global $cls_id;
        global $t_id;
        $assign_by=$_SESSION['user_name'];
        $assign_role=$_SESSION['role_code'];
        $localCon = dbConnect();
        $sql = "INSERT INTO teacher_class(t_id, class_id, assign_by, assign_date, assign_role, update_by, date_modify, update_role) "
                . "VALUES (".$t_id.",".$cls_id.",'".$assign_by."',NOW(),".$assign_role.",'".$assign_by."',NOW(),".$assign_role.")";
        if (!mysqli_query($localCon, $sql)) {
            //die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    } 

}

function checkAlreadyAssignClass($t_id){
    try {
        global $cls_id;
        $localCon = dbConnect();
        $sql="SELECT class_id FROM teacher_class WHERE t_id=".$t_id."";
        $result = mysqli_query($localCon,$sql);
        $class = array();
        while ($row = mysqli_fetch_assoc($result)) { 
            $class[] = $row['class_id'];
        }  
        if (in_array($cls_id, $class)){
            return FALSE;
        }
        else{
            return TRUE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}

if(checkAlreadyAssignClass($t_id)===TRUE){
   if(assignClass()===TRUE){
       header('Location:' . URL . '/assign_class_teacher.php?status=t');
   }  else {
       header('Location:' . URL . '/assign_class_teacher.php?status=f');
   }
}  else {
    header('Location:' . URL . '/assign_class_teacher.php?status=c');
}


