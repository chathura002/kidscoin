<?php
session_start();
require_once 'config.php';
require 'common_model.php';

function setTestSession() {
    $sub_id = filter_input(INPUT_POST, 'subject-name');
    $lesson_id = filter_input(INPUT_POST, 'lesson-name');
    $test_name = filter_input(INPUT_POST, 'test-name');
    $points = filter_input(INPUT_POST, 'points');
    $test_des = filter_input(INPUT_POST, 'test-des');
    if (!empty($sub_id) && !empty($lesson_id) && !empty($test_name) && !empty($points) && !empty($test_des)) {
        $value_arr = array($sub_id, $lesson_id, $test_name, $points, $test_des);
        if (!empty($value_arr)) {
            $_SESSION['tst'] = $value_arr;
            return TRUE;
        } else {
            return FALSE;
        }
    } else {
        return FALSE;
    }
}

if (setTestSession() === TRUE) {
    header('Location:' . URL . '/create_test_add_QA.php');
} else {
    header('Location:' . URL . '/create_test.php');
}
