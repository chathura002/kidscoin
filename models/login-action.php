<?php

session_start();
require_once 'config.php';
require 'common_model.php';
?>
<?php

$user = filter_input(INPUT_POST, 'user-name');
$pwd = filter_input(INPUT_POST, 'user-pwd');

$rs_user;
$rs_pwd;
$rs_role;
$rs_status;
$rs_role_code;
$rs_school_id;
$rs_email;
$checked;

if (isset($user) && isset($pwd)) {
    $localCon = dbConnect();
    $sql = "SELECT u.username, u.pwd, r.roleName, u.active, u.roleId, u.school_id, u.email";
    $sql .= " FROM user u, role r";
    $sql .= " WHERE username='" . $user . "' and pwd='" . $pwd . "' and u.active='1' and u.roleid=r.roleCode";

    $result = mysqli_query($localCon, $sql);
    if ($result != null || $result != FALSE) {
        while ($row = mysqli_fetch_assoc($result)) {
            $rs_user = $row['username'];
            $rs_pwd = $row['pwd'];
            $rs_role = $row['roleName'];
            $rs_status = $row['active'];
            $rs_role_code = $row['roleId'];
            $rs_school_id = $row['school_id'];
            $rs_email = $row['email'];
        }
        if ($rs_user === $user && $pwd === $rs_pwd) {
            session_unset();
            $_SESSION['user_name'] = $user;
            $_SESSION['role'] = $rs_role;
            $_SESSION['role_code'] = $rs_role_code;
            $_SESSION['email'] = $rs_email;

            if ($rs_role_code === '1') {
                header('Location:' . URL . '/dashbord_super.php');
            } elseif ($rs_role_code === '2') {
                $_SESSION['school_id'] = $rs_school_id;
                $rtn_mgr_id = getManagersId($rs_user);
                $_SESSION['manager_id'] = $rtn_mgr_id;
                header('Location:' . URL . '/dashbord_manager.php');
            } elseif ($rs_role_code === '3') {
                $_SESSION['school_id'] = $rs_school_id;
                $cls_arr = getTeacherClass($rs_user);
                $_SESSION['class_name'] = $cls_arr[0];
                $_SESSION['class_id'] = $cls_arr[1];
                $t_id = getTeachersId($rs_user);
                $_SESSION['teacher_id'] = $t_id;
                header('Location:' . URL . '/dashbord_teacher.php');
            } else {
                $_SESSION['school_id'] = $rs_school_id;
                $std_arr = getStudentClass($rs_user);
                $_SESSION['class_id'] = $std_arr[0];
                $_SESSION['class_name'] = $std_arr[1];
                $my_std_id = getStudentsId($rs_user);
                $_SESSION['student_id'] = $my_std_id;
                header('Location:' . URL . '/dashbord_student.php');
            }
        } else {
            session_destroy();
            header('Location:' . URL . '/index.php?login=f');
        }
    } else {
        session_destroy();
        header('Location:' . URL . '/index.php?login=a');
    }
    //mysqli_close($localCon);
    //$hash_pwd=password_hash($pwd, PASSWORD_DEFAULT);
}
?>


