<?php
require_once 'config.php';
require 'common_model.php';

$class_id=filter_input(INPUT_POST, 'cls');
function deleteClass() {
    try {
        global $class_id;
        if (!empty($class_id)) {
            $localCon = dbConnect();
            mysqli_autocommit($localCon, FALSE);
            $sql = "DELETE FROM class_room WHERE class_room_id=" . $class_id . ";";
            mysqli_query($localCon, $sql);
            if (!mysqli_commit($localCon)) {
                print("Transaction commit failed\n");
                exit();
            }
            mysqli_close($localCon);
            return TRUE;
        } else {
            return FALSE;
        }
    } catch (Exception $ex) {
        echo $ex->getTraceAsString();
    }
}

if(deleteClass()===TRUE){
    global $class_id;
    echo 't';
}  else {
    echo 'f';
}
