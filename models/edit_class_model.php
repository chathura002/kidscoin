<?php
session_start();
require_once 'config.php';
require 'common_model.php';


function editClassRoom() {
    try {
        $class_id = filter_input(INPUT_POST, 'cls-id');
        $class_name = filter_input(INPUT_POST, 'class-name');
        $class_des = filter_input(INPUT_POST, 'class-des');
        $update_by = $_SESSION['user_name'];
        $role_update = $_SESSION['role_code'];

        if (!empty($class_id)) {
            if (!empty($class_name) || !empty($class_des)) {
                $localCon = dbConnect();
                $sql = "UPDATE class_room SET "
                        . "class_room_name='" . $class_name . "',"
                        . "des='" . $class_des . "',"
                        . "update_date=NOW(),"
                        . "update_by='" . $update_by . "',"
                        . "role_update=" . $role_update . " "
                        . "WHERE class_room_id=" . $class_id . "";
                if (mysqli_query($localCon, $sql)) {
                    return TRUE;
                } else {
                    echo "Error updating record: " . mysqli_error($localCon);
                    return FALSE;
                }
                mysqli_close($localCon);
                return TRUE;
            } else {
                return FALSE;
            }
        }  else {
            return FALSE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}
if(editClassRoom()===TRUE){
    header('Location:' . URL . '/edit_classroom.php?status=t');
} else {
    header('Location:' . URL . '/edit_classroom.php?status=f');
}