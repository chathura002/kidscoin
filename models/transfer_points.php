<?php
session_start();
require_once 'config.php';
require './common_model.php';

function transferAmount() {
    $curr_amt = filter_input(INPUT_POST, 'curr-acc');
    $sav_amt = filter_input(INPUT_POST, 'sav-acc');
    $trans_amt = filter_input(INPUT_POST, 'amt');
    $ref = filter_input(INPUT_POST, 'ref');
    $std_id = $_SESSION['student_id'];
    $std_role = $_SESSION['role_code'];
    $std_name = $_SESSION['user_name'];

    $blnc_income=$curr_amt-$trans_amt;
    $blnc_savings=$sav_amt+$trans_amt;
    $acc_id=  getStudentAccountId($std_id);
    try {
        $localCon = dbConnect();
        $sql_str = "UPDATE kids_coin_account SET "
                . "income_account_points=income_account_points-" . $trans_amt . ","
                . "savings_account_points=savings_account_points+" . $trans_amt . ","
                . "update_by='" . $std_name . "',"
                . "date_modify=NOW(),"
                . "role_update=" . $std_role . " "
                . "WHERE student_id=" . $std_id . ";";
        $sql_str .= "INSERT INTO kidscoin_transaction(acnt_id, trans_des, income_account_tr, savings_account_tr, bonus_points_tr, fine_points_tr, create_date, create_by, create_role) "
                . "VALUES (" . $acc_id . ",'Transfer from Income to Savings'," . $blnc_income . ",".$blnc_savings.",0,0,NOW(),'" . $std_name . "'," . $std_role . ");";
        if (!mysqli_multi_query($localCon, $sql_str)) {
            die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

if(transferAmount()===TRUE){
    header('Location:' . URL . '/my_account.php?status=t');
} else {
    header('Location:' . URL . '/my_account.php?status=f');
}
