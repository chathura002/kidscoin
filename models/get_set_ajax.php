<?php
require_once 'config.php';
require 'common_model.php';
?>

<?php
$find = $_GET['find'];
if ($find === "1") {
    ajaxActiveManagers();
} elseif ($find === "2") {
    ajaxSchoolsByManagerId(filter_input(INPUT_POST, 'mgr'));
} elseif ($find === "3") {
    ajaxAllTest();
} elseif ($find === "4") {
    $user_role = filter_input(INPUT_POST, 'user');
    if ($user_role === '2') {
        ajaxEditManagers(filter_input(INPUT_POST, 'mgr'));
    } elseif ($user_role === '3') {
        ajaxEditTeachers(filter_input(INPUT_POST, 'mgr'));
    } elseif ($user_role === '4') {
        ajaxEditStudents(filter_input(INPUT_POST, 'mgr'));
    }
} elseif ($find === "5") {
    ajaxGetSchoolName(filter_input(INPUT_POST, 'scl'));
} elseif ($find === "6") {
    ajaxGetClasses(filter_input(INPUT_POST, 'scl'));
} elseif ($find === "7") {
    ajaxGetTeachers(filter_input(INPUT_POST, 'scl'));
} elseif ($find === "8") {
    ajaxViewClass(filter_input(INPUT_POST, 'scl'));
} elseif ($find === "9") {
    ajaxSetTeachers(filter_input(INPUT_POST, 'scl'));
} elseif ($find === "10") {
    ajaxGetStudents(filter_input(INPUT_POST, 'scl'));
} elseif ($find === "11") {
    ajaxGetTeacherClass(filter_input(INPUT_POST, 'user'));
} elseif ($find === "12") {
    ajaxGetTeachersTest(filter_input(INPUT_POST, 'user'));
} elseif ($find === "13") {
    ajaxGetStudentsTest(filter_input(INPUT_POST, 'cls'));
} elseif ($find === "14") {
    ajaxSetStudents(filter_input(INPUT_POST, 'scl'), filter_input(INPUT_POST, 'cls'));
} elseif ($find === "15") {
    ajaxLoadAllTestInfo();
} elseif ($find === "16") {
    ajaxloadTestInfo(filter_input(INPUT_POST, 'tid'));
} elseif ($find === "17") {
    ajaxloadMyAccountAmt(filter_input(INPUT_POST, 'myId'));
} elseif ($find === "18") {
    ajaxLoadTransactions(filter_input(INPUT_POST, 'myId'));
} elseif ($find === "19") {
    ajaxGetManagersTest(filter_input(INPUT_POST, 'man_id'));
} elseif ($find === "20") {
    ajaxHasManager(filter_input(INPUT_POST, 'sc_id'));
} elseif ($find === "21") {
    ajaxDeactivateUsers(filter_input(INPUT_POST, 'uid'),filter_input(INPUT_POST, 'u_type'));
} elseif ($find === "22") {
    ajaxLoadQA(filter_input(INPUT_POST, 'tid'));
} elseif ($find === "23") {
    ajaxSubjectView();
} elseif ($find === "24") {
    ajaxLoadSubjectInfo(filter_input(INPUT_POST, 'sid'));
} elseif ($find === "25") {
    ajaxLessonBySubjectView(filter_input(INPUT_POST, 'sub_id'));
} elseif ($find === "26") {
    ajaxLoadLessonsInfo(filter_input(INPUT_POST, 'lid'));
} elseif ($find === "27") {
    ajaxDeleteLesson(filter_input(INPUT_POST, 'id'));
} elseif ($find === "28") {
    ajaxDeleteSubjects(filter_input(INPUT_POST, 'id'));
} elseif ($find === "29") {
    ajaxLoadStudentStat(filter_input(INPUT_POST, 'id'));
}

function ajaxActiveManagers() {
    $rs_arr = getActiveManagers();
    $arr1 = $rs_arr[0];
    $arr2 = $rs_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function ajaxSchoolsByManagerId($mgr_id) {
    if ($mgr_id === "0") {
        echo '';
    } else {
        $scl_name = getSchoolsByManagerId($mgr_id);
        echo $scl_name;
    }
}

function ajaxAllTest() {
    $rt_arr = getAllTest();
    $arr1 = $rt_arr[0];
    $arr2 = $rt_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function ajaxEditManagers($m_id) {
    $rs_arr = getEditManagers($m_id);
    echo json_encode($rs_arr);
}

function ajaxEditTeachers($t_id) {
    $rs_arr = getEditTeacher($t_id);
    echo json_encode($rs_arr);
}

function ajaxGetSchoolName($scl_id) {
    $ajax_rs_arr = getAllSchoolData($scl_id);
    $scl_name = $ajax_rs_arr[0][0];
    echo $scl_name;
}

function ajaxGetClasses($scl_id) {
    $cls_arr = getClasses($scl_id);
    $arr1 = $cls_arr[0];
    $arr2 = $cls_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function ajaxGetTeachers($scl_id) {
    $t_arr = getTeacherNames($scl_id);
    $arr1 = $t_arr[0];
    $arr2 = $t_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function ajaxViewClass($scl_id) {
    $rs_arr = getClassToView($scl_id);
    $json_str = json_encode($rs_arr);
    $file = 'class_fill.txt';
    file_put_contents($file, $json_str);
}

function ajaxSetTeachers($scl_id) {
    $rs_arr = getTeacherToView($scl_id);
    $json_str = json_encode($rs_arr);
    $file = 'teachers_fill.txt';
    file_put_contents($file, $json_str);
}

function ajaxGetStudents($scl_id) {
    $st_arr = getStudentsNames($scl_id);
    $arr1 = $st_arr[0];
    $arr2 = $st_arr[1];
    if (count($arr1) > 0) {
        for ($i = 0; $i < count($arr1); $i++) {
            echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
        }
    } else {
        echo "false";
    }
}

function ajaxGetTeacherClass($u_name) {
    $cls_arr = getTeacherClass($u_name);
    echo json_encode($cls_arr);
}

function ajaxGetTeachersTest($u_name) {
    $txt_arr = getTeachersTest($u_name);
    $arr1 = $txt_arr[0];
    $arr2 = $txt_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function ajaxGetStudentsTest($cls_id) {
    $st_arr = getStudentsTest($cls_id);
    $arr1 = $st_arr[0];
    $arr2 = $st_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function ajaxSetStudents($scl_id, $cls_id) {
    $rs_arr = getStudentsView($scl_id, $cls_id);
    $json_str = json_encode($rs_arr);
    $file = 'student_fill.txt';
    file_put_contents($file, $json_str);
}

function ajaxEditStudents($std_id) {
    $rs_arr = getEditStudents($std_id);
    echo json_encode($rs_arr);
}

function ajaxLoadAllTestInfo() {
    $rs_arr = loadAllTestInfo();
    $json_str = json_encode($rs_arr);
    $file = 'mytest_fill.txt';
    file_put_contents($file, $json_str);
}

function ajaxloadTestInfo($tid) {
    $rst_arr = getAllTestInfo($tid);
    echo json_encode($rst_arr);
}

function ajaxloadMyAccountAmt($my_id) {
    $rst_amt__arr = getMyAccountAmounts($my_id);
    echo json_encode($rst_amt__arr);
}

function ajaxLoadTransactions($mys_id) {
    $rs_arr = getMyTransactions($mys_id);
    $json_str = json_encode($rs_arr);
    $file = 'transaction_fill.txt';
    file_put_contents($file, $json_str);
}

function ajaxGetManagersTest($man_id) {
    $t_arr = getTestAssignManager($man_id);
    $arr1 = $t_arr[0];
    $arr2 = $t_arr[1];
    for ($i = 0; $i < count($arr1); $i++) {
        echo "<option value=\"" . htmlentities($arr1[$i]) . "\">" . $arr2[$i] . "</option>";
    }
}

function ajaxHasManager($scl_id){
    $cnt=hasSclManagers($scl_id);
    echo json_encode($cnt);
}

function ajaxDeactivateUsers($id,$u_type){
    $msg =  deactivateUsers($id, $u_type);
    echo json_encode($msg);
}

function ajaxLoadQA($t_id) {
    $rs_arr = loadQAEdit($t_id);
    $json_str = json_encode($rs_arr);
    $file = 'qa_fill.txt';
    file_put_contents($file, $json_str);
}

function ajaxSubjectView() {
    $rs_arr = getSubjectName();
    $rt_sub_id=$rs_arr[0];
    $rt_sub_name=$rs_arr[1];
    $temp_arr = array();
    $json_arr = array();
    for($i=0; $i<count($rt_sub_id); $i++){
        $temp_arr[$i]['sub_id']=$rt_sub_id[$i];
        $temp_arr[$i]['sub_name']=$rt_sub_name[$i];
        $json_arr[$i] = $temp_arr[$i];
    }
    $json_str = json_encode($json_arr);
    echo $json_str;
}

function ajaxLoadSubjectInfo($sid){
    $rtn_sub_arr =  loadSubjectInfo($sid);
    echo json_encode($rtn_sub_arr);
}

function ajaxLessonBySubjectView($sub) {
    $rs_arr = getLessonName($sub);
    $rt_less_id=$rs_arr[0];
    $rt_less_name=$rs_arr[1];
    $temp_arr = array();
    $json_arr = array();
    for($i=0; $i<count($rt_less_id); $i++){
        $temp_arr[$i]['lesson_id']=$rt_less_id[$i];
        $temp_arr[$i]['lesson_name']=$rt_less_name[$i];
        $json_arr[$i] = $temp_arr[$i];
    }
    $json_str = json_encode($json_arr);
    echo $json_str;
}

function ajaxLoadLessonsInfo($lid){
    $rtn_les_arr = loadLessonInfo($lid);
    echo json_encode($rtn_les_arr);
}

function ajaxDeleteLesson($lid){
    $rt_msg = deleteLessons($lid);
    echo json_encode($rt_msg);
}

function ajaxDeleteSubjects($sid){
    $rt_msg = deleteSubject($sid);
    echo json_encode($rt_msg);
}

function ajaxLoadStudentStat($sid){
    $json_arr = loadStudentDetailStat($sid);
    echo json_encode($json_arr);
}