<?php
session_start();
require_once 'config.php';
require 'common_model.php';

function editLessons(){
    try {
        $les_id=filter_input(INPUT_POST, 'less-id');
        $les_name=filter_input(INPUT_POST, 'lesson-name');
        $les_des=filter_input(INPUT_POST, 'lesson-des');
        if(isset($les_id) && isset($les_name) && isset($les_des)){
            $localCon = dbConnect();
            $sql = "UPDATE lessons SET "
                    . "lesson_name='".$les_name."', "
                    . "lesson_des='".$les_des."', "
                    . "modify_by='".$_SESSION['user_name']."', "
                    . "modify_role=".$_SESSION['role_code'].", "
                    . "modify_date=NOW() WHERE lesson_id=".$les_id."";
            if (!mysqli_query($localCon, $sql)) {
                die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            return TRUE;
        } else {
            return FALSE;
        }
        
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}

if(editLessons()===TRUE){
    header('Location:'.URL.'/edit_lessons.php?status=t');
}  else {
    header('Location:'.URL.'/edit_lessons.php?status=t');
}