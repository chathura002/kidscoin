<?php
session_start();
require_once 'config.php';
require 'common_model.php';
?>
<?php
function addUser() {

    $school_name = filter_input(INPUT_POST, 'school');
    if ($school_name == 'null' || $school_name == "" || empty($school_name)) {
        $school_name=filter_input(INPUT_POST, 'sid');
    }
    //$schl_name = filter_input(INPUT_POST, 'sid');
    $user_name = filter_input(INPUT_POST, 'user_name');
    $pwd = filter_input(INPUT_POST, 'pwd');
    //$enc_pwd=  password_hash($pwd, PASSWORD_DEFAULT);use hash pwd later
    $email = filter_input(INPUT_POST, 'user-email');
    $status = filter_input(INPUT_POST, 'status');
    global $role_id;
    $role_id = filter_input(INPUT_POST, 'user-type');
    if (!empty($school_name) && !empty($user_name) && !empty($pwd) && !empty($email) && !empty($status)) {
        $value_arr = array($school_name, $user_name, $pwd, $email, $status);
        if (!empty($value_arr)) {
            $_SESSION['vals'] = $value_arr;
            return TRUE;
        } else {
            return FALSE;
        }
    } else {
        return FALSE;
    }
}

if (addUser() === TRUE) {
    header('Location:' . URL . '/add_user_info.php?user=' . $role_id);
} else {
    header('Location:' . URL . '/create_user.php?user=' . $role_id);
}