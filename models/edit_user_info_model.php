<?php

session_start();
require_once 'config.php';
require 'common_model.php';
?>
<?php

$user_type = filter_input(INPUT_POST, 'user-type');

function editManagerInfo() {
    try {
        $first_name = filter_input(INPUT_POST, 'first_name');
        $last_name = filter_input(INPUT_POST, 'last_name');
        $dob = filter_input(INPUT_POST, 'dob');
        $gender = filter_input(INPUT_POST, 'gender');
        $phone = filter_input(INPUT_POST, 'phone');
        $mobile = filter_input(INPUT_POST, 'mobile');
        $address = filter_input(INPUT_POST, 'address');
        $image = filter_input(INPUT_POST, 'image');
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $old_image = filter_input(INPUT_POST, 'img-url');
        $des = filter_input(INPUT_POST, 'des');
        $mgr_id = filter_input(INPUT_POST, 'uid');
        $create_by = $_SESSION['user_name'];
        $role = $_SESSION['role'];
        $role_code = $_SESSION['role_code'];

        if (empty($_FILES["image"]["name"])) {
            $target_file = $old_image;
        }

        if (!empty($first_name) || !empty($last_name) || !empty($dob) || !empty($gender) || !empty($phone) || !empty($mobile) || !empty($address) || !empty($target_file) || !empty($des)) {
            $localCon = dbConnect();
            $sql = "UPDATE manager SET "
                    . "first_name='" . $first_name . "',"
                    . "last_name='" . $last_name . "',"
                    . "dob='" . $dob . "',"
                    . "gender='" . $gender . "',"
                    . "phone='" . $phone . "',"
                    . "mobile='" . $mobile . "',"
                    . "address='" . $address . "',"
                    . "image_link='" . $target_file . "',"
                    . "description='" . $des . "',"
                    . "update_by='" . $create_by . "',"
                    . "modified_role=" . $role_code . ","
                    . "date_modify=NOW() "
                    . "WHERE manager_id=" . $mgr_id . "";

            $rt_code = editOrUpdateImage($target_file, $old_image);
            if ($rt_code === 1) {
                if (!mysqli_query($localCon, $sql)) {
                    die('Error: ' . mysqli_error($localCon));
                    return FALSE;
                }
                mysqli_close($localCon);
                return TRUE;
            } else {
                return $rt_code;
            }
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function editTeacherInfo() {
    try {
        $first_name = filter_input(INPUT_POST, 'first_name');
        $last_name = filter_input(INPUT_POST, 'last_name');
        $dob = filter_input(INPUT_POST, 'dob');
        $gender = filter_input(INPUT_POST, 'gender');
        $phone = filter_input(INPUT_POST, 'phone');
        $mobile = filter_input(INPUT_POST, 'mobile');
        $address = filter_input(INPUT_POST, 'address');
        $image = filter_input(INPUT_POST, 'image');
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $old_image = filter_input(INPUT_POST, 'img-url');
        $des = filter_input(INPUT_POST, 'des');
        $mgr_id = filter_input(INPUT_POST, 'uid');
        $create_by = $_SESSION['user_name'];
        $role = $_SESSION['role'];
        $role_code = $_SESSION['role_code'];

        if (empty($_FILES["image"]["name"])) {
            $target_file = $old_image;
        }

        if (!empty($first_name) || !empty($last_name) || !empty($dob) || !empty($gender) || !empty($phone) || !empty($mobile) || !empty($address) || !empty($target_file) || !empty($des)) {
            $localCon = dbConnect();
            $sql = "UPDATE teacher SET "
                    . "first_name='" . $first_name . "',"
                    . "last_name='" . $last_name . "',"
                    . "dob='" . $dob . "',"
                    . "gender='" . $gender . "',"
                    . "phone='" . $phone . "',"
                    . "mobile='" . $mobile . "',"
                    . "address='" . $address . "',"
                    . "image_link='" . $target_file . "',"
                    . "description='" . $des . "',"
                    . "update_by='" . $create_by . "',"
                    . "role_update=" . $role_code . ","
                    . "date_update=NOW() "
                    . "WHERE teacher_id=" . $mgr_id . "";

            $rt_code = editOrUpdateImage($target_file, $old_image);
            if ($rt_code === 1) {
                if (!mysqli_query($localCon, $sql)) {
                    die('Error: ' . mysqli_error($localCon));
                    return FALSE;
                }
                mysqli_close($localCon);
                return TRUE;
            } else {
                return $rt_code;
            }
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function editStudentsInfo() {
    try {
        $first_name = filter_input(INPUT_POST, 'first_name');
        $last_name = filter_input(INPUT_POST, 'last_name');
        $dob = filter_input(INPUT_POST, 'dob');
        $gender = filter_input(INPUT_POST, 'gender');
        $phone = filter_input(INPUT_POST, 'phone');
        $mobile = filter_input(INPUT_POST, 'mobile');
        $address = filter_input(INPUT_POST, 'address');
        $image = filter_input(INPUT_POST, 'image');
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["image"]["name"]);
        $old_image = filter_input(INPUT_POST, 'img-url');
        $des = filter_input(INPUT_POST, 'des');
        $std_id = filter_input(INPUT_POST, 'uid');
        $create_by = $_SESSION['user_name'];
        $role = $_SESSION['role'];
        $role_code = $_SESSION['role_code'];

        if (empty($_FILES["image"]["name"])) {
            $target_file = $old_image;
        }

        if (!empty($first_name) || !empty($last_name) || !empty($dob) || !empty($gender) || !empty($phone) || !empty($mobile) || !empty($address) || !empty($target_file) || !empty($des)) {
            $localCon = dbConnect();
            $sql = "UPDATE student SET "
                    . "first_name='" . $first_name . "', "
                    . "last_name='" . $last_name . "', "
                    . "dob='" . $dob . "', "
                    . "gender='" . $gender . "', "
                    . "phone='" . $phone . "', "
                    . "mobile='" . $mobile . "', "
                    . "address='" . $address . "', "
                    . "image_link='" . $target_file . "', "
                    . "description='" . $des . "', "
                    . "update_by='" . $create_by . "', "
                    . "role_update=" . $role_code . ", "
                    . "date_update=NOW()  "
                    . "WHERE student_id=" . $std_id . "";
        }

        $rt_code = editOrUpdateImage($target_file, $old_image);
        if ($rt_code === 1) {
            if (!mysqli_query($localCon, $sql)) {
                die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            return TRUE;
        } else {
            return $rt_code;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function editOrUpdateImage($img_file, $old_img) {
    $the_code;
    if (($old_img !== $img_file) && $old_img !== 'uploads/') {
        unlink(PATH . '/uploads/' . $old_img);
        $the_code = uploadImage(PATH . '/uploads/', "image");
        return $the_code;
    } else {
        $the_code = uploadImage(PATH . '/uploads/', "image");
        return $the_code;
    }
}

if ($user_type === '2') {
    if (editManagerInfo() === TRUE) {
        header('Location:' . URL . '/view_manager.php?status=t');
    } else {
        $err_code = editManagerInfo();
        if ($err_code === 0 || $err_code === 2 || $err_code === 3 || $err_code === 4) {
            header('Location:' . URL . '/view_manager.php?status=' . $err_code);
        }
    }
} elseif ($user_type === '3') {
    if (editTeacherInfo() === TRUE) {
        header('Location:' . URL . '/view_teachers.php?status=t');
    } else {
        $err_code = editTeacherInfo();
        if ($err_code === 0 || $err_code === 2 || $err_code === 3 || $err_code === 4) {
            header('Location:' . URL . '/view_teachers.php?status=' . $err_code);
        }
    }
} elseif ($user_type === '4') {
    if (editStudentsInfo() === TRUE) {
        header('Location:' . URL . '/view_students.php?status=t');
    } else {
        $err_code = editStudentsInfo();
        if ($err_code === 0 || $err_code === 2 || $err_code === 3 || $err_code === 4) {
            header('Location:' . URL . '/view_students.php?status=' . $err_code);
        }
    }
}
