<?php require_once 'config.php'; ?>
<?php

/*
 * Get current school max Id
 * return int
 */

function getSchoolId() {
    $localCon = dbConnect();
    $sql = "SELECT max(school_id) FROM schools";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * get all school names and id
 * return array
 */

function getSchoolNames() {
    $localCon = dbConnect();
    $sql = "SELECT school_name,school_id FROM schools WHERE active_state='1'";
    $result = mysqli_query($localCon, $sql);
    $name_arr = array();
    $id_arr = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $name_arr[] = $row['school_name'];
        $id_arr[] = $row['school_id'];
    }
    return array($name_arr, $id_arr);
    mysqli_close($localCon);
}

/*
 * get school logos
 * return array
 */

function getSchoolLogos($sclid) {
    $localCon = dbConnect();
    $sql = "SELECT scl_logo FROM schools WHERE school_id=" . $sclid . "";
    $result = mysqli_query($localCon, $sql);
    $logo_arr = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $logo_arr[] = $row['scl_logo'];
    }
    return $logo_arr;
}

/*
 * get user current id
 * return int
 */

function getUserMaxId() {
    $localCon = dbConnect();
    $sql = "SELECT MAX(userId) FROM user";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * get available user name
 * @parm user_name
 * return int
 */

function getUserNameCount($user_name) {
    $localCon = dbConnect();
    $sql = "SELECT COUNT(username) FROM user WHERE username='" . $user_name . "'";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * get manager current id
 * return int array
 */

function getManagerId() {
    $localCon = dbConnect();
    $sql = "SELECT MAX(manager_id) FROM manager";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * get manager user id
 * @parm user_name, email 
 * return int 
 */

function getManagerUserId($user_name, $email) {
    $localCon = dbConnect();
    $sql = "SELECT userId FROM user WHERE username='" . $user_name . "' and email='" . $email . "'";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * Increase id by 1
 * @parm id 
 * return int 
 */

function increaseId($id) {
    if ($id == null || $id == NULL || $id == 0) {
        $id = 1;
    } else {
        $id = $id + 1;
    }
    return $id;
}

/*
 * Get School Details for view
 * return array
 */

function getSchoolInfo() {
    $localCon = dbConnect();
    $sql = "SELECT school_name, school_id, active_state FROM schools";
    $result = mysqli_query($localCon, $sql);
//    $scl_name_arr = array();
//    $scl_id_arr = array();
//    $scl_status_arr = array();
    $scl_arr = array();
    $rtn_arr = array();
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $scl_arr[$i]['school_name'] = $row['school_name'];
        $scl_arr[$i]['school_id'] = $row['school_id'];
        $scl_arr[$i]['active_state'] = $row['active_state'];
        $rtn_arr['data'][$i] = $scl_arr[$i];
        $i = $i + 1;
    }
    mysqli_close($localCon);
    return $rtn_arr;
}

/*
 * Get School Details for view
 * return array
 */

function getManagersInfo() {
    $localCon = dbConnect();
    $sql = "SELECT m.manager_id, m.first_name, m.last_name, u.username, s.school_name, u.active";
    $sql .= " FROM manager m, user u, schools s";
    $sql .= " WHERE u.userId = m.user_id and u.school_id = s.school_id";
    $result = mysqli_query($localCon, $sql);
//    $scl_name_arr = array();
//    $scl_id_arr = array();
//    $scl_status_arr = array();
    $scl_arr = array();
    $rtn_arr = array();
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $scl_arr[$i]['manager_id'] = $row['manager_id'];
        $scl_arr[$i]['first_name'] = $row['first_name'];
        $scl_arr[$i]['last_name'] = $row['last_name'];
        $scl_arr[$i]['username'] = $row['username'];
        $scl_arr[$i]['school_name'] = $row['school_name'];
        $scl_arr[$i]['active'] = $row['active'];
        $rtn_arr['data'][$i] = $scl_arr[$i];
        $i = $i + 1;
    }
    return $rtn_arr; //array($scl_name_arr,$scl_id_arr,$scl_status_arr);
    mysqli_close($localCon);
}

/*
 * Get active managers to assign tests
 * return array
 */

function getActiveManagers() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT m.manager_id, CONCAT(first_name,' ',last_name) as full_name "
                . "FROM manager m, user u, schools s "
                . "WHERE u.userId = m.user_id and u.school_id = s.school_id and s.active_state =1 and u.active=1";
        $result = mysqli_query($localCon, $sql);
        $mgr_id = array();
        $mgr_name = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $mgr_id[] = $row['manager_id'];
            $mgr_name[] = $row['full_name'];
        }
        mysqli_close($localCon);
        return array($mgr_id, $mgr_name);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get active managers to assign tests
 * return array
 */

function getSchoolsByManagerId($m_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT s.school_name "
                . "FROM user u, schools s, manager m "
                . "WHERE m.manager_id=" . $m_id . " and m.user_id=u.userId and u.school_id=s.school_id";
        $result = mysqli_query($localCon, $sql);
        while ($row = mysqli_fetch_assoc($result)) {
            $schoolName = $row['school_name'];
        }
        mysqli_close($localCon);
        return $schoolName;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get max subject id
 * return array
 */

function getSubjectId() {
    $localCon = dbConnect();
    $sql = "SELECT MAX(sub_id) FROM subject";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * Get subject details
 * return array
 */

function getSubjectName() {
    $localCon = dbConnect();
    $sql = "SELECT sub_id,sub_name FROM subject";
    $result = mysqli_query($localCon, $sql);
    $sub_id = array();
    $sub_name = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $sub_id[] = $row['sub_id'];
        $sub_name[] = $row['sub_name'];
    }
    return array($sub_id, $sub_name);
    mysqli_close($localCon);
}

/*
 * Get max lesson id
 * return array
 */

function getMaxLessonId() {
    $localCon = dbConnect();
    $sql = "SELECT MAX(lesson_id) FROM lessons";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * Get lesson details
 * return array
 */

function getLessonName($sub) {
    $localCon = dbConnect();
    $sql = "SELECT lesson_id,lesson_name FROM lessons WHERE subject_id=" . $sub . "";
    $result = mysqli_query($localCon, $sql);
    $les_id = array();
    $les_name = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $les_id[] = $row['lesson_id'];
        $les_name[] = $row['lesson_name'];
    }
    return array($les_id, $les_name);
    mysqli_close($localCon);
}

/*
 * Get all school data
 * return array
 */

function getAllSchoolData($s_id) {
    $localCon = dbConnect();
    $sql = "SELECT school_name, school_des, phone, fax, school_address, active_state, school_id, scl_logo  FROM schools WHERE school_id=" . $s_id . "";
    $result = mysqli_query($localCon, $sql);
    $rs_arr = array();
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    $rs_arr[] = $row;
    //mysqli_close($localCon);
    return $rs_arr;
}

/*
 * Get max test id
 * return array
 */

function getMaxTestId() {
    $localCon = dbConnect();
    $sql = "SELECT MAX(test_id) FROM tests";
    $result = mysqli_query($localCon, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_NUM);
    return $row[0];
    mysqli_close($localCon);
}

/*
 * Load all test Names for assign test to manager
 * return arrays
 */

function getAllTest() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT test_id,test_name FROM tests";
        $result = mysqli_query($localCon, $sql);
        $test_id = array();
        $test_name = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $test_id[] = $row["test_id"];
            $test_name[] = $row["test_name"];
        }
        mysqli_close($localCon);
        return array($test_id, $test_name);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load all test info for view/edit
 * return arrays
 */

function loadAllTestInfo() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT t.test_id,t.test_name,t.test_des,t.points FROM tests t";
        $result = mysqli_query($localCon, $sql);
        $test = array();
        $rtn_arr = array();
        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $test[$i]['test_id'] = $row['test_id'];
            $test[$i]['test_name'] = $row['test_name'];
            $test[$i]['test_des'] = $row['test_des'];
            $test[$i]['points'] = $row['points'];
            $rtn_arr['data'][$i] = $test[$i];
            $i = $i + 1;
        }
        mysqli_close($localCon);
        return $rtn_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load all manager Details For Edit
 * return arrays
 */

function getEditManagers($m_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT first_name, last_name, dob, gender, phone, mobile, address, image_link, description, manager_id "
                . "FROM manager "
                . "WHERE manager_id=" . $m_id . "";
        $result = mysqli_query($localCon, $sql);
        $rs_arr = array();
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $rs_arr[] = $row;
        mysqli_close($localCon);
        return $rs_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load all teacher Details For Edit
 * return arrays
 */

function getEditTeacher($t_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT first_name, last_name, dob, gender, phone, mobile, address, image_link, description, teacher_id "
                . "FROM teacher "
                . "WHERE teacher_id=" . $t_id . "";
        $result = mysqli_query($localCon, $sql);
        $rs_arr = array();
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $rs_arr[] = $row;
        mysqli_close($localCon);
        return $rs_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load all manager Details For Edit
 * return arrays
 */

function getMaxClassId() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT MAX(class_room_id) FROM class_room";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        return $row[0];
        mysqli_close($localCon);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * get teacher current ID
 * return int array
 */

function getTeacherId() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT MAX(teacher_id) FROM teacher";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        return $row[0];
        mysqli_close($localCon);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * get classes in school
 * @param school_id
 * @return int array
 */

function getClasses($scl_id) {
    $localCon = dbConnect();
    $sql = "SELECT class_room_id,class_room_name FROM class_room WHERE school_id=" . $scl_id . "";
    $result = mysqli_query($localCon, $sql);
    $class_id = array();
    $class_name = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $class_id[] = $row['class_room_id'];
        $class_name[] = $row['class_room_name'];
    }
    mysqli_close($localCon);
    return array($class_id, $class_name);
}

/*
 * get Teachers in school
 * @param school_id
 * @return int array
 */

function getTeacherNames($scl_id) {
    $localCon = dbConnect();
    $sql = "SELECT t.teacher_id,CONCAT(t.first_name,' ',t.last_name) as tname "
            . "FROM teacher t,user u "
            . "WHERE u.school_id=" . $scl_id . " and u.userId=t.user_tab_id";
    $result = mysqli_query($localCon, $sql);
    $t_id = array();
    $t_name = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $t_id[] = $row['teacher_id'];
        $t_name[] = $row['tname'];
    }
    mysqli_close($localCon);
    return array($t_id, $t_name);
}

/*
 * get class room to view/edit
 * @param school_id
 * @return int array
 */

function getClassToView($scl_id) {
    $localCon = dbConnect();
    $sql = "SELECT class_room_id,class_room_name,des FROM class_room WHERE school_id=" . $scl_id . "";
    $result = mysqli_query($localCon, $sql);
    $cls_arr = array();
    $rtn_arr = array();
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $cls_arr[$i]['class_id'] = $row['class_room_id'];
        $cls_arr[$i]['class_name'] = $row['class_room_name'];
        $cls_arr[$i]['des'] = $row['des'];
        $rtn_arr['data'][$i] = $cls_arr[$i];
        $i = $i + 1;
    }
    mysqli_close($localCon);
    return $rtn_arr;
}

/*
 * get teacher details view/edit
 * @param school_id
 * @return array
 */

function getTeacherToView($scl_id) {
    $localCon = dbConnect();
    $sql = "SELECT t.teacher_id, t.first_name, t.last_name, u.username, s.school_name, u.active "
            . "FROM teacher t, user u, schools s "
            . "WHERE u.school_id=" . $scl_id . " and u.userId = t.user_tab_id and u.school_id = s.school_id ";
    $result = mysqli_query($localCon, $sql);
    $t_arr = array();
    $rtn_arr = array();
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $t_arr[$i]['teacher_id'] = $row['teacher_id'];
        $t_arr[$i]['first_name'] = $row['first_name'];
        $t_arr[$i]['last_name'] = $row['last_name'];
        $t_arr[$i]['username'] = $row['username'];
        $t_arr[$i]['school_name'] = $row['school_name'];
        $t_arr[$i]['active'] = $row['active'];
        $rtn_arr['data'][$i] = $t_arr[$i];
        $i = $i + 1;
    }
    mysqli_close($localCon);
    return $rtn_arr;
}

/*
 * Upload user image files to server
 */

function uploadImage($location, $img) {
    //$target_dir = "../uploads/";
    $target_dir = $location;
    $fileName = $target_dir . basename($_FILES[$img]["name"]);
    $uploadOk = 1;
    $code = 0;
    $imageFileType = pathinfo($fileName, PATHINFO_EXTENSION);
    $image_info = getimagesize($_FILES[$img]["tmp_name"]);
    $image_width = $image_info[0];
    $image_height = $image_info[1];
    // Check file already there
    if (file_exists($fileName)) {
        $uploadOk = 0;
        $code = 2;
    }
    // Check file size
    if (($image_width > 120) || ($image_height > 120)) {
        $uploadOk = 0;
        $code = 3;
    }
    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        $uploadOk = 0;
        $code = 4;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        return $code;
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES[$img]["tmp_name"], $fileName)) {
            //echo "The file " . basename($_FILES["image"]["name"]) . " has been uploaded.";
            $code = 1;
            return $code;
        } else {
            //echo "Sorry, there was an error uploading your file.";
            $code = 0;
            return $code;
        }
    }
}

/*
 * get student current ID
 * return int array
 */

function getStudentId() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT MAX(student_id) FROM student";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        return $row[0];
        mysqli_close($localCon);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get student names to assign schools
 * @param school_id
 * retrn array
 */

function getStudentsNames($scl_id) {
    $localCon = dbConnect();
    $sql = "SELECT st.student_id, CONCAT(st.first_name,' ',st.last_name) as sname "
            . "FROM student st,user u "
            . "WHERE u.school_id=" . $scl_id . " and u.userId=st.user_tid and st.student_id NOT IN(SELECT std_id FROM student_class)";
    $result = mysqli_query($localCon, $sql);
    $std_id = array();
    $std_name = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $std_id[] = $row['student_id'];
        $std_name[] = $row['sname'];
    }
    mysqli_close($localCon);
    return array($std_id, $std_name);
}

/*
 * Get teacher's class room by user name
 * @param user_name
 * retrn string
 */

function getTeacherClass($user) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT c.class_room_name,c.class_room_id "
                . "FROM class_room c, teacher t, teacher_class t1 "
                . "WHERE t1.t_id = t.teacher_id and t1.class_id = c.class_room_id and t1.t_id ="
                . "(SELECT t.teacher_id FROM teacher t, user u WHERE u.username='" . $user . "' and u.userId=t.user_tab_id);";
        //echo $sql;
        $result = mysqli_query($localCon, $sql);
        $cls = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $cls[0] = $row['class_room_name'];
            $cls[1] = $row['class_room_id'];
        }
        //mysqli_close($localCon);
        return $cls;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get assign test for teachers by user name
 * @param user_name
 * retrn string
 */

function getTeachersTest($user) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT t2.test_id, t2.test_name "
                . "FROM test_teacher t,teacher t1,tests t2 "
                . "WHERE t1.teacher_id=t.teach_id and t2.test_id=t.tst_id and t.teach_id="
                . "(SELECT t3.teacher_id FROM teacher t3, user u WHERE t3.user_tab_id= u.userId and u.username='" . $user . "')";
        $result = mysqli_query($localCon, $sql);
        $t_id = array();
        $t_name = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $t_id[] = $row['test_id'];
            $t_name[] = $row['test_name'];
        }
        mysqli_close($localCon);
        return array($t_id, $t_name);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get student names to assign test
 * @param school_id
 * retrn array
 */

function getStudentsTest($class_id) {
    $localCon = dbConnect();
    $sql = "SELECT s.student_id,CONCAT(s.first_name,' ',s.last_name) as sname "
            . "FROM student_class s1, student s "
            . "WHERE s1.cls_id=" . $class_id . " and s1.std_id=s.student_id";
    $result = mysqli_query($localCon, $sql);
    $std_id = array();
    $std_name = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $std_id[] = $row['student_id'];
        $std_name[] = $row['sname'];
    }
    mysqli_close($localCon);
    return array($std_id, $std_name);
}

/*
 * get student view
 * @param school_id,class id
 * @return array
 */

function getStudentsView($scl_id, $cls_id) {
    $localCon = dbConnect();
    $sql = "SELECT s.student_id,s.first_name,s.last_name,u.username,u.active "
            . "FROM student_class c,student s,user u "
            . "WHERE c.cls_id=" . $cls_id . " and u.school_id=" . $scl_id . " and c.std_id=s.student_id and s.user_tid=u.userId ";
    $result = mysqli_query($localCon, $sql);
    $std_arr = array();
    $rtn_arr = array();
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $std_arr[$i]['student_id'] = $row['student_id'];
        $std_arr[$i]['first_name'] = $row['first_name'];
        $std_arr[$i]['last_name'] = $row['last_name'];
        $std_arr[$i]['username'] = $row['username'];
        $std_arr[$i]['active'] = $row['active'];
        $rtn_arr['data'][$i] = $std_arr[$i];
        $i = $i + 1;
    }
    mysqli_close($localCon);
    return $rtn_arr;
}

/*
 * Load all students Details For Edit
 * return arrays
 */

function getEditStudents($std_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT first_name, last_name, dob, gender, phone, mobile, address, image_link, description, student_id "
                . "FROM student "
                . "WHERE student_id=" . $std_id . "";
        $result = mysqli_query($localCon, $sql);
        $rs_arr = array();
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $rs_arr[] = $row;
        mysqli_close($localCon);
        return $rs_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load all test details by test id
 * @param test_id
 * return array
 */

function getAllTestInfo($tid) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT (SELECT s.sub_name FROM lessons l, subject s, tests t WHERE l.lesson_id = t.lesson_id AND s.sub_id = l.subject_id AND t.test_id = " . $tid . ") AS 'Subject',"
                . "(SELECT l.lesson_name FROM tests t, lessons l WHERE t.test_id = " . $tid . " AND t.lesson_id = l.lesson_id) AS 'Lesson',"
                . " t.test_name, t.points, t.test_des  FROM tests t WHERE t.test_id = " . $tid . "";
        $result = mysqli_query($localCon, $sql);
        $rs_arr = array();
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $rs_arr[] = $row;
        mysqli_close($localCon);
        return $rs_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get students id
 * @param user name
 * return int
 */

function getStudentsId($user) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT s.student_id FROM student s, user u WHERE u.username='" . $user . "' and u.`userId`=s.user_tid";
        $result = mysqli_query($localCon, $sql);
        $student_id = '';
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $student_id = $row[0];
        //mysqli_close($localCon);
        return $student_id;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get class of students
 * @param user name
 * return array
 */

function getStudentClass($user) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT c.class_room_id,c.class_room_name  "
                . "FROM class_room c, student s, student_class s1 "
                . "WHERE s1.cls_id=c.class_room_id and s1.std_id=s.student_id and s.student_id="
                . "(SELECT s.student_id FROM user u, student s WHERE u.username='" . $user . "' and u.userId=s.user_tid)";
        $result = mysqli_query($localCon, $sql);
        $student_arr = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $student_arr[0] = $row['class_room_id'];
            $student_arr[1] = $row['class_room_name'];
        }
        //mysqli_close($localCon);
        return $student_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get test assign for students
 * @param student_id
 * return array
 */

function getTestAssignedToStudents($student_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT t.test_id,t.test_name,t.lesson_id,l.lesson_name,s.sub_id,s.sub_name "
                . "FROM tests t, test_student t1, lessons l, subject s "
                . "WHERE t1.stud_id=" . $student_id . " and t.test_id = t1.text_id and t.lesson_id=l.lesson_id and s.sub_id=l.subject_id";
        //echo $sql;
        $result = mysqli_query($localCon, $sql);
        $numResults = mysqli_num_rows($result);
        if ($numResults > 0) {
            $test_arr = array();
            $lesson_arr = array();
            $sub_arr = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $test_arr[] = $row['test_id'];
                $test_arr[] = $row['test_name'];
                $lesson_arr[] = $row['lesson_name'];
                $sub_arr[] = $row['sub_id'];
            }
            //mysqli_close($localCon);
            return array($test_arr, $lesson_arr, $sub_arr);
        } else {
            //header('Location:' . URL . '/view_test_assign_students.php?status=f');
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get subject belongs to test
 * @param subject_id array
 * return array
 */

function getSubjectsById($sid_arr) {
    try {
        $localCon = dbConnect();
        $sid = implode(',', $sid_arr);
        $sql = "SELECT sub_name,sub_id FROM subject WHERE sub_id IN (" . $sid . ")";
        $result = mysqli_query($localCon, $sql);
        $sub_arr = array();
        $sub_arr1 = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $sub_arr[] = $row['sub_name'];
            $sub_arr1[] = $row['sub_id'];
        }
        //mysqli_close($localCon);
        return array($sub_arr, $sub_arr1);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get lessons and tests
 * @param student_id
 * @param subject_id
 * return array
 */

function getLessonsBySubjectId($sid, $st_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT t2.lesson_id,l.lesson_name,t.text_id,t2.test_name,t2.points "
                . "FROM test_student t,tests t2,student s,subject s1,lessons l "
                . "where t.stud_id=" . $st_id . " and t.stud_id=s.student_id and t.text_id=t2.test_id and s1.sub_id=" . $sid . " and s1.sub_id=l.subject_id and t2.lesson_id=l.lesson_id and t.test_stat='N'";
        $result = mysqli_query($localCon, $sql);
        $lsn_arr = array();
        $tst_id_arr = array();
        $tst_nme_arr = array();
        $tst_points_arr = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $lsn_arr[] = $row['lesson_name'];
            $tst_id_arr[] = $row['text_id'];
            $tst_nme_arr[] = $row['test_name'];
            $tst_points_arr[] = $row['points'];
        }
        //mysqli_close($localCon);
        return array($lsn_arr, $tst_id_arr, $tst_nme_arr, $tst_points_arr);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get test questions and answers
 * @param test_id
 * return array
 */

function loadQA($t_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT q.question_no,q.question,q.ans1,q.ans2,q.ans3,q.ans4 FROM qa_table q WHERE q.test_id=" . $t_id . "";
        $result = mysqli_query($localCon, $sql);
        $qa_arr = array();
        $rtn_qa_arr = array();
        $j = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $qa_arr[$j]['question_no'] = $row['question_no'];
            $qa_arr[$j]['question'] = $row['question'];
            $qa_arr[$j]['ans1'] = $row['ans1'];
            $qa_arr[$j]['ans2'] = $row['ans2'];
            $qa_arr[$j]['ans3'] = $row['ans3'];
            $qa_arr[$j]['ans4'] = $row['ans4'];
            $rtn_qa_arr[$j] = $qa_arr[$j];
            $j = $j + 1;
        }
        return $rtn_qa_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get max Kidscoin Account ID
 * return int
 */

function getStudentAccountId($std_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT k.acc_id FROM kids_coin_account k WHERE k.student_id=" . $std_id . "";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        return $row[0];
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get current account and savings accounts amount
 * @param student_id
 * return array
 */

function getMyAccountAmounts($std_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT (k.income_account_points+k.bonus_points) as income_account_points,k.savings_account_points, (k.income_account_points+k.bonus_points+k.savings_account_points) as total_points "
                . "FROM kids_coin_account k WHERE k.student_id=" . $std_id . "";
        $result = mysqli_query($localCon, $sql);
        $amt_arr = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $amt_arr[] = $row['income_account_points'];
            $amt_arr[] = $row['savings_account_points'];
            $amt_arr[] = $row['total_points'];
        }
        mysqli_close($localCon);
        return $amt_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get max Kidscoin Account ID
 * @param student_id
 * return array
 */

function getMyTransactions($std_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT DATE_FORMAT(k1.create_date,'%d-%m-%Y') as trans_date, k1.trans_des, k1.income_account_tr, k1.savings_account_tr, k1.bonus_points_tr "
                . "FROM kids_coin_account k, kidscoin_transaction k1, student s "
                . "WHERE k.student_id=" . $std_id . " and s.student_id = k.student_id and k.acc_id = k1.acnt_id";
        $result = mysqli_query($localCon, $sql);
        $trns_arr = array();
        $rtn_arr = array();
        $i = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $trns_arr[$i]['trans_date'] = $row['trans_date'];
            $trns_arr[$i]['trans_des'] = $row['trans_des'];
            $trns_arr[$i]['income_account_tr'] = $row['income_account_tr'];
            $trns_arr[$i]['savings_account_tr'] = $row['savings_account_tr'];
            $trns_arr[$i]['bonus_points_tr'] = $row['bonus_points_tr'];
            $rtn_arr['data'][$i] = $trns_arr[$i];
            $i = $i + 1;
        }
        mysqli_close($localCon);
        return $rtn_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get teachers id
 * @param user name
 * return int
 */

function getTeachersId($user) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT t.teacher_id FROM teacher t, user u WHERE u.username='" . $user . "' and u.userId=t.user_tab_id";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $teacher_id = $row[0];
        mysqli_close($localCon);
        return $teacher_id;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get manager id
 * @param user name
 * return int
 */

function getManagersId($user) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT m.manager_id FROM manager m,user u WHERE u.username='" . $user . "' and u.userId=m.user_id";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $mgr_id = $row[0];
        mysqli_close($localCon);
        return $mgr_id;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get test assign to manager
 * @param manager_id
 * return array
 */

function getTestAssignManager($man_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT t.test_name, t.test_id FROM tests t, manager m, test_manager t1 WHERE m.manager_id =" . $man_id . " AND m.manager_id = t1.mgr_id and t.test_id = t1.txt_id";
        $result = mysqli_query($localCon, $sql);
        $tst_name = array();
        $tst_id = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $tst_name[] = $row['test_name'];
            $tst_id[] = $row['test_id'];
        }
        mysqli_close($localCon);
        return array($tst_id, $tst_name);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get test assign to manager
 * @param manager_id
 * return array
 */

function loadStudentTestSitCount($st_id, $ts_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT t.sit_count,t1.points "
                . "FROM test_student t, tests t1, student s "
                . "WHERE s.student_id=" . $st_id . " and t.text_id=" . $ts_id . " and t.stud_id=s.student_id and t.text_id=t1.test_id";
        $result = mysqli_query($localCon, $sql);
        $sit_cnt = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $sit_cnt[0] = $row['sit_count'];
            $sit_cnt[1] = $row['points'];
        }
        mysqli_close($localCon);
        return array($sit_cnt);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get most kid coiners
 * @param class_is
 * @param school_id
 * return array
 */

function loadMostKidCoiners($cls_id, $scl_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT  CONCAT(s1.first_name,' ',s1.last_name) as S_Name, s1.image_link, (k.income_account_points+k.savings_account_points+k.bonus_points) as go_acc "
                . "FROM  kids_coin_account k, student_class s, student s1, user u "
                . "WHERE s.cls_id=" . $cls_id . " and u.school_id=" . $scl_id . " and s.std_id=s1.student_id and s1.user_tid=u.userId and k.student_id=s1.student_id "
                . "ORDER BY  go_acc DESC "
                . "LIMIT 10";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $student_name = array();
            $student_img = array();
            $student_points = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $student_name[] = $row['S_Name'];
                $student_img[] = $row['image_link'];
                $student_points[] = $row['go_acc'];
            }
            //mysqli_close($localCon);
            return array($student_name, $student_img, $student_points);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get most kid coiners in school
 * @param school_id
 * return array
 */

function loadMostKidCoinersInSchool($scl_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT  CONCAT(s1.first_name,' ',s1.last_name) as S_Name, s1.image_link, (k.income_account_points+k.savings_account_points+k.bonus_points) as go_acc "
                . "FROM  kids_coin_account k, student_class s, student s1, user u "
                . "WHERE u.school_id=" . $scl_id . " and s.std_id=s1.student_id and s1.user_tid=u.userId and k.student_id=s1.student_id "
                . "ORDER BY  go_acc DESC "
                . "LIMIT 10";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $student_name = array();
            $student_img = array();
            $student_points = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $student_name[] = $row['S_Name'];
                $student_img[] = $row['image_link'];
                $student_points[] = $row['go_acc'];
            }
            //mysqli_close($localCon);
            return array($student_name, $student_img, $student_points);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load less kids coiners based on sucess tare
 * @param class_is
 * @param school_id
 * return mix
 */

function loadLessKidCoiners($cls_id, $scl_id, $order, $op) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT CONCAT(s1.first_name, ' ', s1.last_name) AS s_name, s1.image_link,TRUNCATE((count(t.text_id)/sum(t.sit_count)*100),1) as s_rate "
                . "FROM  student_class s, student s1, user u,test_student t "
                . "WHERE s.cls_id" . $op . "" . $cls_id . " and u.school_id=" . $scl_id . " AND s.std_id=s1.student_id AND s1.user_tid=u.userId AND t.stud_id=s1.student_id AND t.sit_count>0 "
                . "GROUP BY  s_name "
                . "ORDER BY  s_rate " . $order . " "
                . "LIMIT 10";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $student_name = array();
            $student_img = array();
            $s_rate = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $student_name[] = $row['s_name'];
                $student_img[] = $row['image_link'];
                $s_rate[] = $row['s_rate'];
            }
            //mysqli_close($localCon);
            return array($student_name, $student_img, $s_rate);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load class performence
 * @param class_is
 * @param school_id
 * return mix
 */

function loadClassPerformence($cls_id, $scl_id, $op) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT sit_count1 "
                . "FROM (SELECT SUM(t.sit_count) AS sit_count1 FROM student_class s, student s1, user u, test_student t WHERE s.cls_id" . $op . "" . $cls_id . " AND u.school_id = " . $scl_id . " AND s.std_id = s1.student_id AND s1.user_tid = u.userId AND t.stud_id = s1.student_id AND t.sit_count > 0 "
                . "UNION ALL "
                . "SELECT SUM(t.sit_count) AS sit_count1 FROM student_class s, student s1, user u, test_student t WHERE s.cls_id" . $op . "" . $cls_id . " AND u.school_id = " . $scl_id . " AND s.std_id = s1.student_id AND s1.user_tid = u.userId AND t.stud_id = s1.student_id AND t.sit_count > 0 AND t.test_stat = 'Y') AS C";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $attempts = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $attempts[] = $row['sit_count1'];
            }
            //mysqli_close($localCon);
            return $attempts;
        } else {
            $attempts[] = 0;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load sit count of school(All Clases)
 * @param class_is
 * @param school_id
 * return mix
 */

function loadSchoolTestSitCounts($scl_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT c.class_room_name,SUM(t.sit_count) AS sits,s.cls_id "
                . "FROM class_room c,student_class s, test_student t "
                . "WHERE c.school_id=" . $scl_id . " and c.class_room_id=s.cls_id and s.std_id=t.stud_id "
                . "GROUP BY c.class_room_name "
                . "ORDER BY c.class_room_name ASC "
                . "LIMIT 10";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $cls_arr = array();
            $cls_name_arr = array();
            $sit_cnt_arr = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $cls_arr[] = $row['cls_id'];
                $cls_name_arr[] = $row['class_room_name'];
                $sit_cnt_arr[] = $row['sits'];
            }
            //mysqli_close($localCon);
            return array($cls_arr, $cls_name_arr, $sit_cnt_arr);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load class performence
 * @param class_is
 * @param school_id
 * return mix
 */

function getClassTestCount($cls_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT SUM(t.sit_count) AS test_cnt FROM class_room c,student_class s, test_student t "
                . "WHERE s.cls_id=" . $cls_id . " and c.class_room_id=s.cls_id and s.std_id=t.stud_id and t.test_stat='Y'";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            while ($row = mysqli_fetch_assoc($result)) {
                $test_cnt = $row['test_cnt'];
            }
            //mysqli_close($localCon);
            return $test_cnt;
        } else {
            $test_cnt = 0;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Load most kids coiners (All Schools)
 * return mix
 */

function loadMostKCSchools() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT s.school_name,s.scl_logo,SUM(k.income_account_points+k.savings_account_points+k.bonus_points) AS points "
                . "FROM schools s, kids_coin_account k, student s1, user u "
                . "WHERE k.student_id=s1.student_id AND s1.user_tid=u.`userId` AND u.school_id=s.school_id "
                . "GROUP BY s.school_name "
                . "ORDER BY (k.income_account_points+k.savings_account_points+k.bonus_points) DESC "
                . "LIMIT 10";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $class_arr = array();
            $img_arr = array();
            $points_arr = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $class_arr[] = $row['school_name'];
                $img_arr[] = $row['scl_logo'];
                $points_arr[] = $row['points'];
            }
            //mysqli_close($localCon);
            return array($class_arr, $points_arr, $img_arr);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Best class calculation
 * @param array
 * return mixed
 *
 */

function bestClassCalculation($rt_scl_arr) {
    $rt_cls_id = $rt_scl_arr[0];
    $rt_cls_nme = $rt_scl_arr[1];
    $rt_sit_cnt = $rt_scl_arr[2];
    $rt_tst_cnt = array();
    $s_rate = array();
    $data_arr = array();
    $display_arr = array();
    for ($i = 0; $i < count($rt_cls_id); $i++) {
        $rt_tst_cnt[$i] = getClassTestCount($rt_cls_id[$i]);
        if ($rt_sit_cnt[$i] == '0') {
            $s_rate[] = 0;
        } else {
            $s_rate[] = ($rt_tst_cnt[$i] / $rt_sit_cnt[$i]) * 100;
        }

        $data_arr[$i]['name'] = $rt_cls_nme[$i];
        $data_arr[$i]['s_rate'] = $s_rate[$i];
        $display_arr[$i] = $data_arr[$i];
    }
    usort($display_arr, function($a, $b) {
        return $b['s_rate'] > $a['s_rate'];
    });
    return $display_arr;
}

/*
 * Load best performerce (all schools)
 * return mixed
 */

function loadTopPerformerce() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT CONCAT(s1.first_name, ' ', s1.last_name) AS s_name, s1.image_link,TRUNCATE((count(t.text_id)/sum(t.sit_count)*100),1) as s_rate, s2.school_name "
                . "FROM  student_class s, student s1, user u,test_student t, schools s2 "
                . "WHERE s.std_id=s1.student_id AND s1.user_tid=u.userId AND t.stud_id=s1.student_id AND u.school_id=s2.school_id AND t.sit_count>0 "
                . "GROUP BY  s_name  "
                . "ORDER BY  s_rate DESC "
                . "LIMIT 10";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $stud_name = array();
            $img_arr = array();
            $pts_arr = array();
            $scl_arr = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $stud_name[] = $row['s_name'];
                $img_arr[] = $row['image_link'];
                $pts_arr[] = $row['s_rate'];
                $scl_arr[] = $row['school_name'];
            }
            //mysqli_close($localCon);
            return array($stud_name, $img_arr, $pts_arr, $scl_arr);
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Kids coin performence
 * return mixed
 */

function loadKidsCoinPerformence() {
    try {
        $localCon = dbConnect();
        $sql = "SELECT sit_count1 "
                . "FROM (SELECT SUM(t.sit_count) AS sit_count1 FROM student_class s, student s1, user u, test_student t WHERE s.std_id = s1.student_id AND s1.user_tid = u.userId AND t.stud_id = s1.student_id AND t.sit_count > 0 "
                . "UNION ALL "
                . "SELECT SUM(t.sit_count) AS sit_count1 FROM student_class s, student s1, user u, test_student t WHERE s.std_id = s1.student_id AND s1.user_tid = u.userId AND t.stud_id = s1.student_id AND t.sit_count > 0 AND t.test_stat = 'Y') AS C";
        $result = mysqli_query($localCon, $sql);
        if ($result !== FALSE) {
            $kc_tot_tries = array();
            while ($row = mysqli_fetch_assoc($result)) {
                $kc_tot_tries[] = $row['sit_count1'];
            }
            //mysqli_close($localCon);
            return $kc_tot_tries;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Has manager function 
 * @param school_id
 * return int
 */

function hasSclManagers($sc_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT count(u.userId) FROM user u WHERE u.school_id=" . $sc_id . " AND u.roleId=2";
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $mgr_cnt = $row[0];
        mysqli_close($localCon);
        return $mgr_cnt;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Deactivate users 
 * @param manager or student or teacher id
 */

function deactivateUsers($id, $u_type) {
    try {
        $localCon = dbConnect();
        if ($u_type == '2') {
            $sql = "SELECT u.userId FROM user u,manager m WHERE m.manager_id=" . $id . " AND m.user_id=u.userId";
        } elseif ($u_type == '3') {
            $sql = "SELECT u.userId FROM user u,teacher t WHERE t.teacher_id=" . $id . " AND t.user_tab_id=u.userId";
        } elseif ($u_type == '4') {
            $sql = "SELECT u.userId FROM user u,student s WHERE s.student_id=" . $id . " AND s.user_tid=u.userId";
        }
        $result = mysqli_query($localCon, $sql);
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $u_id = $row[0];
        $err;
        if (isset($u_id)) {
            $sql_update = "UPDATE user SET active=0 WHERE userId =" . $u_id . "";
            if (!mysqli_query($localCon, $sql_update)) {
                die('Error: ' . mysqli_error($localCon));
                $err = 'db_err';
                return $err;
            }
            mysqli_close($localCon);
            $err = 'true';
            return $err;
        } else {
            $err = 'false';
            return $err;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get test questions and answers
 * @param test_id
 * return array
 */

function loadQAEdit($t_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT q.question_no,q.question,q.ans1,q.ans2,q.ans3,q.ans4,q.correct_ans FROM qa_table q WHERE q.test_id=" . $t_id . "";
        $result = mysqli_query($localCon, $sql);
        $qa_arr = array();
        $rtn_qa_arr = array();
        $j = 0;
        while ($row = mysqli_fetch_assoc($result)) {
            $qa_arr[$j]['question_no'] = $row['question_no'];
            $qa_arr[$j]['question'] = $row['question'];
            $qa_arr[$j]['ans1'] = $row['ans1'];
            $qa_arr[$j]['ans2'] = $row['ans2'];
            $qa_arr[$j]['ans3'] = $row['ans3'];
            $qa_arr[$j]['ans4'] = $row['ans4'];
            $qa_arr[$j]['correct_ans'] = $row['correct_ans'];
            $rtn_qa_arr[$j] = $qa_arr[$j];
            $j = $j + 1;
        }
        return $rtn_qa_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get all subjuct to view/edit
 * return array
 */

function loadSubjectInfo($sid) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT sub_id, sub_name, subject_des, sub_type FROM subject WHERE sub_id=" . $sid . "";
        $result = mysqli_query($localCon, $sql);
        $sub_arr = array();
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $sub_arr[] = $row;
        mysqli_close($localCon);
        return $sub_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Get lesson info to view/edit
 * return array
 */

function loadLessonInfo($lid) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT lesson_id,lesson_name,lesson_des FROM lessons WHERE lesson_id=" . $lid . "";
        $result = mysqli_query($localCon, $sql);
        $les_arr = array();
        $row = mysqli_fetch_array($result, MYSQLI_NUM);
        $les_arr[] = $row;
        mysqli_close($localCon);
        return $les_arr;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

/*
 * Delete lessons
 * @param lesson_id
 * return bool
 */

function deleteLessons($lid) {
    try {
        $localCon = dbConnect();
        $msg="";
        $sql = "DELETE FROM lessons WHERE lesson_id=" . $lid . "";
        if (mysqli_query($localCon, $sql)) {
            $msg="true";
        } else {
            $msg="false";
        }
        mysqli_close($localCon);
        return $msg;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}
/*
 * Delete subject
 * @param lesson_id
 * return bool
 */

function deleteSubject($sid) {
    try {
        $localCon = dbConnect();
        $msg="";
        $sql = "DELETE FROM subject WHERE sub_id=".$sid."";
        if (mysqli_query($localCon, $sql)) {
            $msg="true";
        } else {
            $msg="false";
        }
        mysqli_close($localCon);
        return $msg;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}
/*
 * Get detail students stat
 * @param student_id
 * @return mix
 */
function loadStudentDetailStat($std_id){
    try {
        $localCon = dbConnect();
        $sql = "SELECT t1.test_name, DATE_FORMAT(t.date_assign, '%m-%d-%Y') AS date_assign, t.sit_count,"
                . "CASE "
                . "WHEN t.test_stat = 'Y' THEN 'PASS' "
                . "WHEN t.test_stat = 'N' THEN 'NOT PASS' END AS stat, "
                . "CASE "
                . "WHEN t.sit_count > 0 THEN TRUNCATE(((count(t.text_id)/t.sit_count)*100),1) "
                . "WHEN t.sit_count = 0 THEN 0 END AS s_rate "
                . "FROM test_student t, tests t1 "
                . "WHERE t.stud_id=".$std_id." AND t1.test_id=t.text_id "
                . "GROUP BY t.text_id "
                . "ORDER BY t.date_assign";
        $result = mysqli_query($localCon, $sql);
        if($result !== FALSE){
            $std_arr=array();
            $i=0;
            while ($row = mysqli_fetch_assoc($result)) {
                $std_arr[$i]['test_name']=$row['test_name'];
                $std_arr[$i]['date_assign']=$row['date_assign'];
                $std_arr[$i]['sit_count']=$row['sit_count'];
                $std_arr[$i]['stat']=$row['stat'];
                $std_arr[$i]['s_rate']=$row['s_rate'];
                $i=$i+1;
            }
            mysqli_close($localCon);
            return $std_arr;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}
/*
 * Get overall stat
 * @param student_id
 * @return mix
 */
function studentOverallStat($std_id){
    try {
        $localCon = dbConnect();
        $sql = "SELECT concat(s.first_name,' ',s.last_name) AS s_name, count(t.text_id) as success,sum(t.sit_count) as attempt,(count(t.text_id)/sum(t.sit_count)*100) AS s_rate "
                . "FROM test_student t, student s "
                . "WHERE t.stud_id=".$std_id." AND s.student_id=t.stud_id AND t.sit_count>0";
        $result = mysqli_query($localCon, $sql);
        if($result !== FALSE){
            $stat_arr=array();
            $row = mysqli_fetch_array($result, MYSQLI_NUM);
            $stat_arr[] = $row;
            return $stat_arr;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}