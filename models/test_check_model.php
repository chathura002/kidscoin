<?php
session_start();
$t_id = $_SESSION['test_id'];
require_once 'config.php';
require './common_model.php';
$answers = json_decode(filter_input(INPUT_POST, 'data'));

function checkAnswers($t_id) {
    try {
        $localCon = dbConnect();
        $sql = "SELECT q.correct_ans,t.points FROM tests t,qa_table q WHERE q.test_id=" . $t_id . " and t.test_id=q.test_id";
        $result = mysqli_query($localCon, $sql);
        $correcr_ans_arr = array();
        $pnts_arr = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $correcr_ans_arr[] = $row['correct_ans'];
            $pnts_arr[] = $row['points'];
        }
        return array($correcr_ans_arr, $pnts_arr);
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function addCoinsToStudentAccount($points) {
    try {
        $localCon = dbConnect();
        $acc_id = getStudentAccountId($_SESSION['student_id']);
        $sql_str = "INSERT INTO kidscoin_transaction(acnt_id, trans_des, income_account_tr, savings_account_tr, bonus_points_tr, fine_points_tr, create_date, create_by, create_role) "
                . "VALUES (" . $acc_id . ",'Complete test-" . $_SESSION['test_name'] . "'," . $points . ",0,0,0,NOW(),'" . $_SESSION['user_name'] . "'," . $_SESSION['role_code'] . ");";
        $sql_str.= "UPDATE kids_coin_account SET "
                . "income_account_points=income_account_points+" . $points . ","
                . "update_by='" . $_SESSION['user_name'] . "',"
                . "date_modify=NOW(),"
                . "role_update=" . $_SESSION['role_code'] . " "
                . "WHERE acc_id=" . $acc_id . ";";

        if (!mysqli_multi_query($localCon, $sql_str)) {
            die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    } 
}

function updateSitCount($stat) {
    try {
        $localCon = dbConnect();
        $sql_update = "UPDATE test_student SET "
                . "sit_count=(sit_count+1), "
                . "test_stat='" . $stat . "', "
                . "update_by='" . $_SESSION['user_name'] . "', "
                . "role_update=" . $_SESSION['role_code'] . ", "
                . "date_modify=NOW() "
                . "WHERE stud_id=" . $_SESSION['student_id'] . " and text_id=" . $_SESSION['test_id'] . "";
        if (!mysqli_query($localCon, $sql_update)) {
            die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        //mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

$rtn_mix_arr = checkAnswers($t_id);
$db_ans = $rtn_mix_arr[0];
$points_arr = array_unique($rtn_mix_arr[1]);
$points = implode($points_arr);
$stat = '';

if ($answers === $db_ans) {
    if (updateSitCount($stat = 'Y') === TRUE) {
        addCoinsToStudentAccount($points);
        echo json_encode('true');
        //header('Location:' . URL . '/view_test_assign_students.php?status=t');
    }
} else {
    updateSitCount($stat = 'N');
    echo json_encode('false');
    //header('Location:' . URL . '/view_test_assign_students.php?status=f');
}

