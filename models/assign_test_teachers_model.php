<?php
session_start();
require_once 'config.php';
require 'common_model.php';
$t_id = intval(filter_input(INPUT_POST, 'tcr'));
$txt_id = intval(filter_input(INPUT_POST, 'txt'));
function assignTestTeacher() {
    try {
        global $t_id;
        global $txt_id;
        $assign_by=$_SESSION['user_name'];
        $assign_role=$_SESSION['role_code'];
        $localCon = dbConnect();
        $sql = "INSERT INTO test_teacher(teach_id, tst_id, assign_by, date_assign, role_assign, update_by, date_update, role_update) "
                . "VALUES (".$t_id.",".$txt_id.",'".$assign_by."',NOW(),".$assign_role.",'".$assign_by."',NOW(),".$assign_role.")";
        if (!mysqli_query($localCon, $sql)) {
            //die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    } 
}

function checkAlreadyAssign($t_id){
    try {
        global $txt_id;
        $localCon = dbConnect();
        $sql = "SELECT tst_id FROM test_teacher WHERE teach_id =".$t_id."";
        $result = mysqli_query($localCon,$sql);
        $txt = array();
        while ($row = mysqli_fetch_assoc($result)) { 
            $txt[] = $row['tst_id'];
        }  
        if (in_array($txt_id, $txt)){
            return FALSE;
        }
        else{
            return TRUE;
        }
        
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    } 
}

if(checkAlreadyAssign($t_id)===TRUE){
   if(assignTestTeacher()===TRUE){
       header('Location:' . URL . '/assign_test_teachers.php?status=t');
   }  else {
       header('Location:' . URL . '/assign_test_teachers.php?status=f');
   }
}  else {
    header('Location:' . URL . '/assign_test_teachers.php?status=c');
}



