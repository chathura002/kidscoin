<?php
session_start();
require_once 'config.php';
require 'common_model.php';

function editSubject(){
    try {
        $sub_id=filter_input(INPUT_POST, 'sub-id');
        $sub_name=filter_input(INPUT_POST, 'subject-name');
        $sub_des=filter_input(INPUT_POST, 'subject-des');
        $sub_tpe=filter_input(INPUT_POST, 'sub-type');
        if(isset($sub_id) && isset($sub_name) && isset($sub_des) && isset($sub_tpe)){
            $localCon = dbConnect();
            $sql = "UPDATE subject SET "
                    . "sub_name='".$sub_name."', "
                    . "subject_des='".$sub_des."', "
                    . "sub_type=".$sub_tpe.", "
                    . "update_by='".$_SESSION['user_name']."', "
                    . "role_modify=".$_SESSION['role_code'].", "
                    . "date_modify=NOW() WHERE sub_id=".$sub_id."";
            if (!mysqli_query($localCon, $sql)) {
                die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            return TRUE;
        }  else {
            return FALSE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}

if(editSubject()===TRUE){
    header('Location:'.URL.'/edit_subject.php?status=t');
}  else {
    header('Location:'.URL.'/edit_subject.php?status=t');
}
