<?php
session_start();
require_once 'config.php';
require 'common_model.php';

function addLessons() {
    try {
        $localCon = dbConnect();
        $sub_name = filter_input(INPUT_POST, 'subject-name');
        $lesson_name = mysqli_real_escape_string($localCon,filter_input(INPUT_POST, 'lesson-name'));
        $lesson_des = mysqli_real_escape_string($localCon,filter_input(INPUT_POST, 'lesson-des'));
        $create_by = $_SESSION['user_name'];
        $role_code = $_SESSION['role_code'];
        $curr_id = getMaxLessonId();
        $lesson_id = increaseId($curr_id);

        $sql_str = "INSERT INTO lessons(lesson_id, subject_id, lesson_name, lesson_des, create_by, create_role, create_time, modify_by, modify_role, modify_date) "
                . "VALUES (" . $lesson_id . ", " . $sub_name . ",'" . $lesson_name . "','" . $lesson_des . "','" . $create_by . "'," . $role_code . ",NOW(),'" . $create_by . "'," . $role_code . ",NOW());";

        if (!mysqli_query($localCon, $sql_str)) {
            die('Error: ' . mysqli_error($localCon));
            return FALSE;
        }
        mysqli_close($localCon);
        return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

try {
    if (addLessons() === TRUE) {
        header('Location:' . URL . '/create_lessons.php?status=t');
    } else {
        header('Location:' . URL . '/create_lessons.php?status=f');
    }
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}

