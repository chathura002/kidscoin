<?php 
session_start();
require_once 'config.php'; 
require 'common_model.php';


function addSubject(){
    $sub_name=filter_input(INPUT_POST, 'subject-name');
    $sub_des=filter_input(INPUT_POST, 'subject-des');
    $sub_type=filter_input(INPUT_POST, 'sub-type');
    $create_by = $_SESSION['user_name'];
    $role_code = $_SESSION['role_code'];
    try {
        $sub_id = getSubjectId();
        $curr_id=increaseId($sub_id);
        $localCon = dbConnect();
        $sql_str = "INSERT INTO subject(sub_id, sub_name, subject_des, sub_type, creste_by, create_role, create_date, update_by, role_modify, date_modify) "
                . "VALUES (".$curr_id.",'".$sub_name."','".$sub_des."',".$sub_type.",'".$create_by."',".$role_code.",NOW(),'".$create_by."',".$role_code.",NOW())";
        if (!mysqli_query($localCon, $sql_str)) {
        die('Error: ' . mysqli_error($localCon));
        return FALSE;
    }
    mysqli_close($localCon);
    return TRUE;
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }

}

if(addSubject()===TRUE){
    header('Location:'.URL.'/create_subject.php?status=t');
}  else {
    header('Location:'.URL.'/create_subject.php?status=f');
}

