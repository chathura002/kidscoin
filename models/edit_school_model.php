<?php
session_start();
require_once 'config.php';
require 'common_model.php';
?>
<?php

function editSchoolInfo() {
    try {
        $school_name = filter_input(INPUT_POST, 'school-name');
        $phn = filter_input(INPUT_POST, 'phone');
        $fax = filter_input(INPUT_POST, 'fax');
        $address = filter_input(INPUT_POST, 'address');
        $status = filter_input(INPUT_POST, 'status');
        $school_des = filter_input(INPUT_POST, 'des');
        $target_dir = "logos/";
        $target_file = $target_dir . basename($_FILES["logo"]["name"]);
        $old_image = filter_input(INPUT_POST, 'img-url');
        $s_id = filter_input(INPUT_POST, 'sid');
        $create_by = $_SESSION['user_name'];
        if (empty($_FILES["logo"]["name"])) {
            $target_file = $old_image;
        }
        if (!empty($school_name) || !empty($phn) || !empty($fax) || !empty($address) || !empty($status) || !empty($school_des) || !empty($s_id) || !empty($create_by) || !empty($target_file) ) {
            $localCon = dbConnect();
            $sql = "UPDATE schools SET "
                    . "school_name='" . $school_name . "',"
                    . "school_des='" . $school_des . "',"
                    . "phone='" . $phn . "',"
                    . "fax='" . $fax . "',"
                    . "school_address='" . $address . "', "
                    . "scl_logo='" . $target_file . "', "
                    . "update_date=NOW(),"
                    . "update_by='" . $create_by . "',"
                    . "active_state=" . $status . " "
                    . "WHERE school_id=" . $s_id . "";
            if (!mysqli_query($localCon, $sql)) {
                die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            editOrUpdateImage($target_file, $old_image);
            return TRUE;
        } else {
            return FALSE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function editOrUpdateImage($img_file,$old_img) {
    
    if(($old_img!==$img_file) && $old_img!=='logos/'){
           unlink(URL.'/'.$old_img);
           uploadImage(URL.'/logos/',"logo");
    }
    else {
        uploadImage(URL.'/logos/',"logo");
    }
}

if(editSchoolInfo()===TRUE){
    header('Location:'.URL.'/edit_schools.php?status=t');
} else {
    header('Location:'.URL.'/edit_schools.php?status=f');
}