<?php

session_start();
require_once 'config.php';
require 'common_model.php';

function assignClassStudents() {
    try {
        $cls_id = $_SESSION['class_id'];
        //global $std_arr;
        $assign_by = $_SESSION['user_name'];
        $assign_role = $_SESSION['role_code'];

        $localCon = dbConnect();
        $sql = '';
        foreach ($_POST['std'] as $selected_student) {
            if (checkAlreadyAssignClass($selected_student) === TRUE) {
                global $sql;
                $sql .= "INSERT INTO student_class(std_id, cls_id, assign_by, assign_date, assign_role, update_by, date_modify, update_role) "
                        . "VALUES (" . $selected_student . "," . $cls_id . ",'" . $assign_by . "',NOW()," . $assign_role . ",'" . $assign_by . "',NOW()," . $assign_role . ");";
            }
        }
        if (!empty($sql)) {
            //echo $sql;
            if (!mysqli_multi_query($localCon, $sql)) {
                //die('Error: ' . mysqli_error($localCon));
                return FALSE;
            }
            mysqli_close($localCon);
            return TRUE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

function checkAlreadyAssignClass($std_id) {
    try {
        $cls_id = $_SESSION['class_id'];
        $localCon = dbConnect();
        $sql = "SELECT cls_id FROM student_class WHERE std_id=" . $std_id . "";
        $result = mysqli_query($localCon, $sql);
        $class = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $class[] = $row['cls_id'];
        }
        if (in_array($cls_id, $class)) {
            return FALSE;
        } else {
            return TRUE;
        }
    } catch (Exception $exc) {
        echo $exc->getTraceAsString();
    }
}

if (assignClassStudents() === TRUE) {
    header('Location:' . URL . '/assign_class_student.php?status=t');
} else {
    header('Location:' . URL . '/assign_class_student.php?status=f');
}