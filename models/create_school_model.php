<?php 
session_start();
require_once 'config.php'; 
require 'common_model.php';
?>
<?php 
function createSchool(){
$school_name= filter_input(INPUT_POST, 'school-name');
$phn= filter_input(INPUT_POST, 'phone');
$fax= filter_input(INPUT_POST, 'fax');
$address= filter_input(INPUT_POST, 'address');
$status= filter_input(INPUT_POST, 'status');
$school_des= filter_input(INPUT_POST, 'des');
$target_dir = "logos/";
$target_file = $target_dir . basename($_FILES["logo"]["name"]);
$create_by=$_SESSION['user_name'];
$role=$_SESSION['role'];
$role_id=$_SESSION['role_code'];

$id=  getSchoolId();
if($id=="null" || $id==0){
    $id=1;
}
 else {
    $id=$id+1;
}

$localCon = dbConnect();
$sql_str ="INSERT INTO schools(school_id, school_name, school_des, phone, fax, school_address, scl_logo, create_date, create_by, role, update_date, update_by, active_state) ";
$sql_str.= "VALUES (".$id.",'".$school_name."','".$school_des."','".$phn."','".$fax."','".$address."', '".$target_file."',NOW(),'".$create_by."','".$role_id."',NOW(), '".$create_by."',".$status.")";

if (!mysqli_query($localCon, $sql_str)) {
        //die('Error: ' . mysqli_error($localCon));
        return FALSE;
}
if (!empty($_FILES["logo"]["name"])) {
            uploadImage("../logos/","logo");
        }
mysqli_close($localCon);
return TRUE;
}

if(createSchool()===TRUE){
    header('Location:'.URL.'/create_school.php?status=t');
}
 else {
    header('Location:'.URL.'/create_school.php?status=f');
}








