<?php include 'header.php';
      require_once './models/config.php';
?>
<body class="login-bcolor">
<div class="container container-table">
    <div class="row vertical-center-row">
        <div class="col-md-4 col-md-offset-4">
            <img class="img-responsive center-block" src="<?php echo URL?>/images/KidsCoin-logo-web.png">
            <div class="text-center login-title">LOGIN TO YOUR ACCOUNT</div>
            <div class="login-form">
                    <form name="login-form" id="login-form" action="<?php echo URL; ?>/models/login-action.php" method="post">
                        <div class="form-group">
                            <input class="form-control" type="text" name="user-name" id="user-name" placeholder="User Name" data-validate="required" autocomplete="off">             
                        </div>
                        <div class="form-group">
                            <input class="form-control" type="password" name="user-pwd" id="user-pwd" placeholder="Password" data-validate="required" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="form-control btn btn-default ">Login</button>
                        </div>
                        <div class="text-right" id="lst-pwd"><a href="<?php echo URL;?>/pwd_recovery.php">Lost Your Password?</a></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>

<script>
$(document).ready(function(){
//    $.urlParam = function(name){
//	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
//        if (results===null){
//            return null;
//        }
//	else{
//            return results[1] || 0;
//        }
//};

var err = decodeURIComponent($.urlParam('login'));
console.log(err);

if(err==='f'){
    $("<div>User Name and Password Not Match!</div>").insertBefore("#lst-pwd").addClass("text-center err");
}
else if(err==='a'){
    $("<div>Your Account Is Inactive</div>").insertBefore("#lst-pwd").addClass("text-center err");
}

});
</script>